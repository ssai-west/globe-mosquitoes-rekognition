# Rekognition

This repo is for development of code interfacing with AWS Rekognition.  It is partly written in Python and partly written in Node.js.

### Note on aws-sdk

The latest version of the aws-sdk (@2.756.0) was installed for this project using:

```npm install aws-sdk```

Prior to updating to this version, the AWS Rekognition Label detection returned only the name and confidence of scene labels.  However, the online demo suggested that bounding boxes were also available for some detected objects.  Installing the updated sdk resolved this issue.

### Current structure

The node portion is meant to interface with the greater GLOBE API system. The main function, "erase", takes as input an image as a base-64 encoded string and outputs an array of two items: [metadata, erasure].  "metadata" is a json object containing the detections from Amazon AWS Rekognition as well as other metadata accumulated during processing.  "erasure" is the modified image (texts and faces blurred) encoded as a base64 string.  If no modifications took place,  the erasure is set to null.

The node portion is made up of the following files:

- eraser.js: contains the main erase function
- collector.js: contains the higher order subroutines in support of the erase function
- models.js: contains the interface to support vector machine models
- engine.js: contains the interface to AWS Rekognition
- studio.js: contains Jimp based image manipulation subroutines
- tools.js: contains miscellaneous general tools
- knobs.js: contains main settings

Two more node files were used for development, but play no role in the erase function itself:

- agent.js: wrapper for passing file paths through the erase function and evaluating performance
- mechanic.js: routines for experimenting with the AWS system

The following json files contain the parameters of support vector machine models developed in Python:

- relevance.json: contains the parameters for the relevance model
- texts.json: contains the parameters for the texts model (not currently in use)
- faces.json: contains the paratemers for the faces model (not currently in use)

There are two main files written in Python:

- machines.py: creates the support vector machine models
- projectors.py: diagnostic console for inspecting evaluation data

### Credentials

Additionally, the program expects a file like the example below called "credentials.json" in which to store AWS account credentials for access to the Rekognition service:

```{ "accessKeyId": "ABCDEFG", "secretAccessKey": "hijklmnopqrstuv", "region": "amazon-region" }```

Note: for some reason the region Northern-CA seems to have higher prices than the others.

### Program Flow

The main program flow is outlined below:

#### Convert to bitmap and strip exif information

The initial input to the erase function is an image encoded as a base-64 string.  If the image was a photo, this string may also encode exif information about the image, including the camera orientation when the photo was taken.  To retain this extra data as well as prevent it from interfering with manipulations of the image, the exif data is stripped from the image and added back in at the final step.

#### Check file size and adjust

AWS has a file size limit of 5 MB, and will reject a larger image.  Based on images tested so far, this seems to be a regular occurrence.  In the case of .png and .bmp files, the file size is well correlated with the size of the image.  The file "quality/quantity.png" shows an example of two images and the file size versus a scaling factor.  It also shows a linear approximation to this relationship.  Though perhaps more tightly fitting functions could be used, the linear approximation has the advantage that it always has a larger file size than the actual file sizes.  This means that using this approximation to extrapolate the required scaling factor to get an image below the 5 MB limit will tend to shrink the image a little bit more than it needs to.  This is considered preferable to not shrinking the image enough, and still triggering an error.

For .jpg images, this relationship does not hold, because the file size does not directly correlate with the image size.  However, these images come with a quality setting, from 0 to 100%.  The file "quality/quality.png" displays the relationship for two images between quality and file size.  It also shows a hyperbolic approximation, in which the relationship between file size and quality is assumed to be y = 1 / a (x - c), where y is the file size, x is the quality, and a and c are two image specific parameters.  These parameters are fit by measuring the file size at a quality of 100 and again at a quality of 50.  The quality needed for a particular file size can then be extrapolated.  As in the png example above, the hyperbolic approximation is consistently higher than the actual file size (at least above a quality of 50).

Alternatively, both of these estimation strategies can be turned off in knobs.js.  The object "estimates" has a setting for both 'jpg' and 'png.'  Switching these to false will employ a brute force approach.  In the case of a jpg, the quality will be lowered 1 percentage point at a time until the file size is below the limit.  In the case of a png, the size of the image will be lowered 1 percentage point (compared to original) until the file size is below the limit.

On the one hand, the brute force method gets the quality (or size) of the image as close to the limit as possible, but takes a little more time to get there.  The estimation method is quicker but falls short of picking the highest quality or size possible.   

Overall, AWS detections do not seem particularly sensitive to image quality.  Five images were sent to AWS at various quality settings and their detections were analyzed.  The data is summarized in "quality/erosion.json" and visualized in "quality/erosion.png".  Each line in the graph represents a particular label, such as "Car", and the confidence of the detection across all quality settings.  The color of the line represents the average confidence.  Most lines are localized into colored bands, because confidence scores are fairly stable across quality settings.  The main exceptions are labels detected with average confidence around 55%.  This is right at the limit of detection for the AWS service.  So labels in this region tend to oscillate between 0 and 55% as they are right on the cusp of detection. 

#### Send to AWS Rekognition for initial detections

Once adjusted for file size, the bitmap of the image is passed to the AWS Rekognition service and the data collected.  Currently, there are four modes detected:

- labels: general service for detecting objects in the image.  Returns a list of objects detected as well a confidence score representing the certainty in the detection. 

- texts: searches for texts in the images, and returns a list of words found as well as the specific location in the image where the text was found.  This information is used for blurring texts in the image.

- faces: searches for faces in the image, and returns various characteristics of the face including the emotion expressed and the location of the face in the image.  This information is used for blurring the faces.

- moderations: searches for inappropriate content in the image, including violence and nudity.  

#### Dice the image into quadrants and send back to AWS

In order to achieve higher detection sensitivity, the image can be diced into quadrants and each quadrant can be fed back through the AWS service.  For example, to increase the resolution by a factor of 2, the image can be chopped into 4 separate quadrants.  Each quadrant can then be scaled up to the original image size, and sent to AWS.  

Unfortunately, this process is limited because objects that are too small to see in the original, but also happen to span a dividing line between quadrants would not get detected on either pass.  Therefore, the image is shifted along a diagonal line with the edges wrapped around.  This diagonalized image is chopped up as before.  See "diagonal/diagonal.jpg" for an example (and "diagonal/original.jpg" for the original image).  The lower right hand corner of this diagonalized image is skipped as it has the image borders running through it along both axes.  Therefore, a resolution increase of 2 means 7 extra scans.  A resolution increase of 4 means 31 extra scans.  In general a resolution of n means 2n^2 - 1 scans.

Detections made from these quadrants are compared with those of the original, and only added to the metadata from the image if they offer new information.  For examples, labels with the same name are not retained, nor are faces or texts that overlap significantly with faces or texts in the original images.  The remaining novel detections are stored in the metadata as "redetections" to keep them separate.  This makes it possible to identify the degree to which higher resolution scans add new information. 

#### Evaluate relevance

A determination of relevance is made with the help of a machine learning algorithm called a "support vector machine."  Essentially, the algorithm takes the labeled data from photos categorized as relevant or irrelevant.  Each separate label gets its own axis in a large (700 or so) dimensional space.  The algorithm then finds a hyperplane that best separates the two categories.  A new photo coming in can be placed in this space, and will naturally be on one side or the other of the line dividing the two categories.  In this case, approved photos from GLOBE Observer protocols are used as example of relevant data, and rejected photos are used as examples of irrelevant data.

The relevance model was developed in Python, and the important parameters are stored in the file "relevance.json."  A 2-dimensional projection of the model is given at "machines/relevance/SVC_linear.png."  This projection shows the distribution of training and holdout data for the model in the 700 dimensional space compressed to the 2 dimensions that best span the distribution.  A text file at "machines/relevance/validation.txt" shows the evaluation data for the model.

If the image is judged by the model as "irrelevant" it is flagged as such and marked for review.

#### Evaluate other flags

There are four other reason that a photo is flagged for review:

- Inappropriate content is detected through the Moderation Labels service, in which case the photo is flagged with "moderations" and sent for review.

- A Person instance is detected amongst the objects detected by the Labels service.  As this is a more sensitive indicator for some types of inappropriate content, the photo is flagged with "persons" and sent for review.

- A Car instance is detected amongst the objects detected by the Labels service.  As car license plates are potentially a threat from an identity theft standpoint, and there were a few instances of license plates not detected by the Texts service, the photo is flagged with "cars" and sent for review.

- Any face is detected by the Faces service.  The photo is flagged with "faces" and sent for review.

#### Blur out faces and texts

Text and face detections come with location information.  Therefore, these specific regions can be blurred with image processing techniques available from the Node.js Jimp module.  Blurring requires a blur radius paramter, essentially identifying how big to blur the image.  Setting this paramter to the height of the blurring box should be adequate to blur the region beyond recognition.  However, at higher blur radii it was found that the process did not always complete.  Therefore the region requiring blurring is first shrunk to a height of 50 pixels.  Blurring at this size is consistently completed.  The region is then expanded back to its original size and pasted onto the original.

During evaluation it was found that some mosquito photographs, particular photos with a picture of a larvae were mistaken for faces taking up most of the image.  Blurring the photo renders the photo useless.  Because of this, if a face is detected that takes up more than 60% of the area of the photograph, it will not be blurred.  The photo is still flagged with "faces," however, so the photo still passes to review.

If any text or face is blurred in the image, the "original" status of the image is changed to "false."

### Note on Asynchronicity

Node.js is a language that takes advantage of asyncronous threads to run processes more efficiently.  In this context, requests for all detections are sent at once and processed in turn.  But AWS will reject a request 5 minutes old or older.  Therefore this program was written to limit the number of scans requested at once, instead waiting for data to come back before making new requests.  

There are two main throttle controls, found in knobs.js.  The "zooms" throttle sets the number of high resolution scans to perform at once.  The "modes" throttle sets the number of modes to scan at once.

As the main function operates on a single image at a time, there is no inherent control over the number of images sent at once, and so bulk implementation may encounter this issue.  A function called "chain" in tools.js may be useful in batch processing. 

### Metadata Example

The current form of the returned metadata is as follows:
```
{
  approve: false,
  flags: [ 'irrelevant', 'persons', 'faces' ],
  original: false,
  detections: {
    Labels: [ [Object], [Object], [Object] ],
    TextDetections: [
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object], [Object],
      [Object], [Object], [Object]
    ],
    FaceDetails: [],
    ModerationLabels: [],
    CelebrityFaces: [],
    UnrecognizedFaces: [ [Object] ],
    OrientationCorrection: 'ROTATE_180'
  },
  redetections: {
    Labels: [
      [Object], [Object],
      [Object], [Object],
      [Object], [Object],
      [Object], [Object],
      [Object]
    ],
    TextDetections: [],
    FaceDetails: [],
    ModerationLabels: [],
    CelebrityFaces: []
  }
}
```


### Evaluation study

In order to evaluate the system performance, 600 humam-approved and 600 human-rejected photos from mid-2020 were run through the erase function and the data collected for evaluation.  Results of this study can be found in this repo at "report/AWSRekognition.pdf."

#### Reconstruction

Because photos take up a lot of space, the 1200 photos used for system evaluation are not included in this repo.  However, in case there is a need to reconstruct the evaluation study, the urls for the particular photos used can be found in "evaluation/urls.json."

The full dataset may be downloaded as follows:

```buildoutcfg
> .load agent.js;
> reconstruct('evaluation/urls.json');
```

Erased copies of the images can be reconstructed from the saved data as follows:,

```buildoutcfg
> update('evaluation/approvals', 'evaluation/approvals_scans');
> update('evaluation/rejections', 'evaluation/rejections_scans');
```
The before and after copies may be merged into comparison images as follows:

```buildoutcfg
> staple('evaluation/approvals', 'evaluation/approvals_scans', 'evaluation/approvals_comparisons');
> staple('evaluation/rejections', 'evaluation/rejections_scans', 'evaluation/rejections_comparisons');
```
Note: In this case, the AWS data has already been generated and is saved in json files in this repo.  It is not necessary to actually perform the AWS detections.  However, for completeness, the following commands would regenerate the data files by sending each one through AWS:

```buildoutcfg
> analyze('evaluation/approvals', 'evaluation/apprvovals_scans');
> analyze('evaluation/rejections', 'evaluation/rejections_scans');

#### Diagnosis

The projectors.py file written in Python contains a console for examining the evaluation results.  Once in a Python terminal, activate the projector with the following:

```buildoutcfg
>>> import python.projectors as projectors
>>> projector = projectors.Projector('evaluation')
```

###### summarize

The latest data may be summarized into smaller records using:

```buildoutcfg
>>> projector.summarize()
```

###### criteria

It is often useful to inspect a subset of the evaluation data.  Several criteria are preprogrammed for this purpose:

```'false_positives', 'false_negatives', 'true_positives', 'true_negatives'```

Also, images with particular flags may be examined:

```'flag faces', 'flag persons'```

Or images with particular detections:

```'has persons', 'has texts'```

Or images with a particular keyword in the image path:

```'name mosquito', 'name land_cover_231'```

Or images with a particular note associated with them:

```'note small_texts', 'note missed_person'```

Or images limited to a particular flag:

```'only faces', 'only cars'```

Or images without a particular flag:

```'not faces', 'not irrelevant'```

Or images with a particular label:

```'label astronomy', 'label animal'```

###### inspect

A particular subset may be inspected as follows:

```>>> projector.inspect('false_negatives', 'flag faces')```

for instance will call up the subset of images that were approved by human reviewers but rejected by the AI system, that were flagged for faces.

Once in inspection mode, the current comparison image will be displayed at "evaluation/diagnosis.jpg."  The inspector responds to these particular commands:

```?? draw persons```

will draw boxes around all persons found.

```?? draw persons 0```

will draw a box around the first person in the list of persons,

```?? clear```

will clear all boxes,

```?? view```

will view a more detailed record,

```?? model relevance```

will display the relevance model results,

```?? note small_face```

will associate the note "small_face" with the photo,

```?? forget small_face```

will remove the note "small_face" from the record,

```?? exit```

will exit the inspector.  Just hitting return will move to the next photo.

###### tally

The tally function displays counts of false_positives, false_negatives, true_positives, and true_negatives.  It is also responsive to the same criteria as for the inspect function, so that:

```>>> projector.tally('name cloud')```

will tally up the counts for the subset of evaluation photos with "cloud" in their path name.

###### notify

The notify function simply counts all notes added to the record, and is called as such:

```>>> projector.notify()```

### Relevance Model

As noted above, the relevance model uses a machine-learning algorithm called a "support vector machine" to classify photos based on the AWS detected object labels and their associated confidences.

#### Reconstruction

The data used for the model can be grabbed from the associated urls as for the evaluation study.  These commands will reconstruct the data sets:

```buildoutcfg
> .load agent.js;
> reconstruct('machines/relevance/urls.json');
```

The machines.py file is responsible for training the model.  A model can be trained by running:

```buildoutcfg
>>> import python.machines as machines
>>> machine = machines.Machine('machines/relevance')
>>> machine.analyze()
```

Assuming the validation results are satisfactory, the newly trained model may be updated by using:

```buildoutcfg
>>> machine.send()
```

By default, this will send the "linear" variation.  However, the validation results present two other variations, "ovo", and "ovr", that differ slightly in the details of calculating classification boundaries.  If the validation results are better for one of these, it may be used instead by:

```buildoutcfg
>>> machine.send('ovo')
``` 

#### New Data

If it is desired to train a model on new data or even new categories, the data files must be arranged as follows:

- machines
    - title
        - supercategory_1
            - category_1.json
            - category_2.json
        - supercateogry_2
            - category_1.json
            - category_2.json
            - category_3.json
            
Then, calling:

```buildoutcfg
>>> machine = Machine('machines/title') 
>>> machine.analyze()
```

will automatically associate the categories with their supercategories.  Assuming the json files are structured in the same way as the original, the model will train on the new data.

There are two other parameters, that help optimize the model.  Setting weights like this:

```buildoutcfg
>>> machine.wieghts = [0.1, 1.0]
```

means the first supercategory (alphabetically) is weighted less than the second supercategory, and can shift the balance between false_positives and false_negatives.

The second parameter:

```buildoutcfg
>>> machine.regularization = 0.5
```

is used to resist outliers in general, and may also impact the model performance.  

Several combinations of these parameters can compared by using:

```buildoutcfg
>>> machine.optimize()
```

### Moderation Study

The images used for the moderation study are stored in this repo, as they are simply screenshots from http://www.shutterstock.com.  Because these images are specifically inappropriate, they are stored in scrambled form so that the images are not recognizable.

#### Reconstruction

To reconstruct the moderation study images, you may unscramble the images as follows:

```buildoutcfg
> .load agent.js;
> unscramble('study/scrambles', 'study/unscrambles');
```

Then, implant the images into the upper left corner of a background image at various heights using:

```buildoutcfg
> implant('study/unscrambles', 'study/background.jpg', 'study/implants');
```

These implanted images were scanned at various resolutions, and the data is stored in json files in this repo.  A summary of all results can be made and stored at "study/moderations.json" with this command:

```buildoutcfg
> moderate();
```

A more compact summary at the optimum resolutions of 1x1 for Moderations and 2x2 for Labels can be made and stored at "study/summary.json" with this command:

```buildoutcfg
> summarize('1x1', '2x2');
```


#### Thank you!

- Matthew Bandel, SSAI, Senior Scientific Programmer
