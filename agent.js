// agent.js to scan photos and blur them

// import tools, studio, engine, scrambler
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');
var engine = require('./engine.js');
var models = require('./models.js');
var collector = require('./collector.js');
var eraser = require('./eraser.js');


// analyze all files from an inward directory and place results in outward directory
function analyze(inward, outward, order=null, block=1, brick=0, focus='', shuffle=false) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // get all paths from list of sizes
        var paths = tools.pathfind(inward);

        // if a sizes file is given
        if (order) {

            // open the file
            sizes = tools.retrieve(order)

            // sort paths accordingly
            paths.sort(function(first, second) {return sizes[second] - sizes[first]})
        }
        // get json file name
        var json = inward + '.json';

        // if json file does not yet exist
        var splittings = inward.split('/');
        var folder = splittings.slice(0, splittings.length - 1).join('/');
        if (!tools.pathfind(folder).includes(json)) {

            // create blank file
            tools.debug('creating file ' + json + '...');
            tools.dump({}, json);
        }
        // get current json file data
        var data = tools.retrieve(json);

        // filter out files already analyzed
        paths = paths.filter(function(path) {return !Object.keys(data).includes(path)});

        // only use paths with the focus word if given
        paths = paths.filter(function(path) {return path.includes(focus)});

        // set leftovers to false if paths is empty
        var leftovers = true;
        if (paths.length < 1) {

            // leftovers is false
            leftovers = false;
        }
        // shuffle paths if indicated
        if (shuffle) {

            // sort randomly
            paths = paths.sort(function(a, b) {return Math.random() - Math.random()});
        }
        // only run a brick's worth
        if (brick > 0) {

            // silce
            paths = paths.slice(0, brick);
        }
        // construct scan functional
        var operator = function(path) {return feed(path, json, outward)};

        // scan batch by batch
        var chaining = tools.chain(operator, paths, block);
        chaining.then(function(outputs) {

            // resolve promise with leftovers status
            resolve(leftovers);
            tools.debug('finished!');
        })
    })
    return promise;
}


// feed a single path through the eraser
function feed(path, json=null, outward=null) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // get current time
        var now = new Date();

        // print status
        tools.debug('scanning ' + path + '...');

        // encode to base64
        var base = tools.encode(path);

        // get extension
        var extension = path.split('.').pop();

        // feed encoding to eraser
        var erasing = eraser.erase(base);
        erasing.then(function(data) {

            // print status
            tools.debug('done erasing ' + path + '.');

            // unpack data
            var [metadata, erasure] = data;

            // set default json
            if (!json) {

                // set to troubleshooting directory
                json = 'troubleshooting/testo.json';
            }
            // add metadata to json file
            var retrieval = tools.retrieve(json);
            retrieval[path] = metadata;

            // rewrite json file
            tools.debug('writing ' + json + '...');
            tools.dump(retrieval, json);

            // create bitmap from original
            var bitmap = Buffer.from(base, 'base64');

            // if original was modified
            if (!metadata.original) {

                // make bitmap from incoming encoding
                bitmap = Buffer.from(erasure, 'base64');
            }
            // load up image
            var imagining = studio.imagine(bitmap);
            imagining.then(function(image) {

                // if not given
                var deposit = 'troubleshooting/testo.' + extension;
                if (outward) {

                    // set deposit
                    deposit = outward + '/' + path.split('/').pop().replace('.', '_erased.');
                }
                // deposit image
                tools.debug('writing ' + deposit + '...');
                image.write(deposit);

                // calculate time
                var later = new Date();
                var duration = (later - now) / 1000;
                tools.debug('took ' + duration + ' seconds.')

                // resolve promise
                resolve();
            })
        })
    })
    return promise;
}


// implant lewd pictures into a background at various heights
function implant(lewds, background, outward, heights=[10, 20, 40, 80, 160, 320]) {

    // get background picture
    var imagining = studio.imagine(background);
    imagining.then(function(scenery) {

        // get all lewd image paths
        var paths = tools.pathfind(lewds);
        paths.forEach(function(path) {

            // get lewd image
            var imaginingii = studio.imagine(path);
            imaginingii.then(function(lewd) {

                // go through each height
                heights.forEach(function(height) {

                    // scale the insert
                    var clone = studio.xerox(lewd);
                    var scaling = studio.scale(clone, height);
                    scaling.then(function(scaled) {

                        // make composite in upper left corner
                        var scene = studio.xerox(scenery);
                        scene.composite(scaled, 0, 0);

                        // deposit
                        var name = path.split('/').pop().replace('.jpg', '');
                        var deposit = outward + '/' + name + '_' + height + '.jpg';
                        tools.debug('writing file ' + deposit + '...');
                        scene.write(deposit);
                    })
                })
            })
        })
    })
    return;
}


// inject particular reprocessed results into evaluation
function inject() {

    // get the datasets
    var corrections = tools.retrieve('troubleshooting/testo.json');
    var approvals = tools.retrieve('evaluation/approvals.json');
    var rejections = tools.retrieve('evaluation/rejections.json');

    // set up datasets
    var datasets = {'approvals': approvals, 'rejections': rejections};

    // go through each path
    var paths = Object.keys(corrections);
    paths.forEach(function(path) {

        // print status
        tools.debug('injecting ' + path + '...');

        // get metadata
        var reprocessing = reprocess(path, smudge=true, data=corrections, stash=false);
        reprocessing.then(function (results) {

            // unpack results
            var [metadata, erasure] = results;

            // get original image
            var imagining = studio.imagine(path);
            imagining.then(function (original) {

                // check erasure
                if (erasure == null) {

                    // set to original
                    erasure = original;
                }
                // find appropriate folder
                var folder = '';
                var batches = ['approvals', 'rejections'];
                batches.forEach(function(batch) {

                    // check for batch name
                    if (path.includes(batch)) {

                        // set folder
                        folder = batch;
                    }
                })
                // correct entry in evaluation data
                datasets[folder][path] = metadata;
                tools.dump(datasets[folder], 'evaluation/' + folder + '.json');

                // deposit erasure
                var deposit = path.replace(folder, folder + '_scans').replace('.', '_erased.');
                erasure.write(deposit);

                // fuse original and erasure
                var fusing = studio.fuse(path, deposit);
                fusing.then(function(fusion) {

                    // deposit comparison
                    var comparison = deposit.replace('_scans', '_comparisons').replace('_erased', '_comparison');
                    fusion.write(comparison);
                })
            })
        })
    })
}


// summarize moderation study results
function moderate() {

    // begin moderations object
    var moderations = {};

    // make list of resolutions
    var resolutions = ['1x1', '1x2', '1x3', '2x1', '2x2', '3x1', '3x3'];

    // make list of sizes
    var sizes = ['320', '160', '80', '40', '20', '10'];

    // go through each trigger (category)
    var retrieval = tools.retrieve('study/triggers.json');
    var triggers = [...new Set(Object.values(retrieval))];
    triggers.forEach(function(trigger) {

        // add trigger to moderations
        moderations[trigger] = {};

        // go through each specific lewd image
        Object.keys(retrieval).forEach(function(lewd) {

            // check for trigger
            if (retrieval[lewd] == trigger) {

                // add to moderations
                moderations[trigger][lewd] = {};

                // load up full scan data
                var unscrambles = tools.retrieve('study/unscrambles.json');

                // go through each resolution, redundant for Full size since only the 1x1 scan matters
                resolutions.forEach(function(resolution) {

                    // check for moderation labels
                    var status = unscrambles[lewd].detections.ModerationLabels.length > 0;

                    // make entry
                    moderations[trigger][lewd][resolution] = {'Full': {'moderations': status}};

                    // check for persons
                    var names = unscrambles[lewd].detections.Labels.map(function(label) {return label.Name});
                    var status = names.includes('Person');

                    // add status
                    moderations[trigger][lewd][resolution]['Full']['persons'] = status;

                    // load implants file
                    var implants = tools.retrieve('study/implants_' + resolution + '.json');

                    // go through each size
                    sizes.forEach(function(size) {

                        // make key
                        var key = lewd.replace('.jpg', '_' + size + '.jpg').replace('unscrambles', 'implants');

                        // check for moderation labels status
                        var results = implants[key].detections.ModerationLabels;
                        results = results.concat(implants[key].redetections.ModerationLabels);
                        var status = results.length > 0;

                        // begin record
                        moderations[trigger][lewd][resolution][size] = {'moderations': status};

                        // check for person detection
                        var results = implants[key].detections.Labels.concat(implants[key].redetections.Labels);
                        var names = results.map(function(label) {return label.Name});
                        var status = names.includes('Person');

                        // add status
                        moderations[trigger][lewd][resolution][size]['persons'] = status;
                    })
                })
            }
        })
    })
    // dump file
    var deposit = 'study/moderations.json';
    tools.debug('writing ' + deposit + '...');
    tools.dump(moderations, deposit);

    // print to screen
    tools.debug(moderations);

    return;
}


// reconstruct a number of photos from urls, storing in directory if given
function reconstruct(inward, number=null, start=0, directory=null) {

    // load urls
    var reconstruction = tools.retrieve(inward);

    // get all paths
    var paths = Object.keys(reconstruction);

    // limit to number if given
    if (number) {

        // slice into paths
        paths = paths.slice(start, start + number);
    }
    // pair each path with its source
    var pairs = paths.map(function(path) {return [path, reconstruction[path]]});

    // correct deposits if directory is given
    if (directory) {

        // add prefix
        pairs = pairs.map(function(pair) {return [directory + '/' + pair[0], pair[1]]});;
    }
    // define chaining operation
    var siphoner = function(pair) {return siphon(pair[1], pair[0])};

    // scan batch by batch
    var chaining = tools.chain(siphoner, pairs, block=20);
    chaining.then(function(outputs) {

        // finish status
        tools.debug('finished!');
    })
    return;
}


// reprocess all images in a folder based on previous data
function reincarnate(folder) {

    // construct folder names
    var inward = folder + '/originals'
    var onward = folder + '/erasures'
    var outward = folder + '/diagnosis/staples'

    // get all paths
    var paths = tools.pathfind(inward);

    // create data file and get result
    var data = tools.retrieve(inward + '.json');

    // begin collecting promises
    var promises = [];

    // for each path
    paths.forEach(function(path) {

        // reprocess it, based on given data
        var reprocessing = reprocess(path, smudge=true, data=data, stash=true, inward=inward, onward=onward);

        // add to promises list
        promises.push(reprocessing);
    })
    // save data once all promises are fulfilled
    Promise.all(promises).then(function() {

        // do stapling
        staple(inward, onward, outward);
    })
    return;
}


// reprocess the AWS detection data of a sample
function reprocess(path, smudge=false, data=null, stash=true, inward=null, onward=null) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // status
        tools.debug('updating ' + path + '...');

        // retrieve data if not given
        if (!data) {

            // get json path
            var splittings = path.split('/');
            var source = splittings.slice(0, splittings.length - 1).join('/') + '.json';

            // retrieve data
            data = tools.retrieve(source);
        }
        // get the report for that path
        var report = {};
        report['detections'] = data[path].detections;
        report['redetections'] = data[path].redetections;

        // determine flags
        var metadata = collector.flag(report);

        // stash metadata
        if (stash) {

            // get troubleshooting data
            var json = 'troubleshooting/testo.json';
            var testing = tools.retrieve(json);

            // add entry and dump
            testing[path] = metadata;
            tools.debug('writing ' + json + '...');
            tools.dump(testing, json);
        }
        // rescan if requested
        erasure = null;
        if (smudge) {

            // load the bitmap into a Jimp image
            var imagining = studio.imagine(path);
            imagining.then(function(image) {

                // collect all boxes to be blurred
                var collection = collector.collect(report, image);

                // and blur them
                var smudging = collector.smudge(collection, image);
                smudging.then(function(erasure) {

                    // if wanting to write to file
                    if (stash) {

                        // save to troubleshooting folder
                        var deposit = 'troubleshooting/testo.jpg';
                        tools.debug('writing ' + deposit + '...');
                        erasure.write(deposit);

                        // if there is an onward directory
                        if (onward) {

                            // save to troubleshooting folder
                            var detour = path.replace(inward, onward).replace('.jpg', '_erased.jpg');
                            tools.debug('writing ' + detour + '...');
                            erasure.write(detour);
                        }
                    }
                    // resolve promise
                    resolve([metadata, erasure]);
                })
            })
        }
        // otherwise
        else {

            // resolve promise
            resolve([metadata, erasure]);
        }
    })
    return promise;
}


// siphon a photo from a url to a folder
function siphon(source, deposit) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // get image from url
        var imagining = studio.imagine(source);
        imagining.then(function(image) {

            // print status
            tools.debug('writing ' + deposit + ' from ' + source + '...');

            // deposit image
            image.write(deposit);

            // resolve with empty output
            resolve('');
        })
    })
    return promise;
}


// analyze two folders back and forthc
function slalom(inward, inwardii, block=2, brick=2, total=0, focus='', count=0, shuffle=true) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // print status
        tools.debug('round ' + count + ' of ' + total + '...');

        // create outgoing paths
        var outward = inward + '_scans';
        var outwardii = inwardii + '_scans';

        // analyze first batch
        var analyzing = analyze(inward, outward, block=block, brick=brick, shuffle=shuffle);
        analyzing.then(function(leftovers) {

            // analyze second batch
            var analyzingii = analyze(inwardii, outwardii, block=block, brick=brick, focus=focus, shuffle=shuffle);
            analyzingii.then(function(leftoversii) {

                // advance count
                tools.debug('count ' + count + ' of ' + total + ':');
                count += 1;

                // check against total rounds
                if (total > 0 && count < total) {

                    // resolve next group
                    resolve(slalom(inward, inwardii, block=block, brick=brick, total=total, focus=focus, count=count, shuffle=shuffle));
                }
                // otherwise
                else {

                    // stop
                    tools.debug('finished!');
                    resolve(null);
                }
            })
        })
    })
    return promise;
}


// staple together images from originals and erasures
function staple(inward, onward, outward, refresh=false) {

    // create set of original paths linked to names
    var paths = tools.pathfind(inward);
    var names = paths.map(function(path) {return [tools.stub(path), path]});
    var originals = Object.fromEntries(names);

    // create set of erasure paths linked to names
    var paths = tools.pathfind(onward);
    var names = paths.map(function(path) {return [tools.stub(path).replace('_erased', ''), path]});
    var erasures = Object.fromEntries(names);

    // try to look in folder
    try {

        // for list of comparisions
        var comparisons = tools.pathfind(outward);
    }
    // otherwise
    catch {

        // set as blank
        var comparisons = [];
    }
    // create links between files
    var links = Object.keys(erasures).map(function(name) {return [name, originals[name], erasures[name]]});
    links.forEach(function(link) {

        // generate deposit name
        var deposit = outward + '/' + link[0] + '_comparison.jpg';

        // check for previous
        if (refresh || !comparisons.includes(deposit)) {

            // staple paths together
            var fusing = studio.fuse(link[1], link[2]);
            fusing.then(function(fusion) {

                // write to file
                tools.debug('writing ' + deposit + ' ...');
                fusion.write(deposit);
            })
        }
    })
    return;
}


// summarize the moderation data at particular resolutions
function summarize(resolution='1x1', resolutionii ='2x2') {

    // begin summary
    var summary = {};

    // set sizes
    var sizes = ['10', '20', '40', '80', '160', '320', 'Full'];

    // retrieve moderation data
    var moderations = tools.retrieve('study/moderations.json');
    var triggers = Object.keys(moderations);
    triggers.forEach(function(trigger) {

        // begin entry
        summary[trigger] = {'Count': Object.keys(moderations[trigger]).length};

        // go through each size
        sizes.forEach(function(size) {

            // assume zero
            var counts = 0;

            // count detections
            Object.keys(moderations[trigger]).forEach(function(key) {

                // check for detections
                var moderation = moderations[trigger][key][resolution][size]['moderations'];
                var person  = moderations[trigger][key][resolutionii][size]['persons'];
                if (moderation || person) {

                    // increase count
                    counts++;
                }
            })
            // add entry
            summary[trigger][size] = counts;
        })
    })
    // dump summary file
    deposit = 'study/summary.json';
    tools.debug('writing ' + deposit + '...');
    tools.dump(summary, deposit);

    // print to screen
    tools.debug(summary);

    return;
}


// update the analysis of images and files from an inward directory to an outward directory
function update(inward, outward=null) {

    // retrieve json file
    var json = inward + '.json';
    var reports = tools.retrieve(json);

    // get all paths
    var paths = Object.keys(reports);

    // if no outward folder is specified
    var smudge = true;
    if (!outward) {

        // don't smudge
        smudge = false;
    }
    // begin data collection object
    var data = {};

    // go through each path
    var promises = [];
    paths.forEach(function(path, index) {

        // process the path
        var reprocessing = reprocess(path, smudge=smudge, reports, stash=false);
        promises.push(reprocessing);
        reprocessing.then(function(results) {

            // print status
            tools.debug('processed ' + index + ' of ' + paths.length + '.');

            // unpack results
            var [metadata, erasure] = results;

            // add results to data and save
            data[path] = metadata;

            // if there is an outward directory
            if (outward) {

                // write file
                var deposit = path.replace(inward, outward).replace('.jpg', '_erased.jpg');
                tools.debug('writing ' + deposit + '...');
                erasure.write(deposit);
            }
        })
    })
    // save data once all promises are fulfilled
    Promise.all(promises).then(function() {

        // dump into json
        tools.debug('writing ' + json + '...');
        tools.dump(data, json);
    })
    return;
}


// unconceal all scrambled images in one directory to another
function unscramble(inward, outward) {

    // get all paths from the inward directory
    var paths = tools.pathfind(inward);
    paths.forEach(function(path) {

        // print status
        tools.debug('unscrambling ' + path + '...');

        // unscramble each image (same algorithm as initial scrambling)
        var scrambling = studio.scramble(path);
        scrambling.then(function(scrambled) {

            // get deposit path
            var deposit = path.replace(inward, outward);

            // write scrambled file
            tools.debug('writing ' + deposit + '...');
            scrambled.write(deposit);
        })
    })
    return;
}


// set exports
exports = {'analyze': analyze, 'feed': feed, 'implant': implant, 'inject': inject, 'moderate': moderate};
Object.assign(exports, {'reconstruct': reconstruct, 'reprocess': reprocess, 'siphon': siphon, 'slalom': slalom});
Object.assign(exports, {'staple': staple, 'summarize': summarize, 'unscramble': unscramble, 'update': update});
module.exports = exports;

// status
console.log('agent loaded.');
