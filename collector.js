// collector.js to collect detections for erasing and evaluating

// import support modules
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');
var engine = require('./engine.js');
var models = require('./models.js');


// collect all bounding boxes to be blurred
function collect(report, image) {

    // begin collection
    var collection = [];

    // go through each detection mode
    knobs.modes.forEach(function(mode) {

        // go through both detections and redetections
        var tiers = ['detections', 'redetections'];
        tiers.forEach(function(tier) {

            // go through each individual detection for the mode
            var reservoir = report[tier][engine.reservoirs.get(mode)];
            reservoir.forEach(function(detection) {

                // if the criterion for blurring is met
                if (engine.conditions.get(mode)(detection)) {

                    // collect the bounding boxes
                    var boxes = engine.boxes.get(mode)(detection);
                    boxes.forEach(function(box) {

                        // and add each box to the collection, retaining mode information
                        collection.push({'box': box, 'mode': mode});
                    })
                }
            })
        })
    })
    return collection;
}


// embellish main full size detections with those at higher resolution, weeding out duplicates
function embellish(detections, redetections, mode) {

    // get detection reservoirs
    var primaries = detections[engine.reservoirs.get(mode)];
    var zooms = redetections.map(function(redetection) {return redetection[engine.reservoirs.get(mode)]});

    // merge all secondaries
    var secondaries = [];
    zooms.forEach(function(zoomed) {

        // from each quadrant
        zoomed.forEach(function(secondary) {

            // into one list
            secondaries.push(secondary);
        })
    })
    // sort secondaries by confidence
    secondaries = secondaries.sort(function(a, b) {return b.Confidence - a.Confidence});

    // weed out duplicate detections
    var uniques = [];
    secondaries.forEach(function(secondary) {

        // compare with primaries and unique secondaries so far
        var combined = primaries.concat(uniques);

        // if the secondary is not deemed equivalent to any detection so far
        if (!combined.some(function(primary) {return engine.equivalences.get(mode)(primary, secondary)})) {

            // retain the secondary
            uniques.push(secondary);
        }
    })
    // gather uniques as embellishments
    var field = engine.reservoirs.get(mode);
    var embellishments = {};
    embellishments[field] = uniques;

    // assemble the data object
    var data = {'detections': detections, 'redetections': embellishments};

    return data;
}


// construct metadata based on detections
function flag(report) {

    // begin metadata, assuming the image will be approved, with no flags, and no modifications
    var metadata = {'approve': true, 'flags': [], 'original': true};

    // if the image is deemed irrelevant by the relevance model (assuming this test is switched on)
    var relevance = models.predict('relevance', report);
    if (relevance[0][0].includes('irrelevant') && knobs.switchboard['irrelevant']) {

        // send the image to review, and flag as irrelevant
        metadata.approve = false;
        metadata.flags.push('irrelevant');
    }
    // if a person is detected in the scene (assuming this test is switched on)
    var person = models.spy(report, ['Person', 'Human']);
    if (person && knobs.switchboard['persons']) {

        // send the image to review and flag as having persons
        metadata.approve = false;
        metadata.flags.push('persons');
    }
    // if a car is detected in the scene (assuming this test is switched on)
    var car = models.spy(report, ['Car']);
    if (car && knobs.switchboard['cars']) {

        // sned the image to review and flag as having cars
        metadata.approve = false;
        metadata.flags.push('cars');
    }
    // if other suspicious objects are detected in the scene (assuming this test is switched on)
    var suspicious = models.spy(report, knobs.suspicions);
    if (suspicious && knobs.switchboard['suspicious']) {

        // send the image to review and flag as being suspicious
        metadata.approve = false;
        metadata.flags.push('suspicious');
    }
    // if inappropriate content is detected through the moderations mode (assuming this test is switched on)
    var moderations = report.detections.ModerationLabels.concat(report.redetections.ModerationLabels);
    if (moderations.length > 0 && knobs.switchboard['moderations']) {

        // send the image to review and flag as having moderations
        metadata.approve = false;
        metadata.flags.push('moderations');
    }
    // if faces are detected by the faces mode
    var faces = report.detections.FaceDetails.concat(report.redetections.FaceDetails);
    if (faces.length > 0) {

        // and the faces test is switched on
        if (knobs.switchboard['faces']) {

            // send the image to review and flag as having faces
            metadata.approve = false;
            metadata.flags.push('faces');
        }
        // however, only blur the face if the right condition is met
        if (faces.some(function(face) {return engine.conditions.get('faces')(face)})) {

            // in which case the image will not be an original
            metadata.original = false;
        }
    }
    // if texts are detected by the texts mode
    var texts = report.detections.TextDetections.concat(report.redetections.TextDetections);
    if (texts.length > 0) {

        // and the test is switched on
        if (knobs.switchboard['texts']) {

            // send the image to review and flag as having texts
            metadata.approve = false;
            metadata.flags.push('texts');
        }
        // regardless, the texts will be blurred so the image will be modified
        metadata.original = false;
    }
    // add the detections to the metadata
    metadata.detections = report.detections;
    metadata.redetections = report.redetections;

    return metadata;
}


// reorient zoomed boundary boxes according to full image coordinates
function reorient(detections, mode, quadrant) {

    // for each detection in the reservoir of detections
    var reservoir = detections[engine.reservoirs.get(mode)];
    reservoir.forEach(function(detection) {

        // if the detection has a bounding box
        if ('BoundingBox' in detection) {

            // adjust the bounding box coordinates
            detection.BoundingBox = studio.budge(detection.BoundingBox, quadrant);
        }
        // if the detection has a geometry
        if ('Geometry' in detection) {

            // adjust the bounding box coordinates
            detection.Geometry.BoundingBox = studio.budge(detection.Geometry.BoundingBox, quadrant);

            // and adjust the polygon coordinates
            detection.Geometry.Polygon = studio.nudge(detection.Geometry.Polygon, quadrant);
        }
        // if the detection has instances
        if ('Instances' in detection && detection.Instances.length > 0) {

            // go through each instance
            var instances = detection.Instances;
            instances.forEach(function(instance) {

                // if there is a bounding box
                if ('BoundingBox' in instance) {

                    // adjust the bounding box coordinates
                    instance.BoundingBox = studio.budge(instance.BoundingBox, quadrant);
                }
            })
        }
    })
    return detections;
}


// make all AWS detections for one mode at all resolutions
function scrutinize(mode, bitmap) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // make original detection at full size
        var detecting = engine.detect(mode, bitmap);
        detecting.then(function(detections) {

            // make image from bitmap
            var imagining = studio.imagine(bitmap);
            imagining.then(function(image) {

                // correct original detection bounding boxes in case of negative width or height
                var faux = {'resolution': [1, 1], 'shift': [0, 0], 'image': image, 'Left': 0, 'Top': 0};
                detections = reorient(detections, mode, faux);

                // get resolution and map boxes for high resolution quadrants
                var resolution = knobs.resolutions.get(mode);
                var boxes = studio.dice(...resolution);

                // if brakes are applied (only expected during development)
                if (knobs.brakes) {

                    // only use the first upper left corner box
                    boxes = boxes.slice(0, 1);
                }
                // for each box
                var quadrants = [];
                boxes.forEach(function(box) {

                    // punch out the quadrant from the image and add to the list
                    var quadrant = {}
                    Object.assign(quadrant, box);
                    Object.assign(quadrant, {'resolution': resolution, 'shift': [0, 0], 'image': image});
                    quadrants.push(quadrant);
                })
                // calculate the degree to shift the image in order to capture objects split by quadrant boundaries
                var shift = [1 / (2 * resolution[0]), 1 / (2 * resolution[1])];

                // only apply the shift if it is less than 0.5 (otherwise the resolution is 1 and no shift is needed)
                shift = shift.map(function(entry) {return entry * (entry < 0.5)});

                // make an image shifted along its diagonal
                var diagonalizing = studio.diagonalize(bitmap, shift);
                diagonalizing.then(function(diagonal) {

                    // punch out similar quadrants as with the original image (skipping the lower right corner)
                    boxes.slice(0, boxes.length - 1).forEach(function(box) {

                        // punch out the quadrant from the diagonal image and add to the list
                        var quadrant = {};
                        Object.assign(quadrant, box);
                        Object.assign(quadrant, {'resolution': resolution, 'shift': shift, 'image': diagonal});
                        quadrants.push(quadrant);
                    })
                    // set up zoomer to zoom in on a particular quadrant and make AWS detections
                    var zoomer = function(quadrant) {return zoom(quadrant, mode)};

                    // process quadrants in batches to avoid AWS timeout errors
                    var chaining = tools.chain(zoomer, quadrants, chunk=knobs.throttle['zooms']);
                    chaining.then(function(redetections) {

                        // embellish the full size data with novel high resolution detections
                        var data = embellish(detections, redetections, mode);

                        // and resolve the promise
                        resolve(data);
                    })
                })
            })
        })
    })
    return promise;
}


// blur the image at all appropriate bounding boxes
function smudge(boxes, image) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // clone the image
        var clone = studio.xerox(image);

        // for each bounding box
        var snippets = [];
        var promises = [];
        boxes.forEach(function(box) {

            // shrink, blur, and enlarge again the particular area of the image
            var bouncing = studio.bounce(box, clone);
            promises.push(bouncing);
            bouncing.then(function(snippet) {

                // and add to the list
                snippets.push(snippet);
            })
        })
        // once all snippets have been blurred
        Promise.all(promises).then(function() {

            // superimpose each blurred snippet
            snippets.forEach(function(member) {

                // onto the image
                var left = Math.round(member.box.Left * clone.bitmap.width);
                var top = Math.round(member.box.Top * clone.bitmap.height);
                clone.composite(member.snippet, left, top);
            })
            // and resolve the promise
            resolve(clone);
        })
    })
    return promise;
}


// zoom into a quadrant of the image and get the high resolution detections
function zoom(quadrant, mode) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // punch out the quadrant of the image
        var image = quadrant.image;
        var punchout = studio.punch(quadrant, image);

        // enlarge so that the punchout is at least as big as original in both dimensions
        var factor = Math.max(1, quadrant.resolution[0] / quadrant.resolution[1]);
        var size = quadrant.image.bitmap.height * factor;
        var scaling = studio.scale(punchout, size);
        scaling.then(function(scaled) {

            // convert the image to a buffer
            var buffing = studio.buff(scaled);
            buffing.then(function(buffer) {

                // send to AWS for detections
                var detecting = engine.detect(mode, buffer);
                detecting.then(function(detections) {

                    // reorient the detections based on full image coordinates
                    var reorientations = reorient(detections, mode, quadrant);

                    // and resolve the promise
                    resolve(reorientations);
                })
            })
        })
    })
    return promise;
}


// set exports
exports = {'collect': collect, 'embellish': embellish};
Object.assign(exports, {'flag': flag, 'reorient': reorient});
Object.assign(exports, {'scrutinize': scrutinize, 'smudge': smudge, 'zoom': zoom});
module.exports = exports;

// status
console.log('collector loaded.')
