// engine.js to interface with AWS Rekognition engine

// import and configure AWS using credentials file
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./credentials.json');
var rekognition = new AWS.Rekognition();

// import support modules
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');

// define AWS file size limit in bytes (5 MB)
var limit = 5242880;

// define different detectors for each mode
var detectors = new Map();
detectors.set('labels', function(image, callback) {rekognition.detectLabels(image, callback)});
detectors.set('texts', function(image, callback) {rekognition.detectText(image, callback)});
detectors.set('faces', function(image, callback) {rekognition.detectFaces(image, callback)});
detectors.set('moderations', function(image, callback) {rekognition.detectModerationLabels(image, callback)});
detectors.set('celebrities', function(image, callback) {rekognition.recognizeCelebrities(image, callback)});

// define submission requests for each mode
var requests = new Map();
requests.set('labels', function(bitmap) {return {"Image": {"Bytes": bitmap}}});
requests.set('texts', function(bitmap) {return {"Image": {"Bytes": bitmap}}});
requests.set('faces', function(bitmap) {return {"Image": {"Bytes": bitmap}, "Attributes": ["ALL"]}});
requests.set('moderations', function(bitmap) {return {"Image": {"Bytes": bitmap}, "MinConfidence": 50}});
requests.set('celebrities', function(bitmap) {return {"Image": {"Bytes": bitmap}}});

// define different detection designations for each mode
var reservoirs = new Map();
reservoirs.set('labels', 'Labels');
reservoirs.set('texts', 'TextDetections');
reservoirs.set('faces', 'FaceDetails');
reservoirs.set('moderations', 'ModerationLabels');
reservoirs.set('celebrities', 'CelebrityFaces');

// define different bounding box pathways for each mode
var boxes = new Map();
boxes.set('labels', function(detection) {return detection.Instances.map(function(instance) {return instance.BoundingBox})});
boxes.set('texts', function(detection) {return [studio.bound(detection.Geometry.Polygon)]});
boxes.set('faces', function(detection) {return [detection.BoundingBox]});

// define different blur conditions for each mode
var conditions = new Map();
conditions.set('labels', function(detection) {return false});
conditions.set('texts', function(detection) {return detection.Type === 'LINE'});
conditions.set('faces', function(detection) {return detection.BoundingBox.Width * detection.BoundingBox.Height < 0.60});
conditions.set('moderations', function(detection) {return false});
conditions.set('celebrities', function(detection) {return false});

// define different summary tags for each mode
var tags = new Map();
tags.set('labels', function(detection) {return detection.Name});
tags.set('texts', function(detection) {return detection.DetectedText});
tags.set('faces', function(detection) {return detection.Emotions[0].Type});
tags.set('moderations', function(detection) {return detection.Name});
tags.set('celebrities', function(detection) {return detection.Name});

// define equivalence relations for each mode
var equivalences = new Map();
equivalences.set('labels', function(primary, secondary) {return studio.equate(primary, secondary)});
equivalences.set('texts', function(primary, secondary) {return studio.overlap(primary, secondary, 'texts') > 0.80});
equivalences.set('faces', function(primary, secondary) {return studio.overlap(primary, secondary, 'faces') > 0.80});
equivalences.set('moderations', function(primary, secondary) {return studio.equate(primary, secondary)});
equivalences.set('celebrities', function(primary, secondary) {return studio.equate(primary, secondary)});


// send the image to AWS for one mode of Rekognition analysis
function detect(mode, bitmap) {

    // make a promise
    var promise = new Promise(function (resolve, reject) {

        // shrink bitmap if greater than 5MB
        var shrinking = studio.shrink(bitmap, limit);
        shrinking.then(function(shrunk) {

            // call AWS
            tools.debug('detecting [' + mode + ']...', 2);
            detectors.get(mode)(requests.get(mode)(shrunk), function (error, data) {

                // check for error
                if (error) {

                    // print error
                    tools.debug('Uh-Oh! Error detecting [' + mode + ']', 1);
                    tools.debug(error, 1);
                }
                // otherwise
                if (!error) {

                    // print status
                    tools.debug(data, 3);
                    tools.debug('[' + mode + '] resolved.', 1);

                    // for each detection
                    var detections = data[reservoirs.get(mode)];
                    detections.forEach(function(detection) {

                        // print to screen
                        var tag = tags.get(mode)(detection);
                        tools.debug('found [' + mode + ']: ' + tag, 3);
                    })
                    // and resolve the promise
                    resolve(data);
                }
            })
        })
    })
    return promise;
}


// set exports
exports = {'AWS': AWS, 'limit': limit, 'detectors': detectors, 'requests': requests};
Object.assign(exports, {'reservoirs': reservoirs, 'boxes': boxes, 'conditions': conditions, 'tags': tags});
Object.assign(exports, {'equivalences': equivalences, 'detect': detect});
module.exports = exports;

// status
console.log('engine loaded.');
