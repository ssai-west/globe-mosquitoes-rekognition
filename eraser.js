// erase.js to scan photos, gather data, and blur faces and texts

// import support modules
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');
var engine = require('./engine.js');
var models = require('./models.js');
var collector = require('./collector.js');


// send a base64 encoded image to AWS Rekognition, and return a base64 encoded, blurred image with metadata
function erase(base) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // convert base64 encoding into bits
        var bits = Buffer.from(base, 'base64');

        // peel off exif information as it does not survive cloning
        var peeling = studio.peel(bits);
        peeling.then(function(peels) {

            // unpack result
            var [exif, bitmap] = peels;

            // set up scrutinzer operator for making AWS detections for one mode at all resolutions
            var scrutinizer = function(mode) {return collector.scrutinize(mode, bitmap)};

            // process modes in batches limited by throttle to avoid timeout errors
            var chaining = tools.chain(scrutinizer, knobs.modes, chunk=knobs.throttle['modes']);
            chaining.then(function(data) {

                // begin detection report
                var report = {'detections': {}, 'redetections': {}};

                // combine all modes into one set of full size detections and one of high resolution redetections
                data.forEach(function(datum) {

                    // assign detections and redetections
                    Object.assign(report.detections, datum.detections);
                    Object.assign(report.redetections, datum.redetections);
                })
                // evaluate detection data and construct metadata
                var metadata = collector.flag(report);

                // load the bitmap into a Jimp image
                var imagining = studio.imagine(bitmap);
                imagining.then(function(image) {

                    // collect all bounding boxes to be blurred
                    var collection = collector.collect(report, image);

                    // and blur them
                    var smudging = collector.smudge(collection, image);
                    smudging.then(function(smudged) {

                        // add original exif data to the blurred image
                        smudged._exif = exif;

                        // make bites buffer out of erased image
                        var buffing = studio.buff(smudged);
                        buffing.then(function(buffer) {

                            // and convert to base64
                            var erasure = buffer.toString('base64');

                            // but set erased image to null if no erasures took place
                            if (metadata.original) {

                                // set to null
                                erasure = null;
                            }
                            // resolve the promise
                            resolve([metadata, erasure]);
                        })
                    })
                })
            })
        })
    })
    return promise;
}


// set exports
exports = {'erase': erase};
module.exports = exports;

// status
console.log('eraser loaded.')