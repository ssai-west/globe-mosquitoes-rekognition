// knobs.js for main project settings

// set debugging level to control output quantity, 0 for none, 3 for all
var debugging = 2

// set blur degrees for each mode
var degrees = {'texts': 25, 'faces': 7, 'labels': 10};

// set brakes to true to limit higher resolution scans to upper left corner (during development)
var brakes = false;

// set list of detection modes
var modes = ['labels', 'texts', 'faces', 'moderations'];

// define zoom resolution for each detection type
var resolutions = new Map();
resolutions.set('labels', [1, 1]);
resolutions.set('texts', [1, 1]);
resolutions.set('faces', [1, 1]);
resolutions.set('moderations', [1, 1]);
resolutions.set('celebrities', [1, 1]);

// define throttles for limiting concurrent submissions (to avoid AWS timeout errors)
var throttle = {'zooms': 7, 'modes': 4};

// define custom suspicious labels
var suspicions = ['Knife', 'Blood', 'Gun'];

// define whether to use estimation functions for file sizes exceeding the 5MB limit
var estimates = {'png': true, 'jpg': true};

// define switchboard for various tests
var switchboard = {'irrelevant': true, 'moderations': true, 'persons': true, 'cars': true, 'faces': true};
Object.assign(switchboard, {'texts': false, 'suspicious': false});


// set exports
exports = {'debugging': debugging, 'degrees': degrees, 'brakes': brakes, 'modes': modes, 'resolutions': resolutions};
Object.assign(exports, {'throttle': throttle, 'suspicions': suspicions, 'switchboard': switchboard});
Object.assign(exports, {'estimates': estimates})
module.exports = exports;

// status
console.log('knobs loaded.');