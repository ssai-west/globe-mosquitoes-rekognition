// mechanic.js to calibrate blur degrees from screenshots

// import tools, studio, engine
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');
var engine = require('./engine.js');
var models = require('./models.js');


// calibrate by running blurred images back through detector
function calibrate(mode, incoming) {

    // begin data
    var data = {};

    // go through each file in the incoming directory
    var paths = tools.pathfind(incoming);
    paths.forEach(function(path) {

        // detect
        var detecting = engine.detect(mode, path);
        detecting.then(function(detections) {

            // assume no detection
            var detected = false;

            // for each detection
            var reservoir = detections[engine.reservoirs.get(mode)];
            reservoir.forEach(function(detection) {

                // detection was found
                detected = true;
            })
            // get height and blur degree
            var stub = path.split('.')[0];
            var splittings = stub.split('_');
            var length = splittings.length;
            var height = splittings[length - 2];
            var degree = splittings[length - 1];

            // make datum
            var datum = {'height': height, 'degree': degree, 'detected': detected};
            data[name] = datum;

            // write data to file
            var deposit = incoming + '.json';
            tools.debug('writing file ' + deposit + '...', 1);
            tools.dump(data, deposit);
        })
    })
    return;
}


// measure the distance between pixels
function caliper(pixels, pixelsii) {

    // measure distances between pixel colors
    var distance = 0;
    ['r', 'g', 'b'].forEach(function(channel) {

        // go through each pixel
        [...Array(pixels.length).keys()].forEach(function(index) {

            // add the squared distance
            distance += (Number(pixels[index][channel]) - Number(pixelsii[index][channel])) ** 2;
        })
    })
    return distance;
}


// codify the attributes of an image into a string
function codify(sample) {

    // get code
    var code = sample.width.toString() + sample.height.toString();

    return code;
}


// collect punched out detections from incoming directory into outgoing directory
function collect(mode, incoming, outgoing) {

    // get all files from incoming directory
    var paths = tools.pathfind(incoming);

    // collect detections from each image
    paths.forEach(function(path) {

        // get image with Jimp
        var imagining = studio.imagine(path);

        // get all detections from AWS
        var detecting = engine.detect(mode, path);

        // access image
        imagining.then(function(image) {

            // access detections
            detecting.then(function(detections) {

                // begin counter
                var counter = 1;

                // for each detection
                var reservoir = detections[engine.reservoirs.get(mode)];
                reservoir.forEach(function(detection) {

                    // only if type is correct
                    if (engine.conditions.get(mode)(detection)) {

                        // display detection
                        tools.debug(detection, 2);

                        // punchout each image
                        var box = engine.boxes.get(mode)(detection);
                        var snippet = studio.punch(box, image);

                        // create file tag
                        var tag = engine.tags.get(mode)(detection);

                        //var word = 'face'
                        var stub = path.split('.')[0].replace(incoming, outgoing);
                        var deposit = stub + '_' + mode + '_' + tag + '_' + counter + '.jpg';
                        counter++;

                        // write file
                        tools.debug('writing ' + deposit + '...', 1);
                        snippet.write(deposit);
                    }
                })
            })
        })
    })
    return;
}


// conceal all images
function conceal(inward, outward) {

    // get all paths from the inward
    var paths = tools.pathfind(inward);
    paths.forEach(function(path) {

        // scramble each path
        var scrambling = studio.scramble(path);
        scrambling.then(function(scrambled) {

            // get deposit path
            var deposit = path.replace(inward, outward);

            // write scrambled file
            scrambled.write(deposit);
        })
    })
    return;
}


// convert pngs in incoming directory to jpgs in outgoing directory
function convert(incoming, outgoing) {

    // get all files from incoming directory
    var paths = tools.pathfind(incoming);

    // convert all to jpgs
    paths.forEach(function(path) {

        // convert
        studio.peg(path, outgoing);
    })
    return;
}


// designate all files in the incoming directory by their top label
function designate(incoming, outgoing) {

    // get all files from incoming directory
    var paths = tools.pathfind(incoming);

    // begin counter
    var counter = new Map;

    // for each name
    paths.forEach(function(path) {

        // base 64 encode
        var bitmap = tools.bite(path);
        var detecting = engine.detect('labels', bitmap);

        // when the promise is ready
        detecting.then(function(detections) {

            // get top label and map it
            var label = detections.Labels[0].Name;

            // add key to counter
            if (counter.has(label) === false) {
                counter.set(label, 0);
            }
            // increment by one
            counter.set(label, counter.get(label) + 1);

            // construct new file name
            var deposit = outgoing + '/' + label + '_' + counter.get(label) + '.jpg';
            tools.debug('writing file ' +  deposit + '...', 1);

            // get image
            var imagining = studio.imagine(path);
            imagining.then(function(image) {

                // resave file
                image.write(deposit);
            })
        })
    })
    return;
}


// erode the quality of an image
function erode(path) {

    // get the image
    var imagining = studio.imagine(path);
    imagining.then(function(image) {

        // define qualities
        var qualities = [100, 99, 98, 97, 96, 95, 90, 85, 80, 75, 70, 60, 50, 40, 30, 20, 10];
        qualities.forEach(function(quality) {

            // set quality
            image.quality(quality);
            var deposit = 'quality/testing/' + tools.stub(path) + '_' + quality + '.jpg';
            image.write(deposit);
        })
    })
    return;
}


// generate training data for svm models
function generate(model) {

    // assign generator functions
    var generators = {};
    generators['texts'] = models.textualize;
    generators['faces'] = models.facialize;

    // grab approvals and rejections data
    var approvals = tools.retrieve('evaluation/approvals.json');
    var rejections = tools.retrieve('evaluation/rejections.json');

    // get path names for trues and falses
    var paths = tools.pathfind('machines/model/true/true'.replace('model', model));
    var pathsii = tools.pathfind('machines/model/false/false'.replace('model', model));

    // make json file paths
    var json = 'machines/model/true/true.json'.replace('model', model);
    var jsonii = 'machines/model/false/false.json'.replace('model', model);

    // start data collection
    var trues = {};
    var falses = {};

    // bundle up attributes
    var bundle = [paths, json, trues];
    var bundleii = [pathsii, jsonii, falses];

    // go through each bundle
    [bundle, bundleii].forEach(function(member) {

        // unpack member
        var [names, deposit, collection] = member;

        // go through each name
        names.forEach(function(name) {

            // get the name stub
            var stub = name.split('/').pop();

            // find it in approvals
            if (Object.keys(approvals).some(function(key) {return key.includes(stub)})) {

                // get record
                var identifier = 'evaluation/approvals/' + stub;
                var record = approvals[identifier];
            }
            // or rejections
            if (Object.keys(rejections).some(function(key) {return key.includes(stub)})) {

                // get record
                var identifier = 'evaluation/rejections/' + stub;
                var record = rejections[identifier];
            }
            // get samples from detections and redetections
            var samples = [];
            var tiers = ['detections', 'redetections'];
            tiers.forEach(function(tier) {

                // get the detections
                var reservoir = record[tier][engine.reservoirs.get(model)];
                reservoir.forEach(function(detection) {

                    // check conditions
                    if (engine.conditions.get(model)(detection)) {

                        // make sample and add to list
                        var sample = generators[model](detection, tier);
                        samples.push(sample);
                    }
                })
            })
            // add last entry to sample detections
            collection[name] = samples.pop();

        // make file
        tools.dump(collection, deposit);
        })
    })
    return;
}


// identify images by some image tag
function identify(inward, outward, number=100, chunk=50) {

    // open urls file and identities file
    var paths = tools.retrieve(inward);
    var identities = tools.retrieve(outward);

    // get pool of paths
    var pool = Object.keys(paths).map(function(path) {return path.replace(' ', '')});

    // remove those already checked
    //pool = pool.filter(function(path) {return !Object.keys(identities).includes(path)});

    // get pool length
    tools.debug('pool length: ' + pool.length);

    // adjust size
    pool = pool.slice(0, number);

    var chaining = tools.chain(tag, pool, chunk=chunk);
    chaining.then(function(outputs) {

        // add outputs identifiers
        outputs.forEach(function(output) {

            // add to record
            var [path, identifier] = output;
            tools.debug(path);
            identities[path] = identifier;
        })
        // write file
        tools.debug('writing ' + outward + ' ...');
        tools.dump(identities, outward);
    })
    return;
}


// investigate exif data
function investigate(inward) {

    // open exifs
    var exifs = tools.retrieve('machines/relevance/exifs.json');

    // get paths
    var paths = tools.pathfind(inward);
    paths.forEach(function(path) {

        // get image
        var imagining = studio.imagine(path);
        imagining.then(function(image) {

            // attach exif data
            exifs[path] = image._exif;

            // save file
            tools.dump(exifs, 'machines/relevance/exifs.json');
        })
    })
    return;
}


// match urls
function match(inward, onward, outward, criterion=6000) {

    // open paths
    var paths = tools.retrieve(inward);

    // open urls and reverse
    var urls = tools.retrieve(onward);

    // status
    tools.debug('paths: ' + Object.keys(paths).length);
    tools.debug('urls: ' + Object.keys(urls).length);

    // define swaps
    swaps = {};
    entries = Object.entries(urls);
    entries.forEach(function(entry) {

        // add swap
        var code = codify(entry[1]);
        if (!Object.keys(swaps).includes(code)) {

            // begin entry
            swaps[code] = [];
        }
        swaps[code].push(entry[0]);
    })
    // status
    tools.debug('swaps: ' + Object.keys(swaps).length)

    // get matches
    var matches = {};

    // make matches
    Object.keys(paths).forEach(function(path) {

        // add swap
        var code = codify(paths[path]);
        if (Object.keys(swaps).includes(code)) {

            // copy set of matching sizes
            prospects = swaps[code].slice(0, swaps[code].length);

            // get pixel distances
            distances = prospects.map(function(prospect) {return [prospect, caliper(urls[prospect].pixels, paths[path].pixels)]});

            // sort by closest distance
            distances = distances.sort(function(a, b) {return a[1] - b[1]});

            // check top distance
            if (distances[0][1] < criterion) {

                // add to matches
                matches[path] = distances[0][0];
            }
            // otherwise view
            else {

//                // print
//                tools.debug(path);
//                tools.debug(distances[0]);
            }
        }
    })
    // count matches
    tools.debug(Object.keys(matches).length + ' matches.');

    // write file
    tools.debug('writing ' + outward + '...');
    tools.dump(matches, outward);

    return;
}


// make all superimpositions of a moderated image onto a background image
function moderate(lewd, background, outward) {

    // get background picture
    var imagining = studio.imagine(background);
    imagining.then(function(image) {

        // get height
        var height = image.bitmap.height;

        // determine set of heights
        var chunk = Math.round(height / 50);
        var heights = [];
        for (var i = 1; i <= 25; i++) {

            // get first 25 heights
           heights.push(i * chunk);
        }
        // set heights to constants
        var heights = [10, 20, 40, 80, 160, 320];

        // get next image
        var imaginingii = studio.imagine(lewd);
        imaginingii.then(function(insert) {

            // go through each height
            heights.forEach(function(height) {

                // scale the insert
                var clone = studio.xerox(insert);
                var scaling = studio.scale(clone, height);
                scaling.then(function(scaled) {

                    // make composite in upper left corner
                    var clone = studio.xerox(scaled);
                    clone.composite(scaled, 0, 0);

                    // deposit
                    var name = lewd.split('/').pop().replace('.jpg', '');
                    var deposit = outward + '/' + name + '_' + height + '.jpg';
                    tools.debug('writing file ' + deposit + '...');
                    clone.write(deposit);
                })
            })
        })
    })
}


// rescale all images in incoming directory to a height given by size
function normalize(incoming, outgoing, height) {

    // get all files from incoming directory
    var paths = tools.pathfind(incoming);

    // scale all images
    paths.forEach(function(path) {

        // get image with Jimp
        var imagining = studio.imagine(path);

        // access data
        imagining.then(function(image) {

            // calculate width
            var width = Math.round(height * image.bitmap.width / image.bitmap.height);

            // resize based on given height
            tools.debug('size: ' + image.bitmap.width + ' x ' + image.bitmap.height, 1);
            tools.debug('resizing ' + path + ' to ' + width + ' x ' + height + '...', 1);
            image.resize(width, height);

            // write to outgoing directory
            var deposit = path.replace(incoming, outgoing);
            tools.debug('writing ' + deposit + '...', 1);
            image.write(deposit);
        })
    })
    return;
}


// sense the limit of moderation sensitivity
function sense(inward) {

    // get all paths
    var paths = tools.pathfind(inward);
    paths.forEach(function(path) {

        // load up image
        var imagining = studio.imagine(path);
        imagining.then(function(image) {

            // punch out upper left corner
            var box = {'Left': 0.0, 'Top': 0.0, 'Width': 1.00, 'Height': 1.00};
            var punch = studio.punch(box, image);

            // get height of original image and enlarge
            var height = image.bitmap.height;
            var scaling = studio.scale(punch, height);
            scaling.then(function(scaled) {

                // get bitmap from image
                var buffing = studio.buff(scaled);
                buffing.then(function(bitmap) {

                    // send to engine for detection
                    var detecting = engine.detect('moderations', bitmap);
                    detecting.then(function(detection) {

                        // log detection
                        if (detection.ModerationLabels.length < 1) {

                            // print to screen
                            tools.debug(path);
                        }
                    })
                })
            })
        })
    return;
    })
}


// spatter function to randomly superimpose images from foregrounds onto backgrounds
function spatter(foreground, background) {

    // get all foreground paths
    var folders = tools.pathfind(foreground);
    folders = folders.filter(function(folder) {return !folder.includes('.json')});

    // get all background images
    var foregrounds = [];
    folders.forEach(function(folder) {

        // pathfind images
        var paths = tools.pathfind(folder);
        foregrounds = foregrounds.concat(paths);
    })
    // get all background paths
    var folders = tools.pathfind(background);
    folders = folders.filter(function(folder) {return !folder.includes('.json')});

    // get all background images
    var backgrounds = [];
    folders.forEach(function(folder) {

        // pathfind images
        var paths = tools.pathfind(folder);
        backgrounds = backgrounds.concat(paths);
    })
    // randomly shuffle each
    foregrounds = foregrounds.sort(function(a, b) {return Math.random() - Math.random()});
    backgrounds = backgrounds.sort(function(a, b) {return Math.random() - Math.random()});

    // zip together
    var zipper = foregrounds.map(function(path, index) {return [path, backgrounds[index]]});

    // make composite from each
    zipper.forEach(function(paths) {

        // unpack
        var path = paths[0];
        var pathii = paths[1];

        // status
        tools.debug('superimposing ' + path + ' onto ' + pathii);

        // get initial image
        var imagining = studio.imagine(path);
        imagining.then(function(image) {

            // pick a random size
            sizes = [320, 160, 80].sort(function(a, b) {return Math.random() - Math.random()});
            size = sizes[0];

            // scale the image at random
            var scaling = studio.scale(image, size);
            scaling.then(function(scaled) {

                // get background image
                var imaginingii = studio.imagine(pathii);
                imaginingii.then(function(imageii) {

                    // find a random location
                    var left = Math.round(Math.random() * (imageii.bitmap.width - scaled.bitmap.width));
                    var top = Math.round(Math.random() * (imageii.bitmap.height - scaled.bitmap.height));

                    // superimpose image and write to old file
                    imageii.composite(scaled, left, top);

                    // write over foreground file
                    var deposit = path;
                    tools.debug('saving ' + deposit + '...');
                    imageii.write(deposit);
                })
            })
        })
    })
    return;
}


// randomly blur all snippets in the incoming directory by up to a highest amount
function splatter(incoming, outgoing, high) {

    // get all files from incoming directory
    var paths = tools.pathfind(incoming);

    // scale all images
    paths.forEach(function(path) {

        // get image with Jimp
        var imagining = studio.imagine(path);

        // access image
        imagining.then(function(image) {

            // generate random blur degree
            var degree = 1 + Math.round(Math.random() * high);

            // blur image
            tools.debug('blurring ' + path + ' to degree ' + degree + '...', 1);
            var blurring = studio.blur(image, degree);
            blurring.then(function(image) {

                // generate file name
                var height = Math.round(image.bitmap.height);
                var stub = path.split('.')[0].replace(incoming, outgoing);
                var deposit = stub + '_' + height + '_' + degree + '.jpg';

                // save blurred image
                tools.debug('writing ' + deposit + '...', 1);
                image.write(deposit);
            })
        })
    })
    return;
}


// function to tag an image with its base64encoding
function tag(url, scatter=10) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // strip spaces
        url = url.replace(' ', '');

        // print
        tools.debug(url);

        var points = [[0.25, 0.25], [0.75, 0.75], [0.25, 0.75], [0.75, 0.25]];
        points = points.concat([[0.5, 0.5], [0.25, 0.5], [0.5, 0.25], [0.5, 0.75], [0.75, 0.5]]);

        // try to get image
        try {

            // get the image
            var imagining = studio.imagine(url);
            imagining.then(function(image) {

                // get image characteristics
                var width = image.bitmap.width;
                var height = image.bitmap.height;

                // measure at a random
                var pixels = [];
                points.forEach(function(point) {

                    // measure a random point
                    var horizontal = Math.floor(width * point[0]);
                    var vertical = Math.floor(height * point[1]);
                    var pixel = studio.glimpse(horizontal, vertical, image);

                    // update pixels
                    Object.assign(pixel, {'x': horizontal, 'y': vertical});
                    pixels.push(pixel);
                })
                // make identifier
                var identifier = {'width': width, 'height': height, 'pixels': pixels};

                // resolve
                resolve([url, identifier]);
            })
        }
        // other wise
        catch(err) {

            // see error
            tools.debug(err);

            // make fake pixels
            var pixels = map(function(pixel) {return {'x': 1, 'y': 1, 'r': 255, 'g': 255, 'b': 255}});

            // make identifier
            var identifier = {'width': 1, 'height': 1, 'pixels': pixels};

            // resolve
            resolve([url, identifier]);
        }
    })
    return promise;
}


// create texts data for setting texts size and confidence limit
function texturize() {

    // grab approvals and rejections data
    var approvals = tools.retrieve('evaluation/approvals.json');
    var rejections = tools.retrieve('evaluation/rejections.json');

    // get path names for trues and falses
    var paths = tools.pathfind('machines/texts/true/true');
    var pathsii = tools.pathfind('machines/texts/false/false');

    // make json file paths
    var json = 'machines/texts/true/true.json';
    var jsonii = 'machines/texts/false/false.json';

    // start data collection
    var trues = {};
    var falses = {};

    // bundle up attributes
    var bundle = [paths, json, trues];
    var bundleii = [pathsii, jsonii, falses];

    // go through each bundle
    [bundle, bundleii].forEach(function(member) {

        // unpack member
        var names = member[0];
        var deposit = member[1];
        var collection = member[2];

        // go through each name
        names.forEach(function(name) {

            // find record in approvals
            var stub = name.split('/').pop();

            // in approvals
            if (Object.keys(approvals).some(function(key) {return key.includes(stub)})) {

                // get record
                var identifier = 'evaluation/approvals/' + stub;
                var record = approvals[identifier];
            }
            // or rejections
            if (Object.keys(rejections).some(function(key) {return key.includes(stub)})) {

                // get record
                var identifier = 'evaluation/rejections/' + stub;
                var record = rejections[identifier];
            }
            // get texts from detections and redetections
            var texts = record['detections']['TextDetections'].concat(record['redetections']['TextDetections']);

            // only keep those with type line
            texts = texts.filter(function(text) {return text.Type == 'LINE'});

            // get the last one, likely to be the smallest in the set
            var text = texts.pop();

            // calculate the height and put roughly on a 100 point scale
            var verticals = text.Geometry.Polygon.map(function(vertical) {return vertical.Y});
            var height = Math.max(...verticals) - Math.min(...verticals);
            var labels = [{'Name': 'Height', 'Confidence': height * 100}];

            // calculate the width
            var horizontals = text.Geometry.Polygon.map(function(horizontal) {return horizontal.X});
            var width = Math.max(...horizontals) - Math.min(...horizontals);
            labels.push({'Name': 'Width', 'Confidence': width * 100});

            // get confidence
            var confidence = text.Confidence;
            labels.push({'Name': 'Confidence', 'Confidence': confidence});

            // calculate the detected text length
            var word = text.DetectedText;
            var length = word.length;
            labels.push({'Name': 'Length', 'Confidence': length * 5});

            // calculate the detected text complexity
            var complexity = [...new Set(word)].length / length;
            var length = word.length;
            labels.push({'Name': 'Complexity', 'Confidence': complexity * 100});

            // add entry to detections
            collection[name] = {'detections': {'Labels': labels}, 'redetections': {'Labels': []}};

        // make file
        tools.dump(collection, deposit);
        })
    })
    return;
}


// verify to what extent detections in originals have been blurred past rekognition
function verify(detections, redetections) {

    // retrieve files
    var detections = tools.retrieve(detections + '.json');
    var redetections = tools.retrieve(redetections + '.json');

    // go through detections
    Object.keys(detections).forEach(function(key) {

        // get detection records
        var detection = detections[key];
        var redetection = redetections[detection['scanned']];

        // compare texts and faces
        var modes = ['texts', 'faces'];
        modes.forEach(function(mode) {

            // get first reservoir and confidences
            var reservoir = detection[mode][engine.reservoirs.get(mode)];
            var confidences = reservoir.map(function(member) {return member.Confidence});
            var average = tools.average(confidences);

            // print status
            tools.debug('original for [' + mode + ']: ' + detection['original'], 1);
            tools.debug('average for ' + confidences.length + ' detections: ' + average, 1);

            // print detections
            reservoir.forEach(function(member) {

                // print tag
                var tag = engine.tags.get(mode)(member);
                tools.debug('[' + mode + ']: ' + tag, 1);
            })

            // get second reservoir and confidences
            var reservoir = redetection[mode][engine.reservoirs.get(mode)];
            var confidences = reservoir.map(function(member) {return member.Confidence});
            var average = tools.average(confidences);

            // print status
            tools.debug('scanned image for [' + mode + ']: ' + redetection['original'], 1);
            tools.debug('average for ' + confidences.length + ' detections: ' + average, 1);

            // print detections
            reservoir.forEach(function(member) {

                // print tag
                var tag = engine.tags.get(mode)(member);
                tools.debug('[' + mode + ']: ' + tag, 1);
            })
            // print spacer
            tools.debug(' ', 1);
        })
    })
    return;
}


// status
console.log('mechanic loaded.');
