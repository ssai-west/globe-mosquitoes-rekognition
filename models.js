// models.js to interface with support vector machine models and perform other tests

// import support modules
var knobs = require('./knobs.js');
var tools = require('./tools.js');
var studio = require('./studio.js');
var engine = require('./engine.js');

// load support vector machine models (texts and faces models developed but not in use)
var machines = {};
machines['relevance'] = tools.retrieve('relevance.json');
machines['texts'] = tools.retrieve('texts.json');
machines['faces'] = tools.retrieve('faces.json');


// generate an AWS style Labels list for the faces model
function facialize(detection, tier) {

    // begin suite of features
    var suite = [];

    // set a score to differentiate detections from redetections
    var level = 100;
    if (tier == 'redetections') {

        // but reset to 0 for a redetection
        level = 0;
    }
    // add tier attribute to suite
    suite.push({'Name': 'Tier', 'Confidence': level});

    // add detection confidence to suite (treated as a feature with its own confidence score)
    suite.push({'Name': 'Confidence', 'Confidence': detection.Confidence});

    // add width and height attributes, scaling to a 100 point scale
    var box = detection.BoundingBox;
    suite.push({'Name': 'Width', 'Confidence': box.Width * 100});
    suite.push({'Name': 'Height', 'Confidence': box.Height * 100});

    // add brightness feature
    var brightness = detection.Quality.Brightness;
    suite.push({'Name': 'Brightness', 'Confidence': brightness});

    // add sharpness feature
    var sharpness = detection.Quality.Sharpness;
    suite.push({'Name': 'Sharpness', 'Confidence': sharpness});

    // construct a training sample
    var sample = {'detections': {'Labels': suite}, 'redetections': {'Labels': []}};

    return sample;
}


// gauge image complexity by looking for color variation at several quadrants
function gauge(image, thoroughness=100, criterion=25) {

    // test each corner separately
    var boxes = studio.dice(2, 2);

    // for each quadrant box
    var complexities = [];
    boxes.forEach(function(box) {

        // punch out the snippet
        var snippet = studio.punch(box, image);

        // get snippet dimensions
        var width = snippet.bitmap.width;
        var height = snippet.bitmap.height;

        // for a number of trials, depending on the thoroughness
        pixels = [];
        for (var trial = 0; trial < thoroughness; trial++) {

            // get a random pixel location
            var horizontal = Math.round(Math.random() * width);
            var vertical = Math.round(Math.random() * height);

            // and determine the color
            var color = studio.glimpse(horizontal, vertical, snippet);
            pixels.push(color);
        }
        // go through each RGB color channel
        var deviations = 0;
        ['r', 'g', 'b'].forEach(function(channel) {

            // get the channel intensities for all tested pixels
            var intensities = pixels.map(function(pixel) {return pixel[channel]});

            // and calculate the standard deviation, adding to the total
            var deviation = tools.deviate(intensities);
            deviations += deviation;
        })
        // add to the list of scores
        complexities.push(deviations);
    })
    // if all quadrant scores are greater than the criteria, consider the image complex
    var complexity = Math.min(...complexities) > criterion;

    return complexity;
}


// predict model scores on a sample
function predict(model, sample) {

    // get support vector machine coefficients
    var machine = machines[model];

    // begin scores with intercepts
    var scores = {}
    Object.assign(scores, machine.intercepts);

    // get features and categories of the model
    var categories = Object.keys(scores);
    var features = Object.keys(machine);

    // go through each detection tier
    var tiers = ['detections', 'redetections'];
    tiers.forEach(function(tier) {

        // go through each sample feature
        sample[tier].Labels.forEach(function(feature) {

            // if feature is found in modeled features
            var name = feature.Name;
            var confidence = feature.Confidence;
            if (features.includes(name)) {

                // add the contribution to each category score
                categories.forEach(function(category) {

                    // add contribution to the score
                    var contribution = machine[name][category] * confidence / 100;
                    scores[category] += contribution;
                })
            }
        })
    })
    // sort scores, highest first
    var scores = Object.entries(scores);
    scores.sort(function(pair, pairii) {return pairii[1] - pair[1]});

    return scores;
}


// determine if there is a suspicious label in the image
function spy(report, suspicions) {

    // default presence to false
    var presence = false;

    // get reservoir of label detections and redetections
    var field = engine.reservoirs.get('labels');
    var reservoir = report.detections[field].concat(report.redetections[field]);
    reservoir.forEach(function(detection) {

        // get label name
        var name = detection.Name;

        // if it is in the list of suspicious labels
        if (suspicions.includes(name)) {

            // the presence is true
            presence = true;
        }
    })
    return presence;
}


// generate an AWS style Labels list for the texts model
function textualize(detection, tier) {

    // begin feature suite
    var suite = [];

    // set level for tier attribute at 100 for detections
    var level = 100;
    if (tier == 'redetections') {

        // or zero for redetections
        level = 0;
    }
    // add tier attribute to suite
    suite.push({'Name': 'Tier', 'Confidence': level});

    // add detection confidence to suite (which is its own Confidence)
    suite.push({'Name': 'Confidence', 'Confidence': detection.Confidence});

    // add width and height attributes, scaling to a 100 point scale
    var polygon = detection.Geometry.Polygon;
    var box = studio.bound(polygon);
    suite.push({'Name': 'Width', 'Confidence': box.Width * 100});
    suite.push({'Name': 'Height', 'Confidence': box.Height * 100});

    // add word length
    var word = detection.DetectedText;
    var length = word.length;
    suite.push({'Name': 'Length', 'Confidence': length});

    // calculate the detected text complexity as fraction of unique letters, skewed to penalize one letter texts
    var complexity = ([...new Set(word)].length - 1) / length;
    suite.push({'Name': 'Complexity', 'Confidence': complexity * 100});

    // construct support vector machine  sample
    var sample = {'detections': {'Labels': suite}, 'redetections': {'Labels': []}};

    return sample;
}



// set exports
exports = {'machines': machines, 'facialize': facialize, 'gauge': gauge, 'predict': predict};
Object.assign(exports, {'spy': spy, 'textualize': textualize});
module.exports = exports;

// status
console.log('models loaded.')