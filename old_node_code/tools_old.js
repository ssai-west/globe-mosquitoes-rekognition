// tools.js for general processing tools

// import file manipulation
var fs = require('fs-extra');

// import util for inspecting objects
var util = require('util');

// set debugger level
var debugging = 1;


// average values in a list
function average(array) {

    // default mean to zero
    var mean = 0;

    // if array is nonzero
    if (array.length > 0) {

        // sum values
        var sum = array.reduce(function(a, b) {
            return a + b;
        }, 0);

        // divide by length
        mean = sum / array.length;
    }
    return mean;
}


// base 64 encode a file
function base(path) {

    // load up bitmap
    var bitmap = fs.readFileSync(path);

    // convert to base64
    encoding = bitmap.toString('base64')

    return encoding
}


// batch files in a directory
function batch(directory, size) {

    // get all files from directory
    var names = fs.readdirSync(directory);

    // get number of batches
    var batches = [];
    var number = Math.ceil(names.length / size);
    for (i=0; i < number; i++) {

        // add batch
        batches.push(names.slice(i * size, (i + 1) * size));
    }
    return batches
}


// bitmap a file
function bite(path) {

    // load into bitmap
    var bitmap = fs.readFileSync(path);

    return bitmap
}


// process a large pool as a chain of smaller batches
function chain(operation, pool, size) {

    // abort with empty list
    if (pool.length < 1) {

        return;
    }
    // otherwise split pool
    var batch = pool.slice(0, size);
    pool = pool.slice(size);

    // collect promises from batch
    promises = [];
    batch.forEach(function(member) {

        // apply the operation and collect the promise
        var operating = operation(member);
        debug('operating on ' + member, 1);
        promises.push(operating);
    })
    // once all promises are collected
    Promise.all(promises).then(function() {

        // pass down the chain
        debug('promises fulfilled...', 1);
        chain(operation, pool, size);
    })
    return;
}


// view output on screen for debugging
function debug(data, level) {

    // check debugging level
    if (level <= debugging) {

        // log to console
        console.log(data);
    }
    return;
}


// dump a json file
function dump(data, deposit) {

    // dump into json deposit path
    fs.writeFileSync(deposit, JSON.stringify(data, '', 4));

return;
}


// pathfind function to get all paths from a directory
function pathfind(directory) {

    // get all files from directory
    var names = fs.readdirSync(directory);

    // form paths from directory name
    var paths = [];
    names.forEach(function(name) {

        // construct path and add
        var path = directory + '/' + name;
        paths.push(path);
    })
    return paths;
}


// retrieve a json file as an object
function retrieve(path) {

    // read in data
    var data = fs.readFileSync(path);
    data = JSON.parse(data);

    return data;
}


// see data from a promise
function see(promise) {

    // view promise
    promise.then(function(data) {

        // inspect object
        console.log(util.inspect(data, false, null, true));
    })
    return;
}


// set exports
exports = {'debugging': debugging, 'average': average, 'batch': batch, 'base': base, 'bite': bite, 'chain': chain};
Object.assign(exports, {'debug': debug, 'dump': dump, 'pathfind': pathfind, 'retrieve': retrieve, 'see': see});
module.exports = exports;

// status
console.log('tools loaded.');