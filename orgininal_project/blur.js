var fs = require("fs");
var allImages = [];

var currentImage = -1;

function getImages() {

    allImages = JSON.parse(fs.readFileSync('./data.json').toString());

    // console.log(allImages);

    processNextImage();

}


function processNextImage() {
    currentImage++;
    if (currentImage >= allImages.length) {
        return;
    }

    Jimp.read('./images/' + allImages[currentImage].image, function (err, image) {

        blurText(image);
        // var w = image.bitmap.width; // the width of the image
        // var h = image.bitmap.height; // the height of the image

        // console.log(allImages[currentImage].image, w, h);

        // processNextImage();


    });





}

function saveImage(image) {
    var file = './images/' + allImages[currentImage].image + '.modified.' + image.getExtension();
    image.write(file, function() {
        processNextImage();
    });
}


function blurText(image) {

    if (allImages[currentImage].text.TextDetections.length === 0) {
        processNextImage();
        return;
    } else {
        var imageChanged = false;
        for (var i = 0; i < allImages[currentImage].text.TextDetections.length; i++) {
            console.log(allImages[currentImage].text.TextDetections[i]);
            if (allImages[currentImage].text.TextDetections[i].Confidence > 65) {
                imageChanged = true;
                var newImage = image.clone();
                var w = image.bitmap.width;
                var h = image.bitmap.height;
                newImage.crop(allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Left * w, allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Top * h, allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Width * w, allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Height * h);
                newImage.blur(10);
                image.composite(newImage, allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Left * w, allImages[currentImage].text.TextDetections[i].Geometry.BoundingBox.Top * h);
            }
        }

        if (imageChanged) {
            saveImage(image);
            return;
        } else {
            processNextImage();
            return;
        }


    }

}


 

function init() {

    getImages();

    return;

    Jimp.read('./images/' + img.image, function (err, image) {
        // do stuff with the image (if no exception) 
        var w = image.bitmap.width; // the width of the image
        var h = image.bitmap.height; // the height of the image


        var td = img.text.TextDetections[0];


        // console.log(image);
        image.crop(td.Geometry.BoundingBox.Left * w, td.Geometry.BoundingBox.Top * h, td.Geometry.BoundingBox.Width * w, td.Geometry.BoundingBox.Height * h);
    
        image.blur(15);
        var file = "new_name." + image.getExtension();
        image.write(file, function() {
                Jimp.read('./images/' + img.image, function (err, image2) {

                    image2.composite(image, td.Geometry.BoundingBox.Left * w, td.Geometry.BoundingBox.Top * h);
                    var file = "new_name2." + image.getExtension();
                    image2.write(file);
                });
        });

    });


}



var img = {
        "image": "160811093313-macys-mall-store-1024x576.jpg",
        "faces": {
            "FaceDetails": [],
            "OrientationCorrection": "ROTATE_0"
        },
        "moderationLabels": {
            "ModerationLabels": []
        },
        "text": {
            "TextDetections": [
                {
                    "DetectedText": "macys",
                    "Type": "LINE",
                    "Id": 0,
                    "Confidence": 97.94759368896484,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.47231340408325195,
                            "Height": 0.24163389205932617,
                            "Left": 0.2147660106420517,
                            "Top": 0.13945583999156952
                        },
                        "Polygon": [
                            {
                                "X": 0.2147660106420517,
                                "Y": 0.13945583999156952
                            },
                            {
                                "X": 0.6870794296264648,
                                "Y": 0.132014662027359
                            },
                            {
                                "X": 0.6882839798927307,
                                "Y": 0.3736485540866852
                            },
                            {
                                "X": 0.21597053110599518,
                                "Y": 0.3810897469520569
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "ELAL'DER",
                    "Type": "LINE",
                    "Id": 1,
                    "Confidence": 65.37638854980469,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.07418881356716156,
                            "Height": 0.028860528022050858,
                            "Left": 0.16950379312038422,
                            "Top": 0.6558927893638611
                        },
                        "Polygon": [
                            {
                                "X": 0.16950379312038422,
                                "Y": 0.6558927893638611
                            },
                            {
                                "X": 0.24369260668754578,
                                "Y": 0.6569346785545349
                            },
                            {
                                "X": 0.24356436729431152,
                                "Y": 0.6857951879501343
                            },
                            {
                                "X": 0.16937555372714996,
                                "Y": 0.6847532987594604
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "eer",
                    "Type": "LINE",
                    "Id": 2,
                    "Confidence": 42.946197509765625,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.0743793249130249,
                            "Height": 0.030808692798018456,
                            "Left": 0.8591902256011963,
                            "Top": 0.8585240840911865
                        },
                        "Polygon": [
                            {
                                "X": 0.8591902256011963,
                                "Y": 0.8585240840911865
                            },
                            {
                                "X": 0.9335695505142212,
                                "Y": 0.857431173324585
                            },
                            {
                                "X": 0.9337127804756165,
                                "Y": 0.888239860534668
                            },
                            {
                                "X": 0.8593334555625916,
                                "Y": 0.8893327713012695
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "b",
                    "Type": "LINE",
                    "Id": 3,
                    "Confidence": 53.421939849853516,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.07619732618331909,
                            "Height": 0.03101479820907116,
                            "Left": 0.8598418235778809,
                            "Top": 0.8955502510070801
                        },
                        "Polygon": [
                            {
                                "X": 0.8598418235778809,
                                "Y": 0.8955502510070801
                            },
                            {
                                "X": 0.9360391497612,
                                "Y": 0.8940525650978088
                            },
                            {
                                "X": 0.9362320303916931,
                                "Y": 0.9250673651695251
                            },
                            {
                                "X": 0.860034704208374,
                                "Y": 0.9265650510787964
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "Li",
                    "Type": "LINE",
                    "Id": 4,
                    "Confidence": 54.37498092651367,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.09975361824035645,
                            "Height": 0.04615886136889458,
                            "Left": 0.2521459460258484,
                            "Top": 0.9047473669052124
                        },
                        "Polygon": [
                            {
                                "X": 0.2521459460258484,
                                "Y": 0.9047473669052124
                            },
                            {
                                "X": 0.35189956426620483,
                                "Y": 0.9019472002983093
                            },
                            {
                                "X": 0.35230952501296997,
                                "Y": 0.948106050491333
                            },
                            {
                                "X": 0.2525559067726135,
                                "Y": 0.9509062170982361
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "macys",
                    "Type": "WORD",
                    "Id": 5,
                    "ParentId": 0,
                    "Confidence": 97.94759368896484,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.4758784770965576,
                            "Height": 0.24781513214111328,
                            "Left": 0.21382437646389008,
                            "Top": 0.13294556736946106
                        },
                        "Polygon": [
                            {
                                "X": 0.2147660106420517,
                                "Y": 0.13945583999156952
                            },
                            {
                                "X": 0.6870794296264648,
                                "Y": 0.132014662027359
                            },
                            {
                                "X": 0.6882839798927307,
                                "Y": 0.3736485540866852
                            },
                            {
                                "X": 0.21597053110599518,
                                "Y": 0.3810897469520569
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "ELAL'DER",
                    "Type": "WORD",
                    "Id": 6,
                    "ParentId": 1,
                    "Confidence": 65.37638854980469,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.0734967291355133,
                            "Height": 0.030743002891540527,
                            "Left": 0.16999122500419617,
                            "Top": 0.6559894680976868
                        },
                        "Polygon": [
                            {
                                "X": 0.16950379312038422,
                                "Y": 0.6558927893638611
                            },
                            {
                                "X": 0.24369260668754578,
                                "Y": 0.6569346785545349
                            },
                            {
                                "X": 0.24356436729431152,
                                "Y": 0.6857951879501343
                            },
                            {
                                "X": 0.16937555372714996,
                                "Y": 0.6847532987594604
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "eer",
                    "Type": "WORD",
                    "Id": 7,
                    "ParentId": 2,
                    "Confidence": 42.946197509765625,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.07390153408050537,
                            "Height": 0.032045722007751465,
                            "Left": 0.8597795963287354,
                            "Top": 0.8578453660011292
                        },
                        "Polygon": [
                            {
                                "X": 0.8591902256011963,
                                "Y": 0.8585240840911865
                            },
                            {
                                "X": 0.9335695505142212,
                                "Y": 0.857431173324585
                            },
                            {
                                "X": 0.9337127804756165,
                                "Y": 0.888239860534668
                            },
                            {
                                "X": 0.8593334555625916,
                                "Y": 0.8893327713012695
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "Li",
                    "Type": "WORD",
                    "Id": 9,
                    "ParentId": 4,
                    "Confidence": 54.37498092651367,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.10143232345581055,
                            "Height": 0.05026090145111084,
                            "Left": 0.2520185708999634,
                            "Top": 0.9015442132949829
                        },
                        "Polygon": [
                            {
                                "X": 0.2521459460258484,
                                "Y": 0.9047473669052124
                            },
                            {
                                "X": 0.35189956426620483,
                                "Y": 0.9019472002983093
                            },
                            {
                                "X": 0.35230952501296997,
                                "Y": 0.948106050491333
                            },
                            {
                                "X": 0.2525559067726135,
                                "Y": 0.9509062170982361
                            }
                        ]
                    }
                },
                {
                    "DetectedText": "b",
                    "Type": "WORD",
                    "Id": 8,
                    "ParentId": 3,
                    "Confidence": 53.421939849853516,
                    "Geometry": {
                        "BoundingBox": {
                            "Width": 0.07570433616638184,
                            "Height": 0.0332036018371582,
                            "Left": 0.8606327772140503,
                            "Top": 0.8940575122833252
                        },
                        "Polygon": [
                            {
                                "X": 0.8598418235778809,
                                "Y": 0.8955502510070801
                            },
                            {
                                "X": 0.9360391497612,
                                "Y": 0.8940525650978088
                            },
                            {
                                "X": 0.9362320303916931,
                                "Y": 0.9250673651695251
                            },
                            {
                                "X": 0.860034704208374,
                                "Y": 0.9265650510787964
                            }
                        ]
                    }
                }
            ]
        },
        "labels": {
            "Labels": [
                {
                    "Name": "Star Symbol",
                    "Confidence": 96.8948974609375
                },
                {
                    "Name": "Shop",
                    "Confidence": 77.20359802246094
                },
                {
                    "Name": "Pharmacy",
                    "Confidence": 58.256561279296875
                },
                {
                    "Name": "Boutique",
                    "Confidence": 52.23045349121094
                },
                {
                    "Name": "Emblem",
                    "Confidence": 51.32486343383789
                }
            ],
            "OrientationCorrection": "ROTATE_0"
        }
    };


init();