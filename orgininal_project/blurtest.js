
// path module needed for __dirname in console
var path = require('path')
var __dirname = path.resolve()

// need the fs module to read/write files
var fs = require('fs-extra');

// need Jimp for image manipulation
var Jimp = require("jimp");

// load up AWS
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./cred.json');
var rekognition = new AWS.Rekognition();

// status
console.log('modules loaded')


// get files from directory
function getFiles(directory) {

    // synchronously get the conents of the current directory
    var fileNames = fs.readdirSync(directory);

    // loop through all the files
    var files = [];
    for (var index in fileNames) {

        // build the name of the current file
        var file = directory + '/' + fileNames[index];
        files.push(file);
    }

    return files;
}


// define blurText function
function blurText(imageName, data) {

    // log to console
    console.log('blurring: ' + imageName);

    // read in to Jimp
    Jimp.read(imageName, function(error, image) {

        // clone the image
        var clone = image.clone();
        var width = image.bitmap.width;
        var height = image.bitmap.height;
        console.log('width:', width)
        console.log('height:', height)

        // go through each text detection
        var texts = data.TextDetections
        for (var index in texts) {

            // only blur words
            if (texts[index].Type === 'WORD') {

                // view
                console.log(texts[index]);

                // punch out insert
                var insert = clone.clone();
                var insertLeft = texts[index].Geometry.BoundingBox.Left * width;
                var insertWidth = texts[index].Geometry.BoundingBox.Width * width;
                var insertTop = texts[index].Geometry.BoundingBox.Top * height;
                var insertHeight = texts[index].Geometry.BoundingBox.Height * height;
                insert.crop(insertLeft, insertTop, insertWidth, insertHeight);
                console.log('bounding box:')
                console.log('insert left:', insertLeft)
                console.log('insert width:', insertWidth)
                console.log('insert top:', insertTop)
                console.log('insert height', insertHeight)

                // blur it
                console.log('blurring...');

                // calculate degree
                var atLeast = 0
                var degree = atLeast + Math.floor(insertHeight / 4);
                console.log('blur degree: ' + degree);
                insert.blur(degree);

                // reform
                clone.composite(insert, insertLeft, insertTop);
                }
        }

        // write to directory
        var directory = __dirname;
        var shortFile = imageName.split('/');
        shortFile = shortFile.pop();
        fileName = directory + '/images_blurred/' + shortFile + '_blurred.' + clone.getExtension();
        console.log('writing: ' + fileName);
        clone.write(fileName);
    })
    return;
}


// define detectText function
function detectText(imageName) {

    // create bitmap
    var bitmap = fs.readFileSync(imageName);
    var shortFile = imageName.split('/');
    shortFile = shortFile.pop();

    // call AWS
    console.log('detectText called for: ', shortFile);
    rekognition.detectText({
        "Image": {
        "Bytes": bitmap,
        }
    }, function(error, data) {
        if (!error) {
            console.log('detected')
            blurText(imageName, data)
        }})

    return;
}


// process a batch of images
function processImages(images) {

    // process all files
    for (var index in images) {

        // blur text
        var data = detectText(images[index])
        // console.log(data)
        }

    return data;
}


// process all images
var imageDirectory = __dirname + '/images_test';
var images = getFiles(imageDirectory);
var data = processImages(images)


