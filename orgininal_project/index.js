//  .d888888  dP   dP   dP .d88888b     dP                                           888888ba                                               oo   dP   oo
// d8'    88  88   88   88 88.    "'    88                                           88    `8b                                                   88
// 88aaaaa88a 88  .8P  .8P `Y88888b.    88 88d8b.d8b. .d8888b. .d8888b. .d8888b.    a88aaaa8P' .d8888b. .d8888b. .d8888b. .d8888b. 88d888b. dP d8888P dP .d8888b. 88d888b.
// 88     88  88  d8'  d8'       `8b    88 88'`88'`88 88'  `88 88'  `88 88ooood8     88   `8b. 88ooood8 88'  `"" 88'  `88 88'  `88 88'  `88 88   88   88 88'  `88 88'  `88
// 88     88  88.d8P8.d8P  d8'   .8P    88 88  88  88 88.  .88 88.  .88 88.  ...     88     88 88.  ... 88.  ... 88.  .88 88.  .88 88    88 88   88   88 88.  .88 88    88
// 88     88  8888' Y88'    Y88888P     dP dP  dP  dP `88888P8 `8888P88 `88888P'     dP     dP `88888P' `88888P' `88888P' `8888P88 dP    dP dP   dP   dP `88888P' dP    dP
//                                                                  .88                                                        .88
//                                                              d8888P                                                     d8888P

// need the fs module to read/write files
var fs = require('fs-extra');

// need the aws sdk
var AWS = require('aws-sdk');

// load the aws credentials file
AWS.config.loadFromPath('./cred.json');

console.log('AWS loaded')


var rekognition = new AWS.Rekognition();

// define the images directory
var imageDir = __dirname + '/images_test';

// get a list of images from the images directory
var imageFiles = getFiles(imageDir);

// processed files
var processedFiles = [];


console.log('images found')


// process the first file
processNextImage();


console.log('processed next image')


// processes the first image in the array
function processNextImage() {

    console.log('processing next image...')

    // if the image files array is
    if (imageFiles.length === 0) {
        fs.writeFileSync(__dirname + '/data.json', JSON.stringify(processedFiles, '', 4));
        //console.log(JSON.stringify(processedFiles, '', 4));
        //console.log(JSON.stringify(processedFiles));
        //console.log(JSON.stringify(processedFiles, '', 5));
        console.log('ImageFiles===0')
        console.log(processedFiles.length, processedFiles);
        console.log('all files processed')
        return;
    }

    var thisImage = imageFiles[0];
    imageFiles.shift();

    var bitmap = fs.readFileSync(thisImage);

    var shortFile = thisImage.split('/');
    shortFile = shortFile.pop();

    processedFiles.push({image: shortFile});
    console.log(processedFiles.length, processedFiles);

    detectFaces(thisImage, bitmap);

}




function detectFaces(thisImage, bitmap) {
    
    console.log('detectFaces called', thisImage);
    rekognition.detectFaces({
        "Image": { 
        "Bytes": bitmap
        },
        "Attributes": ["ALL"]
    }, function(err, data) {
        if (!err) {
            // console.log(JSON.stringify(data, '', 4));
            processedFiles[processedFiles.length-1].faces = data;
            // processNextImage();
            detectModerationLabels(thisImage, bitmap);
        } else {
            console.log(err);
            // processNextImage();
            detectModerationLabels(thisImage, bitmap);
        }
    });

    return;

}

function detectModerationLabels(thisImage, bitmap) {
    
    console.log('detectModerationLabels called', thisImage);
    rekognition.detectModerationLabels({
        "Image": { 
        "Bytes": bitmap,
        }
    }, function(err, data) {
        if (!err) {
            // console.log(JSON.stringify(data, '', 4));
            processedFiles[processedFiles.length-1].moderationLabels = data;
            // processNextImage();
            detectText(thisImage, bitmap);
        } else {
            // processNextImage();
            detectText(thisImage, bitmap);
        }
    });

    return;

}



function detectText(thisImage, bitmap) {
    
    console.log('detectText called', thisImage);
    rekognition.detectText({
        "Image": { 
        "Bytes": bitmap,
        }
    }, function(err, data) {
        if (!err) {
            // console.log(JSON.stringify(data, '', 4));
            processedFiles[processedFiles.length-1].text = data;
            detectLabels(thisImage, bitmap);
            // processNextImage();

        } else {
            detectLabels(thisImage, bitmap);
            // processNextImage();

        }
    });

    return;

}



function detectLabels(thisImage, bitmap) {
    
    console.log('detectLabels called', thisImage);
    rekognition.detectLabels({
        "Image": { 
        "Bytes": bitmap,
        }
    }, function(err, data) {
        if (!err) {
            // console.log(JSON.stringify(data, '', 4));
            processedFiles[processedFiles.length-1].labels = data;
            processNextImage();

        } else {
            processNextImage();

        }
    });

    return;

}


// console.log(imageFiles);

return;



var rekognition = new AWS.Rekognition();






var bitmap = fs.readFileSync('./images/original-40.jpg');

    rekognition.detectLabels({
        "Image": { 
            "Bytes": bitmap,
        }
    }, function(err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
    });


console.log('');
console.log('');
console.log('');
console.log('');


     rekognition.detectText({
        "Image": { 
            "Bytes": bitmap,
        }
    }, function(err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log(data);
        }
    });     
     rekognition.detectFaces({
        "Image": { 
            "Bytes": bitmap,
        }
    }, function(err, data) {
        if (err) {
            console.log(err);
        } else {
            console.log(JSON.stringify(data, '', 4));
        }
    });










function getFiles(dir, files_) {
    // start a files array or use the one provided
    files_ = files_ || [];

    // synchronously get the conents of the current directory
    var files = fs.readdirSync(dir);

    // loop through all the files
    for (var i in files) {
        // build the name of the current file
        var name = dir + '/' + files[i];

        // if its a directory, then recursively call
        if (fs.statSync(name).isDirectory()) {
            // pass the current name and array so far of files
            getFiles(name, files_);
        } else {
            // it's a file not a directory, so add it to the list
            if (files[i] !== '.DS_Store') {
                files_.push(name);
            }
        }
    }
    // return the array of files
    return files_;
}

