# cores.py for a core object with basic file directory interaction

# import reload
from importlib import reload

# import system tools
import os
import sys
import json
import shutil

# import time and datetime
import time
import datetime

# import pretty print
import pprint


# class Core
class Core(list):
    """Class core to define basic file manipulatioin.

    Inherits from:
        list
    """

    def __init__(self):
        """Instantiate a Core instance.

        Arguments:
            None
        """

        # set current time
        self.now = time.time()

        return

    def __repr__(self):
        """Create string for on screen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = ' < Core instance >'

        return representation

    def _destroy(self, directory):
        """Delete all files in a directory and the directory itself.

        Arguments:
            directory: str, directory path

        Returns:
            None
        """

        # get all paths in the directory
        paths = self._see(directory)

        # prompt user and continue on blank
        prompt = input('destroy {} !?>>'.format(directory))
        if prompt in ('', ' '):

            # remove all files
            [os.remove(path) for path in paths]

            # print status
            print('{} destroyed.'.format(directory))

        return None

    def _dump(self, contents, destination):
        """Dump a dictionary into a json file.

        Arguments:
            contents: dict
            destination: str, destination file path

        Returns:
            None
        """

        # dump file
        with open(destination, 'w') as pointer:

            # dump contents
            print('dumping into {}...'.format(destination))
            json.dump(contents, pointer)

        return None

    def _group(self, members, function):
        """Group a set of members by the result of a function of the member.

        Arguments:
            members: list of dicts
            function: str

        Return dict
        """

        # get all fields
        fields = self._skim([function(member) for member in members])

        # create groups
        groups = {field: [] for field in fields}
        [groups[function(member)].append(member) for member in members]

        return groups

    def _jot(self, lines, destination):
        """Jot down the lines into a text file.

        Arguments:
            lines: list of str
            destination: str, file path

        Returns:
            None
        """

        # add final endline
        lines = [line + '\n' for line in lines]

        # save lines to file
        with open(destination, 'w') as pointer:

            # write lines
            pointer.writelines(lines)

        return None

    def _load(self, path):
        """Load a json file.

        Arguments:
            path: str, file path

        Returns:
            dict
        """

        # try to
        try:

            # open json file
            with open(path, 'r') as pointer:

                # get contents
                print('loading {}...'.format(path))
                contents = json.load(pointer)

        # unless the file does not exit
        except FileNotFoundError:

            # in which case return empty json
            print('creating {}...'.format(path))
            contents = {}

        return contents

    def _look(self, contents, level=0):
        """Look at the contents of an h5 file, to a certain level.

        Arguments:
            contents: object to look at
            level=2: the max nesting level to see
            five: the h5 file

        Returns:
            None
        """

        # unpack into a dict
        data = self._unpack(contents, level=level)

        # pretty print it
        pprint.pprint(data)

        return None

    def _make(self, folder):
        """Make a folder in the directory if not yet made.

        Arguments:
            folder: str, directory path

        Returns:
            None
        """

        # try to
        try:

            # create the directory
            os.mkdir(folder)

        # unless directory already exists, or none was given
        except (FileExistsError, FileNotFoundError, PermissionError):

            # in which case, nevermind
            pass

        return None

    def _see(self, directory):
        """See all the paths in a directory.

        Arguments:
            directory: str, directory path

        Returns:
            list of str, the file paths.
        """

        # make paths
        paths = ['{}/{}'.format(directory, path) for path in os.listdir(directory)]

        return paths

    def _skim(self, members):
        """Skim off the unique members from a list.

        Arguments:
            members: list

        Returns:
            list
        """

        # trim duplicates and sort
        members = list(set(members))
        members.sort()

        return members

    def _stamp(self, message, initial=False):
        """Start timing a block of code, and print results with a message.

        Arguments:
            message: str
            initial: boolean, initial message of block?

        Returns:
            None
        """

        # get final time
        final = time.time()

        # calculate duration and reset time
        duration = round(final - self.now, 5)
        self.now = final

        # if iniital message
        if initial:

            # add newline
            message = '\n' + message

        # if not an initial message
        if not initial:

            # print duration
            print('took {} seconds.'.format(duration))

        # begin new block
        print(message)

        return None

    def _tabulate(self, rows, destination):
        """Create a csv file from a list of records.

        Arguments:
            rows: list of list of strings
            destination: str, file path

        Returns:
            None
        """

        # write rows
        with open(destination, 'w') as pointer:

            # write csv
            csv.writer(pointer).writerows(rows)

        return None

    def _transcribe(self, path):
        """Transcribe the text file at the path.

        Arguments:
            path: str, file path

        Returns:
            list of str
        """

        # read in file pointer
        with open(path, 'r') as pointer:

            # read in text and eliminate endlines
            lines = pointer.readlines()
            lines = [line.replace('\n', '') for line in lines]

        return lines

    def _unpack(self, contents, level=100, nest=0):
        """Unpack an h5 file into a dict.

        Arguments:
            contents: h5 file or dict
            level=100: int, highest nesting level kept
            nest=0: current nesting level

        Returns:
            dict
        """

        # check for nesting level
        if nest > level:

            # data is truncated to an ellipsis
            data = '...'

        # otherwise
        else:

            # try
            try:

                # to go through keys
                data = {}
                for field, info in contents.items():

                    # add key to data
                    data[field] = self._unpack(info, level, nest + 1)

            # unless there are no keys
            except AttributeError:

                # just return the contents
                data = contents

        return data