# machines.py for support vector machines

# import reload
from importlib import reload

# import csv, os, json
import os
import csv
import json

# import pretty print
from pprint import pprint

# import pil
from PIL import Image

# import request
from urllib.request import urlopen

# import random
from random import random

# import datetime
from datetime import datetime

# import numpy
import numpy

# import math
from math import log, sqrt, log10

# import random forest
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.svm import SVC, LinearSVC

# import pyplot
from matplotlib import pyplot


# define machine class
class Machine(object):
    """Machine class for support vector machines.

    Inherits from:
        object
    """

    def __init__(self, directory, reverse=False, weights=[1.0, 1.0], regularization=1.0):
        """Initialize a Machine instance with a directory name.

        Arguments:
            directory: str,
            reverse=False: boolean, reverse order of groups?
            weights=[1.0, 1.0]: list of floats, weights per group

        Returns:
            None
        """

        # set directory
        self.directory = directory

        # get current date
        self.now = datetime.now()

        # set up hierarchy
        self.reverse = reverse
        self.groups = []
        self.categories = []
        self.hierarchy = {}
        self.registry = {}

        # store data
        self.data = {}

        # list of labels
        self.labels = []
        self.mirror = {}

        # set up training
        self.regularization = regularization
        self.fraction = 0.2
        self.training = []
        self.holdouts = []

        # define weights
        self.weights = weights

        # make models
        self.models = {'linear': None, 'ovr': None, 'ovo': None}

        # model coefficients
        self.coefficients = {'linear': {}, 'ovr': {}, 'ovo': {}}

        # pca compression for graphing
        self.pca = None

        # graphing colors
        self.good = ['blue', 'green', 'cyan', 'olive', 'lightgreen', 'lightblue', 'lime', 'aqua']
        self.bad = ['magenta', 'red', 'orange', 'orchid', 'crimson', 'fuchsia', 'coral', 'hotpink', 'firebrick']
        self.colors = {}

        # multiple run stats
        self.results = {}
        self.mistakes = []

        # optimizer summaries
        self.summaries = []

        # validation report
        self.validation = []

        return

    def analyze(self):
        """Analyze the dataset.

        Arguments:
            None

        Returns:
            None
        """

        # digest the data
        self.digest()

        # train the model
        self.divide()
        self.train()

        # test the model
        self.test()
        self.validate()

        # compress for plots
        self.compress()

        return None

    def ask(self, label):
        """Ask about the distribution of a label amongst categories.

        Arguments:
            label: str

        Returns:
            None
        """

        # get sample from each category
        counts = []
        for category in self.categories:

            # get all samples
            samples = [sample for sample in self.training + self.holdouts if sample['category'] == category]
            samples = [sample for sample in samples if any([label == member['Name'] for member in sample['labels']])]

            # add to counts
            counts.append((category, len(samples)))

        # sort counts
        counts.sort(key=lambda pair: pair[1], reverse=True)

        # print
        print('\ndistribution of: {}'.format(label))
        [print('   {}: {}'.format(category, count)) for category, count in counts]
        print(' ')

        return None

    def chew(self, number=50):
        """Chew through a number of training rounds to get average statistics.

        Arguments:
            number: int, number of rounds

        Returns:
            dict
        """

        # begin report
        report = {'linear': {'training': {}, 'holdouts': {}}}
        report.update({'ovo': {'training': {}, 'holdouts': {}}})
        report.update({'ovr': {'training': {}, 'holdouts': {}}})

        # run training and collect mistakes
        results = []
        mistakes = []
        for trial in range(number):

            # status
            print('{} of {}'.format(trial, number))

            # get results for the round
            result = {'linear': {}, 'ovo': {}, 'ovr': {}}

            # split data and train model
            self.divide()
            self.train()
            self.test()

            # set up batches
            batches = {'training': self.training, 'holdouts': self.holdouts}

            # process confusion matrices
            for model in ('linear', 'ovo', 'ovr'):

                # for holdouts and training
                names = ['training', 'holdouts']
                for name in names:

                    # add to mistakes
                    mismatches = [sample for sample in batches[name] if sample['group'] != sample['superprediction_' + model]]
                    mistakes += mismatches

                    # prepare result
                    result[model][name] = {}

                    # get confusion matrix
                    truths = [sample['group'] for sample in batches[name]]
                    predictions = [sample['superprediction_' + model] for sample in batches[name]]
                    features = self.groups
                    confusion = self.confuse(truths, predictions, features)

                    # extract counts and percents for first line
                    counts = [int(digit) for digit in confusion[1].split('|')[1].split()]
                    percent = int(confusion[1].split('(')[1].split()[0])

                    # add to result
                    result[model][name].update({'true_positives': counts[0], 'false_negatives': counts[1], 'positives': percent})

                    # extract counts and percents for second line
                    counts = [int(digit) for digit in confusion[2].split('|')[1].split()]
                    percent = int(confusion[2].split('(')[1].split(' ')[0])

                    # add to result
                    result[model][name].update({'false_positives': counts[0], 'true_negatives': counts[1], 'negatives': percent})

            # add to results collections
            results.append(result)

        # sum up all results
        fields = ('false_positives', 'true_positives', 'false_negatives', 'true_negatives')
        associations = {'false_negatives': 0, 'false_positives': 1, 'true_positives': 0, 'true_negatives': 1}
        for model in ('linear', 'ovo', 'ovr'):

            # for each batch
            batches = {'training': self.training, 'holdouts': self.holdouts}
            for batch in ('training', 'holdouts'):

                # go through each field
                for field in fields:

                    # calculate relevant total
                    total = len([sample for sample in batches[batch] if sample['group'] == self.groups[associations[field]]])

                    # sum up values
                    scores = [result[model][batch][field] for result in results]
                    average = round(numpy.average(scores), 2)
                    deviation = round(numpy.std(scores), 2)
                    percent = round(100 * average / total, 2)

                    # add to report
                    report[model][batch][field] = [average, deviation, percent]

        # save results
        self.results = report

        # only keep mistakes from ovo model
        mistakes = [mistake for mistake in mistakes if mistake['group'] != mistake['superprediction_ovo']]

        # convert mistakes to strings and remove duplicates
        mistakes = {mistake['name']: mistake for mistake in mistakes}
        mistakes = [value for value in mistakes.values()]
        mistakes.sort(key=lambda record: self.groups.index(record['group']), reverse=True)

        # set attributes
        self.mistakes = mistakes

        return None

    def chop(self, start, finish, number):
        """Chop a range of numbers into a number of sections.

        Arguments:
            start: float
            finish: float
            number: int

        Returns:
            list of floats
        """

        # get segment length
        segment = (finish - start) / number

        # get points
        points = [start + index * segment for index in range(number + 1)]

        return points

    def compress(self):
        """Perform PCA compression.

        Arguments:
            None

        Returns:
            None
        """

        # get matrix from samples
        matrix = [self.vectorize(sample) for sample in self.training + self.holdouts]

        # perform pca and set attribute
        pca = PCA(n_components=2).fit(matrix)
        self.pca = pca

        # get new training vectors
        transform = pca.transform(matrix)
        for sample, vector in zip(self.training + self.holdouts, transform):

            # add vector
            sample['pca'] = [float(entry) for entry in vector]

        return None

    def confuse(self, truths, predictions, features):
        """Calculate the confusion matrices for both models.

        Arguments:
            truths: targets from samples
            predictions: predictions from model
            features: list of features

        Returns:
            list of str
        """

        # determine length of name and number of digits
        length = max([len(feature) for feature in features])
        digits = int(log10(len(truths))) + 1

        # zip together truths and predictions
        zipper = [(truth, prediction) for truth, prediction in zip(truths, predictions)]

        # make header row
        header = ' ' * length + '   '
        for feature in features:

            # add to header
            header += (' ' * (digits - 1)) + feature[0] + ' '

        # begin confusion matrix with header
        confusion = [header]

        # make feature counts
        for feature in features:

            # begin row with padded feature name
            row = ((' ' * length) + feature)[-length:] + ': |'

            # go through features
            for featureii in features:

                # count number of pairs
                members = [pair for pair in zipper if pair[0] == feature and pair[1] == featureii]

                # pad count and add
                count = ((' ' * digits) + str(len(members)))[-digits:]
                row += count + ' '

            # calculate percent correct
            segment = [pair for pair in zipper if pair[0] == feature]
            hits = [pair for pair in segment if pair[1] == feature]
            percent = round(100 * len(hits) / len(segment))

            # add to row
            row += '| ({} %)'.format(percent)

            # add to confusion
            confusion.append(row)

        # add spacer
        confusion.append('')

        return confusion

    def contribute(self, sample, model='ovo'):
        """Calculate the contribution of each label to each score.

        Arguments:
            sample: dict
            model: str, the model name

        Returns:
            dict
        """

        # get the coefficients
        coefficients = self.coefficients[model]
        intercepts = self.coefficients[model]['intercepts']
        categories = [self.registry[category] + '/' + category for category in self.categories]

        # get contributions to each category from each label
        contributions = {category: {} for category in categories}
        for category in categories:

            # go through labels
            for label in sample['labels']:

                # unpack
                name = label['Name']
                confidence = label['Confidence']

                # check in key
                if name in coefficients.keys():

                    # calculate contribution
                    contribution = coefficients[name][category] * confidence / 100
                    contributions[category][name] = contribution

            # get total score
            contributions[category]['score'] = intercepts[category] + sum(contributions[category].values())

        return contributions

    def decide(self, model='ovo'):
        """Get decision boundary lines for the model

        Arguments:
            model: svc instance

        Returns:
            list of dicts
        """

        # functionalize to make function from coefficients
        functionalize = lambda a, b, c, z: lambda x: (z - (c + a * x)) / b

        # get coefficients and intercepts
        coefficients = self.pca.transform(self.models[model].coef_)
        intercepts = self.models[model].intercept_

        # one per class if linear
        if model == 'linear':

            # make color pairs with black as second color
            colors = [(self.colors[category], 'black') for category in self.models[model].classes_]

        # otherwise pairs
        else:

            # make color pairs
            colors = []
            for category in self.models[model].classes_:

                # make category pairs
                index = self.models[model].classes_.tolist().index(category)
                for categoryii in self.models[model].classes_[index + 1:]:

                    # make pair
                    colors.append((self.colors[category], self.colors[categoryii]))

        # make decisions
        decisions = []
        for pair, coefficient, intercept in zip(colors, coefficients, intercepts):

            # get formula for y in terms of x
            y = functionalize(coefficient[0], coefficient[1], intercept, 0)

            # get margins
            upper = functionalize(coefficient[0], coefficient[1], intercept, 1)
            lower = functionalize(coefficient[0], coefficient[1], intercept, -1)

            # make decision boundary arnd margins
            decision = {'y': y, 'upper': upper, 'lower': lower, 'colors': pair}
            decisions.append(decision)

        return decisions

    def diagnose(self, model='ovo', mistakes=None, number=5):
        """Diagnose the linear model as to misclassifications it made.

        Arguments:
            model: str, name of model
            mistakes: list of dicts
            number=3: int, number of contributions

        Returns:
            None
        """

        # collect mistakes
        if not mistakes:

            # go through holdouts then samples
            mistakes = []
            batches = [self.holdouts, self.training]
            designations = ['holdouts', 'training']
            for designation, batch in zip(designations, batches):

                # get members with misclassifications
                errors = [sample for sample in batch if sample['category'] != sample['prediction_' + model]]

                # sort by second group first
                errors.sort(key=lambda sample: self.groups.index(sample['group']), reverse=True)

                # get sample with incorrect groups predictions and correct group predictions
                incorrect = [sample for sample in errors if sample['group'] != sample['superprediction_' + model]]
                correct = [sample for sample in errors if sample['group'] == sample['superprediction_' + model]]

                # add to mistakes
                mistakes += incorrect + correct

        # go through errors
        for index, error in enumerate(mistakes):

            # print heading
            formats = [index, error['category'], error['group']]
            formats += [error['prediction_' + model], error['superprediction_' + model]]
            print('\nerror #{}: {} ({}) -!-> {} ({})\n'.format(*formats))

            # get contributions
            contributions = self.contribute(error, model=model)

            # get class scores
            scores = [(category, round(contributions[category]['score'], 2)) for category in contributions.keys()]
            scores.sort(key=lambda score: score[1], reverse=True)
            [print(score) for score in scores]

            # get top factors against correct answer
            identifier = error['group'] + '/' + error['category']
            print('\ntop factors against {}:'.format(identifier))
            factors = [(item[0], round(item[1], 2)) for item in contributions[identifier].items()]
            factors = [item for item in factors if item[0] != 'score']
            factors.sort(key=lambda pair: pair[1])
            [print('     {}: {}'.format(*factor)) for factor in factors[:number]]

            # get top factors against correct answer
            identifier = error['superprediction_' + model] + '/' + error['prediction_' + model]
            print('\ntop factors for {}:'.format(identifier))
            factors = [(item[0], round(item[1], 2)) for item in contributions[identifier].items()]
            factors = [item for item in factors if item[0] != 'score']
            factors.sort(key=lambda pair: pair[1], reverse=True)
            [print('     {}: {}'.format(*factor)) for factor in factors[:number]]

            # go through menu options
            signal = self.offer(error)

            # break loop if stop signaled
            if signal:

                # break
                break

        return None

    def digest(self):
        """Digest the dataset.

        Arguments:
            None

        Return:
            None
        """

        # define the categories and gather data
        hierarchy = {}
        data = {}
        registry = {}
        colors = {}

        # link to color palettes
        palettes = [self.good, self.bad]

        # get all groups in the directory
        groups = [group for group in os.listdir(self.directory) if '.' not in group]

        # reverse groups?
        groups.sort()
        if self.reverse:

            # reverse groups
            groups.reverse()

        # create hierarchy
        for group, palette in zip(groups, palettes):

            # add to hierarchy
            hierarchy[group] = []

            # get categories
            categories = [category for category in os.listdir(self.directory + '/' + group) if '.' not in category]
            categories = [category for category in categories if '_' not in category]
            categories.sort()

            # gather labels and define colors
            for category, color in zip(categories, palette):

                # add to colors
                colors[category] = color

                # add to hierarchy
                hierarchy[group].append(category)
                registry[category] = group

                # get data file
                path = self.directory + '/' + group + '/' + category + '.json'
                with open(path, 'r') as pointer:

                    # get detections and update data
                    detections = json.load(pointer)
                    for name, info in detections.items():

                        # update data
                        datum = {name: info['detections']['Labels'] + info['redetections']['Labels']}
                        data.update(datum)

        # get all labels
        labels = []
        for info in data.values():

            # add all labels
            [labels.append(entry['Name']) for entry in info]

        # remove duplicates and sort
        labels = list(set(labels))
        labels.sort()

        # create mirror of labels
        mirror = {label: index for index, label in enumerate(labels)}

        # get categories
        categories = [category for group in groups for category in hierarchy[group]]

        # set attributes
        self.hierarchy = hierarchy
        self.registry = registry
        self.groups = groups
        self.categories = categories
        self.data = data
        self.labels = labels
        self.mirror = mirror
        self.colors = colors

        return None

    def divide(self):
        """Divide the data into holding and training sets.

        Arguments:
            None

        Returns:
            None
        """

        # create datasets
        holdouts = []
        training = []

        # go through each category
        for category in self.categories:

            # get all data for category
            members = [item for item in self.data.items() if category in item[0]]

            # create samples
            samples = []
            for name, info in members:

                # make sample
                color = self.colors[category]
                group = self.registry[category]
                sample = {'name': name, 'category': category, 'group': group, 'labels': info, 'color': color}
                samples.append(sample)

            # split into holdouts and training
            samples.sort(key=lambda sample: random())
            hold = int(len(samples) * self.fraction)
            holdouts += samples[:hold]
            training += samples[hold:]

        # set attributes
        self.training = training
        self.holdouts = holdouts

        return None

    def draw(self, *arrows, model='ovo', density=200, name=None, boundaries=False, annotations=False, manual=True):
        """Draw a plot of the model.

        Arguments:
            *arrows: unpack tuple of strings, labels to show arrows
            model=None: svc model object
            density=200: background densitiy
            name=None: str, title for graph
            boundaries: boolean, draw class boundaries?
            annotations: boolean, annotate sample numbers?
            manual: boolean, use manual predictions?

        Returns:
            None
        """

        # clear pyplot
        pyplot.cla()

        # determine horizontal ticks in pca space
        horizontals = [sample['pca'][0] for sample in self.training + self.holdouts]
        frame = (max(horizontals) - min(horizontals)) * 0.05
        horizontals = self.chop(min(horizontals) - frame, max(horizontals) + frame, density)

        # dtermine vertical ticks in pca space
        verticals = [sample['pca'][1] for sample in self.training + self.holdouts]
        frame = (max(verticals) - min(verticals)) * 0.5
        verticals = self.chop(min(verticals) - frame, max(verticals) + frame, density)

        # plot background
        background = [[horizontal, vertical] for horizontal in horizontals for vertical in verticals]
        inverse = self.pca.inverse_transform(background)

        # get predictions
        predictions = self.models[model].predict(inverse)
        if manual:

            # keep
            keepers = predictions

            # get prediction manually
            predictions = self.predict(inverse, model)

            # compare predictions
            zipper = [triplet for triplet in zip(keepers, predictions, inverse)]
            triplets = [triplet for triplet in zipper if triplet[0] != triplet[1]]
            for triplet in triplets:

                print(triplet)

        # plot predictions
        for point, prediction in zip(background, predictions):

            # plot
            color = self.colors[prediction]
            pyplot.plot(point[0], point[1], color=color, marker=',')

        # get decision boundaries
        if boundaries:

            # get boundaries and plot
            decisions = self.decide(model)
            for decision in decisions:

                # plot boundary
                xs = horizontals
                ys = [decision['y'](x) for x in xs]
                pyplot.plot(xs, ys, color=decision['colors'][0], linestyle='solid')
                pyplot.plot(xs, ys, color=decision['colors'][1], linestyle='dashed')

                # plot upper margin
                upper = [decision['upper'](x) for x in xs]
                pyplot.plot(xs, upper, color=decision['colors'][0], linestyle='dotted')

                # plot upper margin
                lower = [decision['lower'](x) for x in xs]
                pyplot.plot(xs, lower, color=decision['colors'][1], linestyle='dotted')

        # plot training samples
        legends = []
        for index, sample in enumerate(self.training):

            # get legend
            legend = None
            if sample['category'] not in legends:

                # make legend and add
                legend = sample['category']
                legends.append(legend)

            # plot and annotate
            pyplot.plot(sample['pca'][0], sample['pca'][1], color=sample['color'], marker='o', label=legend)

            # annotate
            if annotations:

                # annotate
                pyplot.annotate(str(index), sample['pca'])

        # plot holdout samples
        for index, sample in enumerate(self.holdouts):

            # plot and annotate
            pyplot.plot(sample['pca'][0], sample['pca'][1], color=sample['color'], marker='^')

            # annotate
            if annotations:

                # annotate
                pyplot.annotate(str(index), sample['pca'])

        # plot arrows
        for arrow in arrows:

            # make arrow
            index = self.mirror[arrow]
            dx = self.pca.components_[0][index]
            dy = self.pca.components_[1][index]

            # get scaling factor
            factors = []
            factors.append(horizontals[-1] / (2 * dx))
            factors.append(horizontals[0] / (2 * dx))
            factors.append(verticals[-1] / (2 * dy))
            factors.append(verticals[0] / (2 * dy))

            # sort by smallest positive factor
            factors = [factor for factor in factors if factor > 0]
            factors.sort()

            # apply factor
            dx = dx * factors[0]
            dy = dy * factors[0]

            # plot arrow
            pyplot.arrow(0, 0, dx, dy, head_width=0.08)

            # annotate
            pyplot.annotate(arrow, (dx * 1.5, dy * 1.5))

        # bound graph
        pyplot.xlim(min(horizontals), max(horizontals))
        pyplot.ylim(min(verticals), max(verticals))

        # get suffix
        suffix = 'linear'
        if 'decision_function_shape' in dir(self.models[model]):

            # add to title
            suffix = self.models[model].decision_function_shape

        # get explained variances by fraction
        variances = self.pca.explained_variance_ratio_
        variances = [int(float(100 * variance)) for variance in variances]

        # begin title with explained variances of first two dimensions
        title = name or 'SVC_' + suffix
        explanation = ' {}% x {}% '.format(variances[0], variances[1])

        # add title to plot
        pyplot.title(title + explanation)

        # add x axis label
        zipper = [(float(point), label) for point, label in zip(self.pca.components_[0], self.labels)]
        zipper.sort(key=lambda pair: abs(pair[0]), reverse=True)
        axis = '{}: {}, {}: {}'.format(zipper[0][1], round(zipper[0][0], 2), zipper[1][1], round(zipper[1][0], 2))
        pyplot.xlabel(axis)

        # add y axis label
        zipper = [(float(point), label) for point, label in zip(self.pca.components_[1], self.labels)]
        zipper.sort(key=lambda pair: abs(pair[0]), reverse=True)
        axis = '{}: {}, {}: {}'.format(zipper[0][1], round(zipper[0][0], 2), zipper[1][1], round(zipper[1][0], 2))
        pyplot.ylabel(axis)

        # attempt legend
        pyplot.legend(loc='upper left')

        # show plot
        pyplot.savefig(self.directory + '/' + title + '.png')
        pyplot.close()
        pyplot.cla()

        return None

    def kill(self, sample, directory='troubleshooting/kills'):
        """Kill a sample from the image directory and json file, moving to kill folder.

        Argument:
            sample: dict
            directory: directory to dump the kill into

        Returns:
            None
        """

        # get the file path and name
        path = sample['name']
        name = path.split('/')[-1]

        # make deposit pathway
        deposit = directory + '/' + name

        # move file
        os.rename(path, deposit)

        # open json file
        jason = path.replace('/' + name, '.json')
        with open(jason, 'r') as pointer:

            # get data
            data = json.load(pointer)

        # open kills file
        jasonii = directory + '.json'
        with open(jasonii, 'r') as pointer:

            # get data
            kills = json.load(pointer)

        # add record to kills
        record = data[path]
        kills[path] = record

        # delete from data
        del(data[path])

        # resave data file
        with open(jason, 'w') as pointer:

            # write
            json.dump(data, pointer)

        # resave kills
        with open(jasonii, 'w') as pointer:

            # write
            json.dump(kills, pointer)

        return None

    def look(self, label, category):
        """Look for pictures in a category with a particular label.

        Arguments:
            label: str

        Returns:
            None
        """

        # get all samples
        samples = [sample for sample in self.holdouts + self.training if sample['category'] == category]
        samples = [sample for sample in samples if any([label == member['Name'] for member in sample['labels']])]

        # go through each name
        print('pictures in {} with {}:'.format(category, label))
        for sample in samples:

            # offer menu options
            signal = self.offer(sample)

            # break if stop signaled
            if signal:

                # break out of loop
                break

        return None

    def offer(self, sample):
        """Offer up menu options about a sample.

        Arguments:
            sample: dict

        Returns:
            boolean, ready to stop?
        """

        # set default signal
        signal = False

        # print name
        name = sample['name']
        print(name)

        # get image and write to diagnosis
        image = Image.open(name)
        image.save(self.directory + '/diagnosis.jpg')

        # induce pause
        pause = input('next?')

        # check for ask
        if 'ask' in pause:

            # ask about label
            label = pause.replace('ask ', '')
            self.ask(label)

            # secondary pause
            pause = input('next?')

        # check for ask
        if 'view' in pause:

            # view it
            self.view(sample)

            # secondary pause
            pause = input('next?')

        # kill the entry?
        if 'kill' in pause:

            # kill it
            self.kill(sample)

        # exit
        if pause in ('X', 'XX', 'XXX', 'xxx', 'exit', 'bye', 'done'):

            # stop loop
            signal = True

        return signal

    def optimize(self, weightings=None, regularizations=None, positives=True):
        """Optimize scores across various weighting and regularizations.

        Arguments:
            weightings: list of lists of floats, weighting schemes
            regularizations: list of float, regularization parameters
            positives=True: boolean, sort by false postives?

        Returns:
            None
        """

        # retain orginials
        originals = [self.weights, self.regularization]

        # set default weightings
        if not weightings:

            # set default
            weightings = [[0.2, 1.0], [0.4, 1.0], [0.6, 1.0], [0.8, 1.0], [1.0, 1.0]]
            weightings += [[1.0, 0.8], [1.0, 0.6], [1.0, 0.4], [1.0, 0.2]]

        # set default regularizations
        if not regularizations:

            # set default
            regularizations = [0.2, 0.4, 0.6, 0.8, 1.0]

        # set default criteria
        if positives:

            # set default
            criteria = [('holdouts', 'false_positives'), ('holdouts', 'false_negatives')]
            criteria += [('training', 'false_positives'), ('training', 'false_negatives')]

        # set reverse criteria
        if not positives:

            # set default
            criteria = [('holdouts', 'false_negatives'), ('holdouts', 'false_positives')]
            criteria += [('training', 'false_negatives'), ('training', 'false_positives')]

        # go through each combination
        reports = []
        combinations = []
        for weights in weightings:

            # regularizations
            for regularization in regularizations:

                # set parameters
                self.weights = weights
                self.regularization = regularization

                # get report on several iterations
                self.chew()
                reports.append(self.results)

                # append combinations
                combination = [weights, regularization]
                combinations.append(combination)

        # reset originals
        self.weights = originals[0]
        self.regularization = originals[1]

        # get summaries
        summaries = []
        for report, combination in zip(reports, combinations):

            # go through each model
            [summaries.append((report[model], combination, model)) for model in ('linear', 'ovo', 'ovr')]

        # sort by criteria in reverse order
        for criterion in criteria[::-1]:

            # sort summaries
            summaries.sort(key=lambda summary: summary[0][criterion[0]][criterion[1]], reverse=True)

        # print results, best on bottom
        print('\noptimization:')
        for summary in summaries:

            # create line
            formats = [summary[1][0], summary[1][1], summary[2]]
            formats += [summary[0]['holdouts']['false_positives'], summary[0]['holdouts']['false_negatives']]
            formats += [summary[0]['training']['false_positives'], summary[0]['training']['false_negatives']]
            line = 'weights: {}, regularization: {}, model: {}: fp, h: {}, fn, h: {}, fp, t: {}, fn, t: {}'.format(*formats)

            # print line
            print(line)

        # set summaries
        self.summaries = summaries

        return None

    def predict(self, matrix, model='linear', show=False):
        """Predict the category of a vector using a particular model.

        Arguments:
            matrix: list of lists of floats
            model: str, the model
            show=False: boolean, show scores?

        Returns:
            str, the prediction
        """

        # get all categories
        categories = [self.registry[category] + '/' + category for category in self.categories]

        # go through each vector
        results = []
        for vector in matrix:

            # get all labels
            zipper = [(label, quantity) for label, quantity in zip(self.labels, vector)]
            zipper = [entry for entry in zipper if abs(entry[1]) > 0.0]

            # predict for all categories
            scores = []
            for category in categories:

                # get score
                score = self.coefficients[model]['intercepts'][category]
                score += sum([self.coefficients[model][label][category] * quantity for label, quantity in zipper])

                # for now change score to negative
                scores.append((category, score))

            # get category and add to results
            scores.sort(key=lambda entry: entry[1], reverse=True)
            if show:

                # show scores if desired
                print(scores)

            # pick top score for result
            category = scores[0][0].split('/')[-1]
            results.append(category)

        return results

    def report(self, number=5):
        """Report on the strongest labels for each category.

        Arguments:
            number=5: int, number of top coefficients

        Returns:
            None
        """

        # get coefficients
        coefficients = self.models['linear'].coef_

        # go through each category
        for category, vector in zip(self.models['linear'].classes_, coefficients):

            # zip together with labels
            scores = {label: coefficient for label, coefficient in zip(self.labels, vector)}

            # print
            print('\n{}'.format(category))

            # get top picks
            print('\ntop factors for: ')

            # find all training samples with this category
            contributions = {label: 0.0 for label in self.labels}
            samples = [sample for sample in self.training if sample['category'] == category]
            for sample in samples:

                # go through each label
                for label in sample['labels']:

                    # add weighted contribution
                    name = label['Name']
                    confidence = label['Confidence']
                    contributions[name] += confidence * scores[name] / (100 * len(samples))

            # sort by biggest
            contributions = [item for item in contributions.items()]
            contributions.sort(key=lambda pair: pair[1], reverse=True)
            for entry in contributions[:number]:

                # print factor
                print(entry)

            # get bottom picks
            print('\ntop factors against: ')

            # find all training samples with this category
            contributions = {label: 0.0 for label in self.labels}
            samples = [sample for sample in self.training if sample['category'] != category]
            for sample in samples:

                # go through each label
                for label in sample['labels']:

                    # add weighted contribution
                    name = label['Name']
                    confidence = label['Confidence']
                    contributions[name] += confidence * scores[name] / (100 * len(samples))

            # sort by biggest
            contributions = [item for item in contributions.items()]
            contributions.sort(key=lambda pair: pair[1], reverse=False)
            for entry in contributions[:number]:

                # print factor
                print(entry)

        return None

    def send(self, model='linear'):
        """Send the model coefficients to a file.

        Arguments:
            model=None

        Returns:
            None
        """

        # dump json file
        path = self.directory.replace('machines/', '') + '.json'
        print('saving ' + path + '...')
        with open(path, 'w') as pointer:

            # dump jason
            json.dump(self.coefficients[model], pointer)

        # make summary
        summary = []
        summary.append('Support Vector Machine model for {}'.format(self.directory))
        summary.append('date: {}'.format(self.now))
        summary.append('categories: {}'.format(self.hierarchy))
        summary.append('weights: {}'.format(self.weights))
        summary.append('regularization: {}'.format(self.regularization))
        summary.append('model: {}'.format(model))
        summary.append(' ')

        # get lines ready
        lines = [line + '\n' for line in summary + self.validation]

        # save validation
        with open(self.directory + '/validation.txt', 'w') as pointer:

            # write pointer
            pointer.writelines(lines)

        return None

    def tabulate(self, model='ovo'):
        """Tabulate the model coefficients.

        Arguments:
            model=None: the particular model

        Return:
            None
        """

        # defautl coefficients
        coefficients = {}

        # make class identifiers
        identifiers = [self.registry[category] + '/' + category for category in self.models[model].classes_]

        # for linear
        if model == 'linear':

            # begin coefficients record with intercepts
            coefficients = {}
            intercepts = {category: intercept for category, intercept in zip(identifiers, self.models[model].intercept_)}
            coefficients['intercepts'] = intercepts

            # make label entries
            for index, label in enumerate(self.labels):

                # add coefficients entry
                entry = {category: slopes[index] for category, slopes in zip(identifiers, self.models[model].coef_)}
                coefficients[label] = entry

            # if binary classifier, use negative of decision boundary for second class
            if len(self.models[model].classes_) == 2:

                # add intercept term
                second = self.models[model].classes_[1]
                identifier = self.registry[second] + '/' + second
                coefficients['intercepts'][identifier] = -self.models[model].intercept_[0]

                # make label entries
                for index, label in enumerate(self.labels):

                    # add term
                    coefficients[label][identifier] = -self.models[model].coef_[0][index]

        # for ovo or ovr (differs from scikit ovo to ovr decision function by summing values instead of counting votes)
        if model in ('ovo', 'ovr'):

            # get category pairs
            pairs = []
            for index, category in enumerate(identifiers):

                # go through categories again
                for categoryii in identifiers[index + 1:]:

                    # add to pairs
                    pairs.append((category, categoryii))

            # begin coefficients record with intercepts
            coefficients = {label: {category: 0.0 for category in identifiers} for label in ['intercepts'] + self.labels}
            for pair, vector, intercept in zip(pairs, self.models[model].coef_, self.models[model].intercept_):

                # get length
                length = len(self.models[model].classes_) - 1

                # add intercept to pairs
                coefficients['intercepts'][pair[0]] += intercept / length
                coefficients['intercepts'][pair[1]] += -intercept / length

                # go through each label in the vectors
                for label, quantity in zip(self.labels, vector):

                    # add to coefficients
                    coefficients[label][pair[0]] += quantity / length
                    coefficients[label][pair[1]] += -quantity / length

        # if binary classifier, use negative of decision boundary for second class
        if len(self.models[model].classes_) == 2:

            # go through each label
            for label in self.labels + ['intercepts']:

                # go through each category
                for category in identifiers:

                    # change value
                    coefficients[label][category] *= -1

        # dump into attribute
        self.coefficients[model] = coefficients

        return None

    def take(self, model='linear'):
        """Take the coefficients from the file.

        Arguments:
            None

        Returns:
            None
        """

        # dump json file
        path = self.directory.replace('machines/', '') + '.json'
        print('retrieving ' + path + '...')
        with open(path, 'r') as pointer:

            # load json
            self.coefficients[model] = json.load(pointer)

        # retest and validate
        self.test()
        self.validate()

        return None

    def test(self):
        """Test the support vector machine models.

        Arguments:
            None

        Returns:
            None
        """

        # repeat for holdouts and training
        batches = [self.training, self.holdouts]

        # go through each batch
        for batch in batches:

            # create matrix
            matrix = [self.vectorize(sample) for sample in batch]

            # predict with all three models
            predictions = self.predict(matrix, 'linear')
            predictionsii = self.predict(matrix, 'ovr')
            predictionsiii = self.predict(matrix, 'ovo')

            # create samples
            zipper = zip(batch, predictions, predictionsii, predictionsiii)
            for sample, prediction, predictionii, predictioniii in zipper:

                # add predictions
                sample['prediction_linear'] = prediction
                sample['prediction_ovr'] = predictionii
                sample['prediction_ovo'] = predictioniii

                # add superpredictions
                sample['superprediction_linear'] = self.registry[prediction]
                sample['superprediction_ovr'] = self.registry[predictionii]
                sample['superprediction_ovo'] = self.registry[predictioniii]

        return None

    def train(self):
        """Train the support vector machine models.

        Arguments:
            None

        Returns:
            None
        """

        # gather weights
        weights = {category: self.weights[self.groups.index(self.registry[category])] for category in self.categories}

        # create matrix from training examples
        matrix = [self.vectorize(sample) for sample in self.training]
        targets = [sample['category'] for sample in self.training]

        # train linearsvc
        model = LinearSVC(class_weight=weights, C=self.regularization)
        model.fit(matrix, targets)
        self.models['linear'] = model

        # tabulate linear coefficients
        self.tabulate('linear')

        # train SVC
        model = SVC(kernel='linear', class_weight=weights, decision_function_shape='ovr', C=self.regularization)
        model.fit(matrix, targets)
        self.models['ovr'] = model

        # tabulate coefficients
        self.tabulate('ovr')

        # train SVC
        model = SVC(kernel='linear', class_weight=weights, decision_function_shape='ovo', C=self.regularization)
        model.fit(matrix, targets)
        self.models['ovo'] = model

        # tabulate coefficients
        self.tabulate('ovo')

        return None

    def validate(self):
        """Calculate confusion matrices.

        Arguments:
            None

        Returns:
            None
        """

        # begin accumulation for text file
        validation = []

        # create titles
        titles = []
        suffixes = ['ovo', 'ovr', 'linear']
        batches = ['training', 'holdouts']
        for batch in batches:

            # go through suffixes
            for suffix in suffixes:

                # make title
                title = 'SCV_' + suffix + ', ' + batch
                titles.append(title)
                titles.append(title)

        # create other fields
        fields = ['category', 'group'] * 6
        results = [['prediction_' + suffix, 'superprediction_' + suffix] for suffix in suffixes] * 2
        results = [result for pair in results for result in pair]
        blocks = [self.training] * 6 + [self.holdouts] * 6
        suites = [self.categories, self.groups] * 6

        # create zipper
        zipper = [quartet for quartet in zip(titles, fields, results, blocks, suites)]

        # go through zipper
        for title, field, result, block, suite in zipper:

            # print title
            validation.append(title + ':')

            # get confusion for training
            truths = [sample[field] for sample in block]
            predictions = [sample[result] for sample in block]
            confusion = self.confuse(truths, predictions, suite)
            for row in confusion:

                # accumulation.append
                validation.append(row)

        # save validation file
        self.validation = validation

        # print results
        for line in validation:

            # print to screen
            print(line)

        # pause
        pause = input('')

        return None

    def vectorize(self, sample):
        """Create a vector from a sample.

        Arguments:
            sample: dict

        Returns:
            list of floats
        """

        # make vector
        vector = [0] * len(self.labels)
        for label in sample['labels']:

            # add to vector
            index = self.mirror[label['Name']]
            vector[index] = label['Confidence'] / 100

        return vector

    def view(self, record):
        """View a sample record.

        Arguments:
            record: dict

        Returns:
            None
        """

        # pretty print it
        pprint(record)

        return None


# script to build relevance model
machine = Machine('machines/relevance', reverse=True, weights=[0.2, 1.0], regularization=0.2)
# machine.analyze()

