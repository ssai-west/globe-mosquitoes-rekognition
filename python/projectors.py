# projectors.py to grab photo urls from csvs and download them

# import reload
from importlib import reload

# import csv, os, json
import os
import csv
import json

# import request
from urllib.request import urlopen

# import datetime
from datetime import datetime, timedelta

# import pil
from PIL import Image

# import pprint
from pprint import pprint

# import random
from random import random

# import Counter
from collections import Counter

# import numpy
import numpy

# import pandas
import pandas

# import math
from math import log, sqrt

# import random forest
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.svm import SVC, LinearSVC


# Projector class to project evaluation images
class Projector(list):
    """Class to project and diagnose AI system.

    Inherits from:
        list
    """

    def __init__(self, directory='evaluation'):
        """Initialize instance with directory name.

        Arguments:
            directory: file path
        """

        # set directory
        self.directory = directory

        # establish files
        self.diagnosis = directory + '/diagnosis.jpg'
        self.notes = directory + '/notes.json'
        self.summaries = directory + '/summaries.json'
        
        # establish criteria
        self.criteria = {}
        self._establish()

        # establish extracts
        self.elements = {}
        self._extract()

        return

    def _apply(self, filter, collection):
        """Apply a boolean filter to a list.

        Arguments:
            filter: function object.
            collection: list

        Returns:
            list
        """

        # apply function
        members = [member for member in collection if filter(member)]

        return members

    def _attach(self, attachments, records, place):
        """Attach members of one dataset to matching member of the other.

        Arguments:
            attachments: dict
            records: dict
            place: str

        Returns:
            dict
        """

        # go through records
        for name, record in records.items():

            # attach same record at the place
            record[place] = attachments.setdefault(name, [])

        return records

    def _bound(self, polygon):
        """Create a bounding box from the polygon.

        Arguments:
            polygon: list of dicts

        Returns:
            dict
        """

        # get xs
        xs = [point['X'] for point in polygon]
        ys = [point['Y'] for point in polygon]

        # describe box
        left = min(xs)
        top = min(ys)
        width = max(xs) - min(xs)
        height = max(ys) - min(ys)

        # make box
        box = {'Left': left, 'Top': top, 'Width': width, 'Height': height}

        return box

    def _corner(self, box):
        """Get the corner points of the box.

        Arguments:
            box: dict

        Returns:
            list of floats
        """

        # make corner
        corner = [round(box['Left'], 3), round(box['Top'], 3)]

        return corner

    def _dump(self, data, path):
        """Dump data into a json file at the path.

        Arguments:
            data: dict
            path: file path

        Returns:
            None
        """

        # print status
        print('dumping into {}...'.format(path))

        # dump data
        with open(path, 'w') as pointer:

            # dump
            json.dump(data, pointer)

        return None

    def _establish(self):
        """Establish search criteria.
        
        Arguments:
            None
            
        Returns:
            None
            
        Populates:
            self.criteria
        """

        # define main criteria
        self.criteria['true_positives'] = lambda feature: lambda record: record['study'] == 'approvals' and record['approve']
        self.criteria['false_positives'] = lambda feature: lambda record: record['study'] == 'rejections' and record['approve']
        self.criteria['true_negatives'] = lambda feature: lambda record: record['study'] == 'rejections' and not record['approve']
        self.criteria['false_negatives'] = lambda feature: lambda record: record['study'] == 'approvals' and not record['approve']

        # define other criteria
        self.criteria['has'] = lambda feature: lambda record: sum(record[feature]['counts']) > 0
        self.criteria['no'] = lambda feature: lambda record: sum(record[feature]['counts']) == 0
        self.criteria['flag'] = lambda feature: lambda record: feature in record['flags']
        self.criteria['name'] = lambda feature: lambda record: feature in record['name']
        self.criteria['note'] = lambda feature: lambda record: feature in record['notes']
        self.criteria['not'] = lambda feature: lambda record: feature not in record['flags']
        self.criteria['only'] = lambda feature: lambda record: record['flags'] == [feature]

        # define label criteria
        labeling = lambda record: [entry['name'] for entry in record['labels']['labels'][0] + record['labels']['labels'][1]]
        self.criteria['label'] = lambda feature: lambda record: feature.capitalize() in labeling(record)

        return None

    def _extract(self):
        """Define extractions for each element.

        Arguments:
            None

        Returns:
            None

        Populates:
            self.elements
            self.reservoirs
        """

        # define reservoir and condition for labels
        reservoir = 'Labels'
        condition = lambda detection: True
        instances = lambda detection: [detection]

        # define labels extracts
        extracts = {}
        extracts.update({'name': lambda instance: instance['Name']})
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'labels': element})

        # define reservoir and condition for moderations
        reservoir = 'ModerationLabels'
        condition = lambda detection: True
        instances = lambda detection: [detection]

        # define moderations extracts
        extracts = {}
        extracts.update({'name': lambda instance: instance['Name']})
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'moderations': element})

        # define reservoir and condition for texts
        reservoir = 'TextDetections'
        condition = lambda detection: detection['Type'] == 'LINE'
        instances = lambda detection: [detection]

        # define texts extracts
        extracts = {}
        extracts.update({'text': lambda instance: instance['DetectedText']})
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        extracts.update({'corner': lambda instance: self._corner(self._bound(instance['Geometry']['Polygon']))})
        extracts.update({'size': lambda instance: self._size(self._bound(instance['Geometry']['Polygon']))})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'texts': element})

        # define reservoir and condition for faces
        reservoir = 'FaceDetails'
        condition = lambda detection: True
        instances = lambda detection: [detection]

        # define faces extracts
        extracts = {}
        extracts.update({'emotion': lambda instance: instance['Emotions'][0]['Type']})
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        extracts.update({'corner': lambda instance: self._corner(instance['BoundingBox'])})
        extracts.update({'size': lambda instance: self._size(instance['BoundingBox'])})
        extracts.update({'brightness': lambda instance: round(instance['Quality']['Brightness'], 2)})
        extracts.update({'sharpness': lambda instance: round(instance['Quality']['Sharpness'], 2)})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'faces': element})

        # define reservoir and condition for persons
        reservoir = 'Labels'
        condition = lambda detection: detection['Name'] == 'Person'
        instances = lambda detection: detection['Instances']

        # define persons extracts
        extracts = {}
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        extracts.update({'corner': lambda instance: self._corner(instance['BoundingBox'])})
        extracts.update({'size': lambda instance: self._size(instance['BoundingBox'])})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'persons': element})

        # define reservoir and condition for persons
        reservoir = 'Labels'
        condition = lambda detection: detection['Name'] == 'Car'
        instances = lambda detection: detection['Instances']

        # define persons extracts
        extracts = {}
        extracts.update({'confidence': lambda instance: round(instance['Confidence'], 2)})
        extracts.update({'corner': lambda instance: self._corner(instance['BoundingBox'])})
        extracts.update({'size': lambda instance: self._size(instance['BoundingBox'])})
        element = {'reservoir': reservoir, 'condition': condition, 'instances': instances, 'extracts': extracts}
        self.elements.update({'cars': element})

        return None

    def _load(self, path):
        """Load up the json file at the path.

        Arguments:
            path: file path

        Returns:
            dict
        """

        # print status
        print('loading {}...'.format(path))

        # load file
        with open(path, 'r') as pointer:

            # get contents
            contents = json.load(pointer)

        return contents

    def _populate(self, *criteria):
        """Populate the projector with summary data according to criteria.
        
        Arguments:
            *criteria: unpacked tuple of str or function objects
            
        Returns:
            None
            
        Populates:
            self
        """

        # depopulate
        while len(self) > 0:

            # pop out
            discard = self.pop()

        # load up summaries and notes
        summaries = self._load(self.summaries)
        notes = self._load(self.notes)

        # attach the notes to the summaries and reduce to a list
        summaries = self._attach(notes, summaries, 'notes')
        summaries = [value for value in summaries.values()]

        # apply criteria to summaries
        for criterion in criteria:

            # assume criterion is a function
            try:

                # and apply
                summaries = self._apply(criterion, summaries)

            # otherwise
            except TypeError:

                # split string
                try:

                    # apply filter
                    command, feature = (criterion + ' ').split(' ')[:2]
                    filter = self.criteria[command](feature)
                    summaries = self._apply(filter, summaries)

                # othewise
                except KeyError:

                    # pass
                    pass

        # load into self
        [self.append(summary) for summary in summaries]

        return None

    def _size(self, box):
        """Get the size of the box.

        Arguments:
            box: dict

        Returns:
            list of floats
        """

        # make corner
        size = [round(box['Width'], 3), round(box['Height'], 3)]

        return size

    def clear(self, index):
        """Clear all marks made on the comparison.

        Arguments:
            index: int

        Returns:
            None
        """

        # put image into diagnosis
        image = Image.open(self[index]['comparison'])
        image.save(self.diagnosis)

        return None

    def contribute(self, record, machine):
        """Calculate the contribution of each label to each score.

        Arguments:
            record: dict
            machine: dict

        Returns:
            dict
        """

        # get the coefficients
        intercepts = machine['intercepts']
        categories = [key for key in machine['intercepts'].keys()]

        # get contributions to each category from each label
        contributions = {category: {} for category in categories}
        for category in categories:

            # go through labels
            for chunk in record['labels']['labels']:

                # go through each chunk
                for label in chunk:

                    # unpack
                    name = label['name']
                    confidence = label['confidence']

                    # check for presence
                    if name in machine.keys():

                        # calculate contribution
                        contribution = machine[name][category] * confidence / 100
                        contributions[category][name] = contribution

            # get total score
            contributions[category]['score'] = intercepts[category] + sum(contributions[category].values())

        return contributions

    def draw(self, index, command):
        """Draw boxes around an element.

        Arguments:
            index: int
            command: str
        """

        # get element and index
        try:

            # get element and number
            element, number = command.split()[1:]
            number = int(number)

        # otherwise
        except ValueError:

            # get element and set number to none
            element = command.split()[1]
            number = None

        # draw boxes around elements
        try:

            # get elements
            elements = self[index][element][element][0] + self[index][element][element][1]
            for indexii, member in enumerate(elements):

                # check against number
                if number == indexii or number is None:

                    # draw outline
                    self.outline(member['corner'], member['size'])

        # otherwise
        except (KeyError, IndexError):

            # print
            print('improper selection!')

        return None

    def fling(self, index, command):
        """Fling the image from the record into a new folder for analysis later.

        Arguments:
            record: dict
            folder: str, the path name

        Returns:
            None
        """

        # get record name and folder
        path = self[index]['name']
        folder = command.split()[1]

        # get stub
        name = path.split('/')[-1]

        # get name
        image = Image.open(path)
        image.save(folder + '/' + name)

        return None

    def forget(self, index, command):
        """Remove a note from the record.

        Arguments:
            index: int
            command: str

        Returns:
            None
        """

        # get note
        message = command.split()[1]

        # remove note from record
        self[index]['notes'] = [entry for entry in self[index]['notes'] if entry != message]

        # remove note from file
        notes = self._load(self.notes)
        name = self[index]['name']
        notes[name] = [entry for entry in notes[name] if entry != message]
        self._dump(notes, self.notes)

        return None

    def imply(self, note, implication, remove=False):
        """Imply one note for a photo from another.

        Arguments:
            note: str
            implication: str
            remove=False: boolean, remove old note?

        Returns:
            None
        """

        # get notes
        notes = self._load(self.notes)

        # go through notes
        for name, messages in notes.item():

            # add implication
            if note in messages:

                # add implication
                messages.append(implication)
                messages = list(set(messages))

                # remove old note
                if remove:

                    # remove old note
                    messages = [message for message in messages if message != note]

                # reassign
                notes[name] = messages

        # update file
        self._dump(notes, self.notes)

        return None

    def inspect(self, *criteria, skip=0):
        """Inspect a subset of evaluations data.

        Arguments:
            *criteria: unpacked tuple of functions or strings
            skip=0: number to skip to

        Returns:
            None
        """

        # perform summariziation
        self.summarize()

        # populate based on the criteria
        self._populate(*criteria)

        # go through records
        for number, record in enumerate(self[skip:]):

            # set index
            index = number + skip

            # put comparison at diagnosis
            self.clear(index)

            # print review
            print('\ncriteria: {}'.format(list(criteria)))
            self.review(index)

            # offer choices until done
            done = self.offer(index)

            # break if done
            if done:

                # end
                break

        return None

    def model(self, index, command, top=5):
        """Display current model results.

        Arguments:
            index: int
            command: str
            top: int, number of top contributions to show

        Returns:
            None
        """

        # split commands
        test = command.split()[1]

        # get element and index
        try:

            # get machine
            machine = self._load(test + '.json')

            # prepare sample from record
            samples = self.prepare(index, test)
            for number, sample in enumerate(samples):

                # get contributions
                contributions = self.contribute(sample, machine)
                categories = [key for key in contributions.keys()]

                # get class scores
                scores = [(category, round(contributions[category]['score'], 2)) for category in categories]
                scores.sort(key=lambda score: score[1], reverse=True)

                # get prediction
                prediction = scores[0][0]

                # print heading
                print('\n#{} of {} -!-> {}\n'.format(number, len(samples), prediction))

                # print scores
                [print(score) for score in scores]

                # get top factors for wrong answer
                print('\ntop factors for {}:'.format(prediction))
                factors = [(item[0], round(item[1], 2)) for item in contributions[prediction].items()]
                factors = [item for item in factors if item[0] != 'score']
                factors.sort(key=lambda pair: pair[1], reverse=True)
                [print('     {}: {}'.format(*factor)) for factor in factors[:top]]

        # otherwise
        except FileNotFoundError:

            # print
            print('improper selection!')

        return None

    def note(self, index, command):
        """Add a note to the record.

        Arguments:
            index: int
            command: str

        Returns:
            None
        """

        # get note
        message = command.split()[1]

        # add note to record
        self[index]['notes'].append(message)

        # add to notes file
        notes = self._load(self.notes)
        name = self[index]['name']
        notes[name].append(message)
        self._dump(notes, self.notes)

        return None

    def notify(self):
        """Tally up notes.

        Arguments:
            secondary=

        Returns:
            None
        """

        # open file
        notes = self._load(self.notes)

        # count all notes
        entries = [note for entry in notes.values() for note in entry]
        counter = Counter(entries)

        # print entries
        print(' ')
        entries = [item for item in counter.items()]
        entries.sort(key=lambda pair: pair[1], reverse=True)
        entries.sort(key=lambda pair: pair[0][0])
        [print(entry) for entry in entries]

        return None

    def offer(self, index):
        """Offer choices.

        Arguments:
            index: int

        Returns:
            boolean, done inspecting?
        """

        # make list of commands
        commands = {}
        commands.update({'review': lambda pause: self.review(index)})
        commands.update({'clear': lambda pause: self.clear(index)})
        commands.update({'view': lambda pause: self.view(index)})
        commands.update({'note': lambda pause: self.note(index, pause)})
        commands.update({'forget': lambda pause: self.forget(index, pause)})
        commands.update({'draw': lambda pause: self.draw(index, pause)})
        commands.update({'model': lambda pause: self.model(index, pause)})
        commands.update({'fling': lambda pause: self.fling(index, pause)})

        # set done to false by default
        done = False

        # pause
        pause = input('??')

        # get input
        while pause not in ('', ' ') and not done:

            # perform command
            command = pause.split()[0]
            if command in commands.keys():

                # perform command
                commands[command](pause)

            # check for exit
            if pause in ('XX', 'XXX', 'xx', 'xxx', 'done', 'exit', 'Exit', 'Done'):

                # set done to true and exit
                print('exiting...')
                done = True

            # get new pause
            pause = input('??')

        return done

    def outline(self, corner, size):
        """Outline the given element.

        Arguments:
            index: int
            corner: list of floats
            size: list of floats

        Returns:
            None
        """

        # load in diagnosis
        image = Image.open(self.diagnosis)
        array = numpy.array(image)
        shape = array.shape

        # unpack coordinates
        left, top = corner
        width, height = size

        # change to absolute pixels
        horizontal = int(shape[1] / 2)
        vertical = int(shape[0])
        left = int(left * horizontal)
        width = int(width * horizontal)
        top = int(top * vertical)
        height = int(height * vertical)

        # get coordinates for horizontal lines
        points = []
        for coordinate in range(left, left + width):

            # thick lines
            for thickness in range(3):

                # add to points
                points.append((coordinate, top + thickness))
                points.append((coordinate + horizontal, top + thickness))
                points.append((coordinate, top + height - thickness))
                points.append((coordinate + horizontal, top + height - thickness))

        # get coordinates for vertical lines
        for coordinate in range(top, top + height):

            # thick lines
            for thickness in range(3):

                # add to points
                points.append((left + thickness, coordinate))
                points.append((left + horizontal + thickness, coordinate))
                points.append((left + width - thickness, coordinate))
                points.append((left + horizontal + width - thickness, coordinate))

        # convert all points to turquoise
        points = list(set(points))
        for column, row in points:

            # try to draw box
            try:

                # convert to orange
                array[row][column][0] = 0
                array[row][column][1] = 255
                array[row][column][2] = 255

            # unless index error
            except IndexError:

                # skip
                pass

        # save image
        image = Image.fromarray(array)
        image.save(self.diagnosis)

        return None

    def prepare(self, index, test):
        """Prepare a sample from a record.

        Arguments:
            record: dict
            machine: str

        Returns:
            list of dict
        """

        # assume sample is the record
        record = self[index]
        samples = [record]

        # make texts samples
        if test == 'texts':

            # make text samples at teach tier
            samples = []
            for tier in (0, 1):

                # go through each text
                for text in record['texts']['texts'][tier]:

                    # begin sample
                    sample = {'labels': {'labels': [[], []]}}

                    # calculate complexity
                    complexity = (len(set(text['text'])) - 1) / len(text['text'])

                    # fill in features
                    sample['labels']['labels'][0].append({'name': 'Tier', 'confidence': -100 * tier + 100})
                    sample['labels']['labels'][0].append({'name': 'Confidence', 'confidence': text['confidence']})
                    sample['labels']['labels'][0].append({'name': 'Width', 'confidence': text['size'][0] * 100})
                    sample['labels']['labels'][0].append({'name': 'Height', 'confidence': text['size'][1] * 100})
                    sample['labels']['labels'][0].append({'name': 'Length', 'confidence': len(text['text'])})
                    sample['labels']['labels'][0].append({'name': 'Complexity', 'confidence': complexity * 100})

                    # add to samples
                    samples.append(sample)

        # make texts sample
        if test == 'faces':

            # make text samples at teach tier
            samples = []
            for tier in (0, 1):

                # go through each text
                for face in record['faces']['faces'][tier]:

                    # begin sample
                    sample = {'labels': {'labels': [[], []]}}

                    # fill in features
                    sample['labels']['labels'][0].append({'name': 'Tier', 'confidence': -100 * tier + 100})
                    sample['labels']['labels'][0].append({'name': 'Confidence', 'confidence': face['confidence']})
                    sample['labels']['labels'][0].append({'name': 'Width', 'confidence': face['size'][0] * 100})
                    sample['labels']['labels'][0].append({'name': 'Height', 'confidence': face['size'][1] * 100})
                    sample['labels']['labels'][0].append({'name': 'Brightness', 'confidence': face['brightness']})
                    sample['labels']['labels'][0].append({'name': 'Sharpness', 'confidence': face['sharpness']})

                    # add to samples
                    samples.append(sample)

        return samples

    def review(self, index):
        """Review a record.

        Arguments:
            index: int
            criteria: list of str or function objects

        Returns:
            None
        """

        # get record
        record = self[index]

        # summarize status
        print(' ')
        print('#{} of {}: '.format(index, len(self)))
        print('name: {}'.format(record['name']))
        print(' ')
        print('approve: {}'.format(record['approve']))
        print('original: {}'.format(record['original']))
        print('flags: {}'.format(record['flags']))
        print(' ')

        # print elements
        for element in self.elements.keys():

            # print element summary
            print('{}: {}, {}'.format(element, *record[element]['counts']))

        # print notes
        print(' ')
        print('notes: {}'.format(record['notes']))

        return None

    def summarize(self):
        """Summarize counts of evaluation study.

        Arguments:
            None

        Returns:
            None
        """

        # check directory for json files
        studies = [study for study in os.listdir(self.directory) if '.' not in study and '_' not in study]

        # collect data
        data = {}
        for study in studies:

            # get data and attach notes
            data[study] = self._load('{}/{}.json'.format(self.directory, study))

        # make summaries
        summaries = {}
        for study in studies:

            # go through records
            for name, record in data[study].items():

                # begin summary
                comparison = name.replace('.jpg', '_comparison.jpg').replace(study, 'staples')
                summary = {'name': name, 'study': study, 'comparison': comparison}

                # add statuses and flags
                for status in ('approve', 'original', 'flags'):

                    # add status
                    summary.update({status: record[status]})

                # summarize all elements
                elements = [element for element in self.elements.keys()]
                for element in elements:

                    # get the extractions recipe
                    reservoir = self.elements[element]['reservoir']
                    condition = self.elements[element]['condition']
                    instances = self.elements[element]['instances']
                    extracts = self.elements[element]['extracts']

                    # add element
                    summary[element] = {'counts': [], element: []}

                    # go through each tier
                    for tier in ('detections', 'redetections'):

                        # go through each detection and extract it
                        extractions = []
                        detections = [detection for detection in record[tier][reservoir] if condition(detection)]
                        for detection in detections:

                            # get instances
                            for instance in instances(detection):

                                # add extract
                                extraction = {field: extract(instance) for field, extract in extracts.items()}
                                extractions.append(extraction)

                        # add to summary
                        summary[element]['counts'].append(len(extractions))
                        summary[element][element].append(extractions)

                # add to summary
                summaries[name] = summary

        # save summaries
        self._dump(summaries, self.summaries)

        return None

    def tally(self, *criteria):
        """Tally up stats on false and true positives and negatives, given other criteria.

        Arguments:
            *mutes=unpacked tuple of strings
            protocol=None: filter by protocol

        Returns:
            None
        """

        # tally
        tallies = {}

        # populate according to criteria
        self._populate(*criteria)

        # tally up
        for filter in ('false_positives', 'false_negatives', 'true_positives', 'true_negatives'):

            # add tally
            tallies[filter] = len(self._apply(self.criteria[filter](''), self))

        # print confustion matrix
        print(' ')
        print('confusion matrix: ')
        print('              a', ' r')
        print(' approvals: ', tallies['true_positives'], tallies['false_negatives'])
        print('rejections: ', tallies['false_positives'], tallies['true_negatives'])

        return None

    def view(self, index):
        """View a record.

        Arguments:
            index: int

        Returns:
            None
        """

        # pretty print record
        pprint(self[index])

        return None


# make projector
# projector = Projector()

