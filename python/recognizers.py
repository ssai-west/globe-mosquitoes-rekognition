from importlib import reload
import base64
import boto3
from PIL import Image
import numpy


def moderate(photo):
    client = boto3.client('rekognition')

    with open(photo, 'rb') as image:
        response = client.detect_moderation_labels(Image={'Bytes': image.read()})

    # print
    print('.')
    # print('Detected moderation labels in ' + photo)
    # for label in response['ModerationLabels']:
    #     print(label['Name'] + ' : ' + str(label['Confidence']))

    return response

def superimpose(insert, image, row, column):
    """Superimpose a small insert onto a larger image at particular row and column.

    Arguments:
        insert: numpy array
        image: numpy array
        row: int
        column: int

    Returns:
        numpy array
    """

    # # get numpy array
    # insert = numpy.array(insert)
    # image = numpy.array(image)

    # get insert dimensions
    height, width, _ = insert.shape

    # go through rows
    for index in range(height):

        # add insert
        image[index + row][column: column + width] = insert[index]

    # # return to png
    # image = Image.fromarray(image)

    return image


# walk a superimposed image around a bigger image
def walk(insert, image):
    """Walk an inserted image around a larger image and get report.

    Arguments:
        insert: str, image name
        image: str, image name

    Returns:
        None
    """

    #print
    print('superimposing', end='')

    # load up images and convert to arrays
    insert = numpy.array(Image.open(insert))
    image = numpy.array(Image.open(image))

    # get ending rows and columns
    height = image.shape[0] - insert.shape[0]
    width = image.shape[1] - insert.shape[1]

    # walk the image around
    confidences = []
    for row in range(0, height, 5):

        # walk image across
        for column in range(0, width, 5):

            # counter
            print('.', end='')

            # make superimposition
            superimposition = superimpose(insert, image, row, column)

            # save
            Image.fromarray(superimposition).save('superimposition.png')

            # get response
            response = moderate('superimposition.png')

            # find a suggestive confidence
            confidence = 0.0
            try:

                # get confidence
                labels = response['ModerationLabels']
                scores = [score for score in labels if score['Name'] == 'Suggestive']
                confidence = scores[0]['Confidence']

            # otherwise pass
            except (KeyError, IndexError):

                # pass
                pass

            # append confidence
            confidences.append(confidence)

    # report statistics
    stats = {}
    stats['count'] = len(confidences)
    stats['minimum'] = min(confidences)
    stats['maximum'] = max(confidences)
    stats['mean'] = numpy.mean(confidences)
    stats['deviation'] = numpy.std(confidences)

    # print report
    print(' ')
    for field, value in stats.items():

        # print
        print('{}: {}'.format(field, value))

    return None



