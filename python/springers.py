# spring.py helper functions for spring2018 cloud study

# import reload
from importlib import reload

# import system tools
import os
import csv
import sys
import json
import re
import shutil

# read in xls files
import xlwt
import xlrd

# import request
from urllib.request import urlopen

# import pil
from PIL import Image

# import ppriint
import pprint

# import Core
from python.cores import Core


# define class Springer to manage Spring2018 image files
class Springer(Core):
    """class Springer to manage Spring2018 photo files.

    Inherits from:
        cores.Core
    """

    def __init__(self, directory):
        """Initialize a Springer instance wth a directory.

        Arguments:
            directory: str, folder name
        """

        # initialize the base Hydra instance
        super().__init__()

        # add directory and directory stub
        self.directory = directory
        self.stub = directory.split('/')[-1]

        return

    def __repr__(self):
        """Create onscreen representation.

        Arguments:
            None

        Returns:
            str
        """

        # create representation
        representation = '< Springer at: {} >'.format(self.directory)

        return representation

    def capture(self, number=5, ignore=False):
        """Capture the images by downloading from the urls.

        Arguments:
            number: int, number of images
            ignore: false, ignore scanned status?

        Returns:
            None
        """

        # get json
        photos = self._load('{}/{}.json'.format(self.directory, self.stub))

        # if not ignore
        if not ignore:

            # get the subset that have not been scanned
            photos = [photo for photo in photos if not photo['scanned']]

        # sort by index and take the number
        photos.sort(key=lambda record: record['index'])
        photos = photos[:number]

        # begin sizes
        sizes = {}

        # capture each image
        for index, photo in enumerate(photos):

            # get url
            url = photo['url']

            # make image
            image = Image.open(urlopen(url))

            # determine image size
            size = image.size[0] * image.size[1] / (1024 ** 2)
            size = round(size, 2)

            # save
            deposit = photo['image']
            print('writing {}, image {} of {}...{} MB'.format(deposit, index, number, size))
            image.convert('RGB').save(deposit)

            # append
            sizes[deposit] = size

        # create sizes file
        self._dump(sizes, '{}/sizes.json'.format(self.directory))

        return None

    def compile(self):
        """Compile latest aws results into spreadsheet.

        Arguments:
            None

        Returns:
            None
        """

        # open up photos and results
        photos = self._load('{}/{}.json'.format(self.directory, self.stub))
        results = self._load('{}/originals.json'.format(self.directory))

        # dump a copy of the records into archives
        xerox = '{}/{}_originals_copy.json'.format(self.directory.replace(self.stub, 'archives'), self.stub)
        self._dump(results, xerox)

        # for each record
        for photo in photos:

            # try to
            try:

                # get result
                result = results[photo['image']]

                # update information
                photo['scanned'] = True
                photo['modified'] = not result['original']
                photo['approved'] = result['approve']

                # add texts to flags
                flags = result['flags']

                # check for texts
                if len(result['detections']['TextDetections']) + len(result['redetections']['TextDetections']) > 0:

                    # add text to flags
                    flags.append('texts')

                # add flags to photo record
                photo['flags'] = ', '.join(flags)

            # unless not in record
            except KeyError:

                # skip
                pass

        # self dump updated records
        self._dump(photos, '{}/{}.json'.format(self.directory, self.stub))

        # create spreadsheet
        headers = ['index', 'url', 'scanned', 'modified', 'approved', 'flags']

        # sort photos
        photos.sort(key=lambda photo: photo['index'])

        # create csv rows
        rows = [[photo[header] for header in headers] for photo in photos]

        # write workbook
        workbook = xlwt.Workbook()
        sheet = workbook.add_sheet('photos')

        # go through each row
        for index, row in enumerate([headers] + rows):

            # go through each column
            for column, info in enumerate(row):

                # add to sheet
                sheet.write(index, column, str(info))

        # save workbook
        deposit = '{}/{}ScannedPhotos.xls'.format(self.directory, self.stub.capitalize())
        print('saving {}...'.format(deposit))
        workbook.save(deposit)

        return None

    def fling(self, start=0, finish=0, *flags):
        """Fling the flagged results into troubleshooting for diagnosis.

        Arguments:
            start: int, starting photo index
            finish: int, finishing photo index
            *flag: unpacked list of str, particular flags

        Returns:
            None
        """

        # open up photos
        photos = self._load('{}/{}.json'.format(self.directory, self.stub))

        # subset photos from start to finish
        start = start or 0
        finish = finish or len(photos)
        photos = photos[start: finish]

        # open up results
        results = self._load('{}/originals.json'.format(self.directory))

        # make originals folder in troubleshooting
        troubleshooting = self.directory.replace(self.stub, 'troubleshooting')
        self._make('{}/originals'.format(troubleshooting))
        self._make('{}/erasures'.format(troubleshooting))
        self._make('{}/diagnosis'.format(troubleshooting))
        self._make('{}/diagnosis/staples'.format(troubleshooting))

        # depopulate the folders
        [os.remove(path) for path in self._see('{}/originals'.format(troubleshooting))]
        [os.remove(path) for path in self._see('{}/erasures'.format(troubleshooting))]
        [os.remove(path) for path in self._see('{}/diagnosis/staples'.format(troubleshooting))]

        # begin troubleshooting info
        originals = {}

        # subset photos
        photos = [photo for photo in photos if len(photo['flags']) > 2]

        # if keyword given
        if len(flags) > 0:

            # subset based on word
            photos = [photo for photo in photos if any([flag in photo['flags'] for flag in flags])]

        # go through each photo
        for photo in photos:

            # transfer the results to the file
            path = photo['image']

            # make deposit path and populate json
            deposit = path.replace(self.stub + '/', 'troubleshooting/')
            originals[deposit] = results[path]

            # grab the url and transfer to the folder
            url = photo['url']

            # make image and save
            image = Image.open(urlopen(url))
            print('writing image ' + deposit + '...')
            image.save(deposit)

        # deposit json
        self._dump(originals, '{}/originals.json'.format(troubleshooting))

        # deposit copy in diagnosis
        staples = {name.replace('originals', 'diagnosis/staples'): info for name, info in originals.items()}
        self._dump(staples, '{}/diagnosis/staples.json'.format(troubleshooting))

        # deposit notes
        self._dump({}, '{}/diagnosis/notes.json'.format(troubleshooting))

        return None

    def ingest(self, name=None):
        """Ingest the file of Photo urls.

        Arguments:
            name: str, name of xls file

        Returns:
            None
        """

        # set up originals and erasures folders for image deposits and aws information
        self._make('{}/originals'.format(self.directory))
        self._make('{}/erasures'.format(self.directory))

        # set default name file name and path
        name = name or '{}TrainedPhotos.xls'.format(self.stub.capitalize())
        path = '{}/{}'.format(self.directory, name)

        # read in workbook
        print('ingesting {}...'.format(path))
        workbook = xlrd.open_workbook(path)
        sheet = workbook.sheet_by_index(0)
        rows = [sheet.row_values(index) for index in range(sheet.nrows)]

        # make records
        records = []
        for index, row in enumerate(rows):

            # create record
            image = '{}/originals/{}_{}.jpg'.format(self.directory, self.stub, index)
            record = {'index': index, 'url': row[0], 'image': image, 'scanned': False}
            record.update({'flags': ' ', 'modified': False, 'approved': False})
            records.append(record)

        # create json
        self._dump(records, '{}/{}.json'.format(self.directory, self.stub))

        return None

    def look(self, index, *chain, level=1):
        """Look at the contents of an h5 file, to a certain level.

        Arguments:
            index: int, index of photo
            chain: unpacked list of str, additional chain of fields
            level=1: the max nesting level to see

        Returns:
            None
        """

        # open the results
        results = self._load('{}/inward.json'.format(self.directory))

        # find the images marked by the index
        images = [image for image in results.keys() if '_{}.jpg'.format(index) in image]
        image = images[0]

        # set branch
        branch = results[image]
        for link in chain:

            # step into branch
            branch = branch[link]

        # unpack into a dict
        data = self._unpack(branch, level=level)

        # pretty print it
        pprint.pprint(data)

        return None

    def prune(self):
        """Prune away image files that have already been run.

        Arguments:
            None

        Returns:
            None
        """

        # open up photos and convert to dictionary
        photos = self._load('{}/{}.json'.format(self.directory, self.stub))
        photos = {record['image']: record for record in photos}

        # get inward files
        images = self._see('{}/originals'.format(self.directory))

        # erase all scanned images
        for image in images:

            # delete records
            if photos[image]['scanned']:

                # delete
                os.remove(image)

        # remove all erasures
        erasures = self._see('{}/erasures'.format(self.directory))
        [os.remove(erasure) for erasure in erasures]

        return None

    def spring(self, number=50):
        """Advance one round for a number of photos.

        Arguments:
            number: int, number of new photos to load

        Returns:
            None
        """

        # compile latest data
        print('compiling latest results...')
        self.compile()

        # prune off images
        print('deleting scanned images...')
        self.prune()

        # capture new images
        print('capturing {} new images...'.format(number))
        self.capture(number=number)

        return None
