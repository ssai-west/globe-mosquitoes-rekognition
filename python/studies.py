# projectors.py to grab photo urls from csvs and download them

# import reload
from importlib import reload

# import csv, os, json
import os
import csv
import json

# import request
from urllib.request import urlopen

# import time
from time import time, sleep

# import datetime
from datetime import datetime, timedelta

# import pil
from PIL import Image

# import pprint
from pprint import pprint

# import random
from random import random

# import Counter
from collections import Counter

# import numpy
import numpy

# import pandas
import pandas

# import math
from math import log, sqrt, floor

# import random forest
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn.svm import SVC, LinearSVC

# import pyplot
from matplotlib import pyplot

# import pooling
from multiprocessing import Pool

# set protocols
protocols = ['mosquitoes', 'lands', 'clouds', 'trees']


# load
def load(path):
    """Load up the json file at the path.

    Arguments:
        path: file path

    Returns:
        dict
    """

    # load up dict
    with open(path, 'r') as pointer:

        # get file
        contents = json.load(pointer)

    return contents


# dump
def dump(contents, path):
    """Dump contents into a path.

    Arguments:
        path: file path

    Returns:
        dict
    """

    # dump contents
    with open(path, 'w') as pointer:

        # dump file
        json.dump(contents, pointer)

    return None


# make
def make(path):
    """Make a blank file.

    Arguments:
        path: path name

    Returns:
        None
    """

    # get empty contents
    contents = {}

    # dump the contents
    dump(contents, path)

    return None


# get photo urls
def absorb():
    """Absorb all photos urls from several protocols.

    Arguments:
        None

    Returns:
        None
    """

    # protocols
    photos = {protocol: [] for protocol in protocols}
    for protocol in protocols:

        # read in csv and find photos
        names = os.listdir('csvs/' + protocol)

        # get headings
        paths = ['csvs/' + protocol + '/' + name for name in names]

        # convert csvs
        documents = []
        for path in paths:

            # read in csvs
            with open(path, 'r') as pointer:

                # read in rows
                rows = [row for row in csv.reader(pointer)]

            # make dict
            headers = rows[0]
            records = [{field: info for field, info in zip(headers, row)} for row in rows[1:]]
            documents += records

        # find keys
        entries = []
        keys = [key for key in documents[0].keys() if 'photo' in key.lower()]
        for key in keys:

            # add to photo entries
            [entries.append(record[key]) for record in documents]

        # split by ';'
        urls = [url for entry in entries for url in entry.split(';')]
        urls = [url for url in urls if 'https' in url]
        photos[protocol] = urls

    # write to json
    with open('evaluation/approval_urls.json', 'w') as pointer:

        # dump into json
        json.dump(photos, pointer)

    # write feed file
    feed = []
    for protocol in protocols:

        # add urls
        feed += photos[protocol]

    # make feed data
    feed = {url: '' for url in feed}
    dump(feed, 'evaluation/mosquito_feed.json')

    return None


# capture photos by downloading them
def capture(protocol, number):
    """Capture the images by downloading from the urls.

    Arguments:
        number: int, number of images

    Returns:
        None
    """

    # get json
    with open('evaluation/approval_urls.json', 'r') as pointer:

        # get photos
        photos = json.load(pointer)

    # get list of urls
    urls = photos[protocol]

    # get images from each
    urls.sort(key=lambda member: random())
    urls = urls
    for index, url in enumerate(urls):

        # make image
        image = Image.open(urlopen(url))

        # save
        deposit = 'evaluation/approvals/' + protocol + '_' + str(index) + '.jpg'
        print('writing image ' + deposit + '...')
        image.save(deposit)

    return None


# prune deleted files from data
def prune():
    """Prune deleted images from data files for training.

    Arguments:
        None

    Returns:
        None
    """

    # define categories
    categories = ['mosquitoes', 'clouds', 'trees', 'lands', 'indoors', 'buildings', 'featureless', 'meritless']

    # check datafile against scan records
    datasets = []
    for category in categories:

        # load up datasets
        path = 'labels/' + category + 'cans.json'
        with open(path, 'r') as pointer:

            # get datasets
            dataset = json.load(pointer)

        # look in folder
        directory = 'labels/' + category
        names = os.listdir(directory)
        names = ['labels/' + category + '/' + name for name in names]

        # keep only those in names
        dataset = {key: value for key, value in dataset.items() if key in names}

        # deposit
        with open(path, 'w') as pointer:

            # dump dataset
            json.dump(dataset, pointer)

    return None


# build random forest mode
def train(fraction=0.2):
    """Train a support vector machine on labels.

    Arguments:
        fraction=0.2: holdout set fraction

    Returns:
        None
    """

    # define categories
    categories = ['mosquitoes', 'clouds', 'trees', 'lands', 'indoors', 'buildings', 'featureless', 'meritless']
    registry = {category: index for index, category in enumerate(categories)}

    # define relevants and irrelevants
    relevants = categories[:4]
    irrelevants = categories[4:]

    # define weights
    weights = [0.1, 0.1, 0.1, 0.1, 1.0, 1.0, 1.0, 1.0]
    weights = {index: weight for index, weight in enumerate(weights)}

    # gather up datasets
    datasets = []
    for category in categories:

        # load up datasets
        path = 'labels/' + category + 'cans.json'
        with open(path, 'r') as pointer:

            # get datasets
            dataset = json.load(pointer)
            datasets.append(dataset)

    # reduce datasets to labels
    data = []
    for category, dataset in zip(categories, datasets):

        # get labels
        dataset = [pair for pair in dataset.items()]
        dataset = [{'name': pair[0], 'category': category, 'labels': pair[1]['detections']['Labels']} for pair in dataset]
        data.append(dataset)

    # get all labels
    labels = [entry['Name'] for dataset in data for member in dataset for entry in member['labels']]
    labels = list(set(labels))
    labels.sort()

    # make label mirror
    mirror = {label: index for index, label in enumerate(labels)}

    # get category size and calculate holdout number
    size = min([len(datum) for datum in data])
    holdout = int(fraction * size)

    # construct matrix and targets
    targets = []
    matrix = []
    holdouts = []
    truths = []
    holds = []
    for category, dataset in zip(categories, data):

        # randomize and shrink dataset to minimum size
        dataset.sort(key=lambda entry: random())
        dataset = dataset[:size]

        # for each member
        for index, member in enumerate(dataset):

            # get target
            target = registry[category]

            # create a vector
            vector = [0.0] * len(labels)
            for entry in member['labels']:

                # add score
                name = entry['Name']
                score = entry['Confidence'] / 100
                vector[mirror[name]] = score

            # add to targets
            if index >= holdout:

                # add target
                targets.append(target)
                matrix.append(vector)

            # or to holdouts
            if index < holdout:

                # add to holdouts
                truths.append(target)
                holdouts.append(vector)
                holds.append(member)

    # make and svm
    #support = LinearSVC(class_weight=weights).fit(matrix, targets)
    support = SVC(class_weight=weights, kernel='linear').fit(matrix, targets)

    # predict on training samples
    predictions = support.predict(matrix)

    # check percents
    print('\ntraining:')
    confusion = {category: {category: 0 for category in categories} for category in categories}
    for category in categories:

        # find all truths
        index = registry[category]
        pairs = [(truth, prediction) for truth, prediction in zip(targets, predictions) if int(truth) == index]
        positives = [(truth, prediction) for truth, prediction in pairs if int(truth) == int(prediction)]

        # print percent
        percent = 100 * len(positives) / len(pairs)
        print('{}: {} %'.format(category, percent))

        # add entries to confustion matrix
        for pair in pairs:

            # update confusion matrix
            categoryii = categories[pair[1]]
            confusion[category][categoryii] += 1

    # print confusion matrix
    print('\nconfusion:')
    for category in categories:

        # print vector
        vector = [confusion[category][categoryii] for categoryii in categories]
        vector = [(' ' + str(entry))[-2:] for entry in vector]
        vector = str(vector).replace("'", '').replace(',', '')
        print(vector)

    # print relevant, irrelvant breakdown
    breakdown = {'keep': {'keep': 0, 'discard': 0}, 'discard': {'keep': 0, 'discard': 0}}
    pairs = [(truth, prediction) for truth, prediction in zip(targets, predictions)]
    fields = ['keep', 'discard']
    for truth, prediction in pairs:

        # get fields
        field = fields[int(truth > 3)]
        fieldii = fields[int(prediction > 3)]
        breakdown[field][fieldii] += 1

    # print percentages
    print(' ')
    print('relevants: {} %'.format(100 * breakdown['keep']['keep'] / sum(breakdown['keep'].values())))
    print('irrelevants: {} %'.format(100 * breakdown['discard']['discard'] / sum(breakdown['discard'].values())))

    # print confusion matrix
    print('\nconfusion:')
    for field in fields:

        # print vector
        vector = [breakdown[field][fieldii] for fieldii in fields]
        vector = [('  ' + str(entry))[-3:] for entry in vector]
        vector = str(vector).replace("'", '').replace(',', '')
        print(vector)

    # predict on holdouts
    predictions = support.predict(holdouts)

    # check holdouts
    print('\nholdouts:')
    confusion = {category: {category: 0 for category in categories} for category in categories}
    for category in categories:

        # find all truths
        index = registry[category]
        pairs = [(truth, prediction) for truth, prediction in zip(truths, predictions) if int(truth) == index]
        positives = [(truth, prediction) for truth, prediction in pairs if int(truth) == int(prediction)]

        # print percent
        percent = 100 * len(positives) / len(pairs)
        print('{}: {} %'.format(category, percent))

        # add entries to confustion matrix
        for pair in pairs:

            # update confusion matrix
            categoryii = categories[pair[1]]
            confusion[category][categoryii] += 1

    # print confusion matrix
    print('\nconfusion:')
    for category in categories:

        # print vector
        vector = [confusion[category][categoryii] for categoryii in categories]
        vector = [(' ' + str(entry))[-2:] for entry in vector]
        vector = str(vector).replace("'", '').replace(',', '')
        print(vector)

    # print relevant, irrelvant breakdown
    breakdown = {'keep': {'keep': 0, 'discard': 0}, 'discard': {'keep': 0, 'discard': 0}}
    pairs = [(truth, prediction) for truth, prediction in zip(truths, predictions)]
    fields = ['keep', 'discard']
    for truth, prediction in pairs:

        # get fields
        field = fields[int(truth > 3)]
        fieldii = fields[int(prediction > 3)]
        breakdown[field][fieldii] += 1

    # print percentages
    print(' ')
    print('relevants: {} %'.format(100 * breakdown['keep']['keep'] / sum(breakdown['keep'].values())))
    print('irrelevants: {} %'.format(100 * breakdown['discard']['discard'] / sum(breakdown['discard'].values())))

    # print confusion matrix
    print('\nconfusion:')
    for field in fields:

        # print vector
        vector = [breakdown[field][fieldii] for fieldii in fields]
        vector = [('  ' + str(entry))[-3:] for entry in vector]
        vector = str(vector).replace("'", '').replace(',', '')
        print(vector)

    # print mismatched holdouts
    for entry, prediction in zip(holds, predictions):

        # if mismatch
        if categories[int(prediction)] != entry['category']:

            # check for broad mismatch
            if (prediction < 4) != (registry[entry['category']] < 4):

                # print
                print(' ')
                print('name: {}'.format(entry['name']))
                print('category: {}'.format(entry['category']))
                print('prediction: {}'.format(categories[int(prediction)]))

                # print labels
                index = registry[entry['category']]
                for label in datasets[index][entry['name']]['detections']['Labels']:

                    # print
                    print(label)

    # print holdout confusion matrix again
    print('\nconfusion:')
    for field in fields:

        # print vector
        vector = [breakdown[field][fieldii] for fieldii in fields]
        vector = [('  ' + str(entry))[-3:] for entry in vector]
        vector = str(vector).replace("'", '').replace(',', '')
        print(vector)

    # begin coefficients record with intercepts
    coefficients = {}
    intercepts = {category: intercept for category, intercept in zip(categories, support.intercept_)}
    coefficients['intercepts'] = intercepts

    # make labels file
    for index, label in enumerate(labels):

        # add coefficients entry
        model = {category: slopes[index] for category, slopes in zip(categories, support.coef_)}
        coefficients[label] = model

    # dump json file
    path = '../support.json'
    with open(path, 'w') as pointer:

        # dump jason
        json.dump(coefficients, pointer)

    return support


# test a sample
def test(identifier, support, matrix, targets):
    """Test a training example to see the predictions.

    Arguments:
        identifier: int, index of sample number
        support: sklearn svc model
        matrix: list of list of floats
        targets: list of floats
    """

    # get truth
    truth = targets[identifier]
    print('truth for sample {}: {}'.format(identifier, truth))

    # calculate values for all categories
    for index, vector in enumerate(support.coef_):

        # calculate category score
        score = numpy.array(matrix[identifier]).dot(numpy.array(vector)) + support.intercept_[index]
        print('category {}: {}'.format(index, score))

    return None


# merge files
def merge(resolution):
    """Merge moderation results from insert copies with insert labels.

    Arguments:
        resolution: int, the resolution

    Returns:
        None
    """

    # open moderations
    path = 'demo/inserts_' + str(resolution) + '_copy.json'
    with open(path, 'r') as pointer:

        # get moderations
        moderations = json.load(pointer)

    # open labels
    path = 'demo/inserts_' + str(resolution) + '.json'
    with open(path, 'r') as pointer:

        # get moderations
        labels = json.load(pointer)

    # go through each entry
    for name in labels.keys():

        # set moderation labels and flags
        labels[name]['detections']['ModerationLabels'] = moderations[name]['detections']['ModerationLabels']
        labels[name]['redetections']['ModerationLabels'] = moderations[name]['redetections']['ModerationLabels']
        labels[name]['flags'] = moderations[name]['flags']

    # resave file
    with open(path, 'w') as pointer:

        # dump
        json.dump(labels, pointer)

    return None


# test moderation
def moderate():
    """Determine the lowest detected moderation for various inserts.

    Arguments:
        *resolutions: unpacked list of ints

    Returns:
        None
    """

    # get propriety model
    path = 'propriety.json'
    with open(path,'r') as pointer:

        # read in from json
        propriety = json.load(pointer)

    # get all background detections
    path = 'demo/backgrounds/mosquitoes.json'
    with open(path, 'r') as pointer:

        # get mosquitoes detections
        mosquitoes = json.load(pointer)

    # get background
    key = 'demo/backgrounds/mosquitoes.jpg'
    background = [entry['Name'] for entry in mosquitoes[key]['detections']['Labels']]

    # print
    print('background: ')
    print(background)

    # get all labels
    path = 'demo/moderationscans.json'
    with open(path, 'r') as pointer:

        # get scan data
        scans = json.load(pointer)

    # get labels
    labels = {}
    for key, value in scans.items():

        # get name
        name = key.split('/')[-1].replace('.jpg', '')
        labels[name] = {'name': '', 'confidence': 0}
        if 'moderations' in value['flags']:

            # add label
            labels[name] = {'name': value['detections']['ModerationLabels'][0]['Name']}
            labels[name]['confidence'] = value['detections']['ModerationLabels'][0]['Confidence']

    # get all datasets
    datasets = []
    for resolution in resolutions:

        # get data set
        path = 'demo/inserts_' + str(resolution) + '.json'
        with open(path, 'r') as pointer:

            # get data
            dataset = json.load(pointer)
            datasets.append(dataset)

    # status
    print('datasets: {}'.format(len(datasets)))

    # collect all data points
    scores = {}
    for resolution, dataset in zip(resolutions, datasets):

        # get all keys
        keys = [key for key in dataset.keys()]

        # get all things
        things = [entry['Name'] for key in keys for entry in dataset[key]['detections']['Labels']]
        things += [entry['Name'] for key in keys for entry in dataset[key]['redetections']['Labels']]
        things = list(set(things))
        things = [thing for thing in things if thing not in background]
        print('things: ')
        print(things)

        # go through keys
        for key in keys:

            # make name
            name = key.split('/')[-1].split('_')[0].replace('.jpg', '')
            number = int(key.split('_')[-1].replace('.jpg', ''))
            if name not in scores.keys():

                # make default
                scores[name] = {}

            # make resolution
            if resolution not in scores[name].keys():

                # add
                scores[name][resolution] = {'detects': [1000], 'nondetects': [0], 'human': [1000], 'propriety': [1000]}

            # get status
            status = 'nondetects'
            if 'moderations' in dataset[key]['flags']:

                # set status
                status = 'detects'

            # add to scores
            scores[name][resolution][status].append(number)

            # check for labels
            words = ['Person', 'Human']
            scene = [entry['Name'] for entry in dataset[key]['detections']['Labels']]
            scene += [entry['Name'] for entry in dataset[key]['redetections']['Labels']]
            if any([word in scene for word in words]):

                # set to true
                scores[name][resolution]['human'].append(number)

            # apply propriety model
            detections = dataset[key]['detections']['Labels'] + dataset[key]['redetections']['Labels']
            classifications = propriety['intercepts'].copy()
            for member in detections:

                # check for key
                if member['Name'] in propriety.keys():

                    # add all values to classifications
                    for category in classifications.keys():

                        # add to classification
                        classifications[category] += propriety[member['Name']][category] * member['Confidence'] / 100

            # sort classifications
            classifications = [item for item in classifications.items()]
            classifications.sort(key=lambda classification: classification[1], reverse=True)
            if 'inappropriate' in classifications[0][0]:

                # add detections
                scores[name][resolution]['propriety'].append(number)

    # status
    print('scores: {}'.format(len(scores)))

    # get maximums at each resolution
    vectors = {}
    humans = {}
    proprieties = {}
    for name, value in scores.items():

        # get min detect at each resolution
        vector = [min(value[resolution]['detects']) for resolution in resolutions]
        vectors[name] = vector

        # find minimum
        human = [min(value[resolution]['human']) for resolution in resolutions]
        humans[name] = human

        # find minimum
        appropriateness = [min(value[resolution]['propriety']) for resolution in resolutions]
        proprieties[name] = appropriateness

    # status
    print('vectors: {}'.format(len(vectors)))

    # sort vectors
    sorting = vectors
    vectors = []
    for name, vector in sorting.items():

        # default label and confidence
        label = ''
        confidence = 0

        # try to get label
        if name in labels.keys():

            # get label and confidence
            label = labels[name]['name']
            confidence = int(labels[name]['confidence'])

        # create vector
        human = humans[name]
        appropriateness = proprieties[name]
        entry = (human, appropriateness, vector, label, confidence, name)
        vectors.append(entry)

    # sort
    vectors.sort(key=lambda entry: entry[0][-1], reverse=True)

    # print
    for vector in vectors:

        # print
        print(vector)

    return None


# plot the quality readings
def qualify():
    """Plot the quality readings from test images.

    Arguments:
        None

    Returns:
        None
    """

    # get the file
    with open('quality/qualities.json', 'r') as pointer:

        # load up
        qualities = json.load(pointer)

    # set colors
    colors = ['go-', 'bo-', 'ro-', 'ko-', 'mo-', 'co-'] * 5

    # get the points
    images = [key for key in qualities.keys()][:2]
    for index, image in enumerate(images):

        # get x and ys
        xs = [int(number) for number in qualities[image].keys() if number.isdigit()]
        ys = [qualities[image][str(x)] for x in xs]

        # plot points
        pyplot.plot(xs, ys, colors[index])

        # determine hyperbole
        center = (qualities[image]['100'] * 100 - qualities[image]['50'] * 50) / (qualities[image]['100'] - qualities[image]['50'])
        multiplier = 1 / (qualities[image]['100'] * (100 - center))

        # get points
        ys = [1 / (multiplier * (x - center)) for x in xs]
        pyplot.plot(xs, ys, colors[index].replace('o', 'x'))

    # show plot
    pyplot.ylabel('file size')
    pyplot.xlabel('quality')
    pyplot.title('jpg file size vs. quality (o) + hyperbolic approximation (x)\n')
    pyplot.savefig('quality/quality.png')
    pyplot.close()

    return None


# plot the quantity readings
def quantify():
    """Plot the quantity readings from test images.

    Arguments:
        None

    Returns:
        None
    """

    # get the file
    with open('quality/quantities.json', 'r') as pointer:

        # load up
        quantities = json.load(pointer)

    # set colors
    colors = ['go-', 'bo-', 'ro-', 'ko-', 'mo-', 'co-'] * 5

    # get the points
    images = [key for key in quantities.keys()][1:3]
    for index, image in enumerate(images):

        # get x and ys
        xs = [float(number) for number in quantities[image].keys() if number.isdigit() or '.' in number]
        xs.sort()
        ys = [quantities[image][str(x)] for x in xs]

        # plot points
        pyplot.plot(xs, ys, colors[index])

        # determine line
        slope = quantities[image]["0.99"] / 0.99

        # get points
        ys = [slope * x for x in xs]
        pyplot.plot(xs, ys, colors[index].replace('o', 'x'))

    # show plot
    pyplot.ylabel('file size')
    pyplot.xlabel('scaling factor')
    pyplot.title('png/bmp file size vs. scaling factor (o) + linear approximation (x)\n')
    pyplot.savefig('quality/quantity.png')
    pyplot.close()

    return None


# Fake color records as a test system for machines
def fake(number=25, overlap=0):
    """Fake a number of label records for testing support vector machines.

    Arguments:
        number=25: int, number of records of each color
        overlap=20: percent overlap

    Returns:
        None
    """

    # make groups
    groups = {'cool': ['blue', 'green'], 'warm': ['magenta', 'red']}

    # calculate ranges
    low = [0, 50 + overlap]
    high = [50 - overlap, 100]
    ranges = {'Redish': {'red': high, 'blue': low, 'green': low, 'magenta': high}}
    ranges.update({'Blueish': {'red': low, 'blue': high, 'green': low, 'magenta': high}})

    # go through each group
    for group, categories in groups.items():

        # and each category
        for category in categories:

            # begin data
            data = {}

            # create a number of records
            for trial in range(number):

                # get random number based scores
                redish = ranges['Redish'][category][0] + (ranges['Redish'][category][1] - ranges['Redish'][category][0]) * random()
                blueish = ranges['Blueish'][category][0] + (ranges['Blueish'][category][1] - ranges['Blueish'][category][0]) * random()

                # create datum
                name = 'faux/{}/{}/{}_{}.jpg'.format(group, category, category, trial)
                datum = {'detections': {'Labels': []}, 'redetections': {'Labels': []}}
                datum['detections']['Labels'].append({'Name': 'Redish', 'Confidence': redish})
                datum['redetections']['Labels'].append({'Name': 'Blueish', 'Confidence': blueish})

                # add to data
                data[name] = datum

            # save file
            deposit = 'faux/{}/{}.json'.format(group, category)
            with open(deposit, 'w') as pointer:

                # save
                json.dump(data, pointer)

    return None


# add redetections field to scan file if absent
def redetect(path):
    """Add redetection fields to data.

    Arguments:
        path: str, the file path

    Returns:
        None
    """

    # open file
    with open(path, 'r') as pointer:

        # get data
        data = json.load(pointer)

    # go through each entry
    for name, info in data.items():

        # check for redetections field
        if 'redetections' not in info.keys():

            # add field
            data[name]['redetections'] = {'Labels': []}

    # resave file
    with open(path, 'w') as pointer:

        # save
        json.dump(data, pointer)

    return None


# siphon moeration image detections into their own file
def siphon():
    """Siphon moderation scan data into their own folders.

    Arguments:
        None

    Returns:
        None
    """

    # get moderation scans
    with open('demo/moderationscans.json', 'r') as pointer:

        # get the data
        scans = json.load(pointer)

    # get all folders
    stem = 'propriety/inappropriate'
    folders = os.listdir(stem)

    # collect data from each folder
    for folder in folders:

        # begin data
        data = {}

        # get all names within
        names = os.listdir(stem + '/' + folder)
        for name in names:

            # add to data
            path = stem + '/' + folder + '/' + name
            previous = 'demo/moderations/' + name
            data[path] = scans[previous]

        # deposit the data
        deposit = stem + '/' + folder + '.json'
        with open(deposit, 'w') as pointer:

            # dump data
            json.dump(data, pointer)

    return None


# snip out deleted photos from data files
def snip():
    """Snip out deleted images from the moderation scan files.

    Arguments:
        None

    Returns:
        None
    """

    # go through each folder
    names = os.listdir('propriety/appropriate')
    names = [name for name in names if '.' not in name]

    # go through each name
    for name in names:

        # get images
        folder = 'propriety/appropriate/' + name
        images = os.listdir(folder)

        # get scans
        with open(folder + '.json', 'r') as pointer:

            # get scans
            scans = json.load(pointer)

        # only retain those associated with images
        data = {path: info for path, info in scans.items() if any([image in path for image in images])}

        # dump back into folder
        with open(folder + '.json', 'w') as pointer:

            # dump data
            json.dump(data, pointer)

    return None


# decipher filenames into a series
def decipher(directory):
    """Rename the members of a png directory with a sytematic name.

    Arguments:
        directory: str

    Returns:
        None
    """

    # get list of files
    names = os.listdir(directory)

    # rename files
    for index, name in enumerate(names):

        # get tage
        tag = directory.split('/')[-1].split('_')[0]

        # get path and deposit
        path = directory + '/' + name
        deposit = directory + '/' + tag + '_' + str(index) + '.' + name.split('.')[-1]

        # rename
        os.rename(path, deposit)

    return None


# download images from rejections spreadsheet
def investigate(protocol, number):
    """Investigate rejected photos.

    Arguments:
        protocol: str
        number: int

    Returns:
        None
    """

    # download the csv
    with open('evaluation/rejections.csv', 'r') as pointer:

        # get csv rows
        rows = [row for row in csv.reader(pointer)]

    # narrow down by date
    samples = [row for row in rows[1:]]

    # go through samples
    count = 0
    for index, sample in enumerate(samples):

        # only check past 100
        if index > 100:

            # check for protocol
            if protocol in sample[2]:

                # advance count
                count += 1

                # get urls
                url = sample[1]

                # make image
                image = Image.open(urlopen(url))

                # save
                deposit = 'evaluation/rejections/' + sample[2].replace(' ', '_') + '_' + str(index) + '.jpg'
                print('writing image ' + deposit + '...')
                image.save(deposit)

        # break if count is high enough
        if count >= number:

            # break
            break

    return None


# trim non unique labels
def trim(path):
    """Trim labels down to the most confident member.

    Arguments:
        path: file path

    Returns:
        None
    """

    # open up data
    with open(path, 'r') as pointer:

        # get data
        data = json.load(pointer)

    # get all entries
    for identifier, info in data.items():

        # get all detected and redeteceted labels
        detections = info['detections']['Labels']
        redetections = info['redetections']['Labels']

        # sort redetections by confidence
        redetections.sort(key=lambda label: label['Confidence'], reverse=True)

        # get all labels in detections
        names = [label['Name'] for label in detections]
        trimmings = []
        for label in redetections:

            # check for name
            if label['Name'] not in names:

                # add to trimmings and names
                trimmings.append(label)
                names.append(label['Name'])

        # add back to record
        data[identifier]['redetections']['Labels'] = trimmings

    # rewrite file
    with open(path, 'w') as pointer:

        # dump file
        json.dump(data, pointer)

    return None


# tally up counts of evaluation study
def summarize():
    """Summarize counts of evaluation study.

    Arguments:
        None

    Returns:
        None
    """

    # open approvals data
    with open('evaluation/approvals.json') as pointer:

        # get data
        approvals = json.load(pointer)

    # open rejections data
    with open('evaluation/rejections.json') as pointer:

        # get data
        rejections = json.load(pointer)

    # open note
    with open('evaluation/notes.json') as pointer:

        # get notes
        notes = json.load(pointer)

    # make summaries
    summaries = []
    batches = ['approvals', 'rejections']
    reservoirs = [approvals, rejections]
    for batch, reservoir in zip(batches, reservoirs):

        # go through records
        for name, record in reservoir.items():

            # make summary
            comparison = name.replace('.jpg', '_comparison.jpg').replace(batch, batch + '_comparisons')
            summary = {'batch': batch, 'name': name, 'comparison': comparison}

            # add statuses
            for status in ('approve', 'original'):

                # add status
                summary.update({status: record[status]})

            # add flags
            summary.update({'flags': record['flags']})

            # summarize labels
            labels = {'counts': [], 'labels': []}
            labels['counts'].append(len(record['detections']['Labels']))
            labels['counts'].append(len(record['redetections']['Labels']))

            # get all detected labels and sort by confidence
            detections = [[label['Name'], round(label['Confidence'], 2)] for label in record['detections']['Labels']]
            detections.sort(key=lambda label: label[1], reverse=True)
            labels['labels'].append(detections)

            # get all redetected labels and sort by confidence
            redetections = [[label['Name'], round(label['Confidence'], 2)] for label in record['redetections']['Labels']]
            redetections.sort(key=lambda label: label[1], reverse=True)
            labels['labels'].append(redetections)

            # add to summary
            summary['labels'] = labels

            # summarize texts
            texts = {'counts': [], 'texts': [[], []]}

            # organize detections
            detections = [detection for detection in record['detections']['TextDetections'] if detection['Type'] == 'LINE']
            texts['counts'].append(len(detections))
            for detection in detections:

                # turn polygon into bounding box
                polygon = detection['Geometry']['Polygon']
                left = min([point['X'] for point in polygon])
                width = max([point['X'] for point in polygon]) - left
                top = min([point['Y'] for point in polygon])
                height = max([point['Y'] for point in polygon]) - top

                # get text
                text = detection['DetectedText']
                corner = [round(left, 3), round(top, 3)]
                size = [round(width, 3), round(height, 3)]
                confidence = round(detection['Confidence'], 2)
                box = [text, confidence, corner, size]

                # add to texts
                texts['texts'][0].append(box)

            # organize redetections
            detections = [detection for detection in record['redetections']['TextDetections'] if detection['Type'] == 'LINE']
            texts['counts'].append(len(detections))
            for detection in detections:

                # turn polygon into bounding box
                polygon = detection['Geometry']['Polygon']
                left = min([point['X'] for point in polygon])
                width = max([point['X'] for point in polygon]) - left
                top = min([point['Y'] for point in polygon])
                height = max([point['Y'] for point in polygon]) - top

                # get text
                text = detection['DetectedText']
                corner = [round(left, 3), round(top, 3)]
                size = [round(width, 3), round(height, 3)]
                confidence = round(detection['Confidence'], 2)
                box = [text, confidence, corner, size]

                # add to texts
                texts['texts'][1].append(box)

            # add to summary
            summary['texts'] = texts

            # summarize faces
            faces = {'counts': [], 'faces': [[], []]}

            # organize detections
            detections = [detection for detection in record['detections']['FaceDetails']]
            faces['counts'].append(len(detections))
            for detection in detections:

                # get face
                emotion = detection['Emotions'][0]['Type']
                bounds = detection['BoundingBox']
                corner = [round(bounds['Left'], 3), round(bounds['Top'], 3)]
                size = [round(bounds['Width'], 3), round(bounds['Height'], 3)]
                confidence = round(detection['Confidence'], 2)
                brightness = round(detection['Quality']['Brightness'], 2)
                sharpness = round(detection['Quality']['Sharpness'], 2)
                box = [emotion, confidence, corner, size, brightness, sharpness]

                # add to faces
                faces['faces'][0].append(box)

            # organize redetections
            detections = [detection for detection in record['redetections']['FaceDetails']]
            faces['counts'].append(len(detections))
            for detection in detections:

                # get face
                emotion = detection['Emotions'][0]['Type']
                bounds = detection['BoundingBox']
                corner = [round(bounds['Left'], 3), round(bounds['Top'], 3)]
                size = [round(bounds['Width'], 3), round(bounds['Height'], 3)]
                confidence = round(detection['Confidence'], 2)
                brightness = round(detection['Quality']['Brightness'], 2)
                sharpness = round(detection['Quality']['Sharpness'], 2)
                box = [emotion, confidence, corner, size, brightness, sharpness]

                # add to faces
                faces['faces'][1].append(box)

            # add to summary
            summary['faces'] = faces

            # summarize moderations
            moderations = {'counts': [], 'moderations': []}
            moderations['counts'].append(len(record['detections']['ModerationLabels']))
            moderations['counts'].append(len(record['redetections']['ModerationLabels']))

            # get all detected moderations and sort by confidence
            detections = [[moderation['Name'], round(moderation['Confidence'], 2)] for moderation in record['detections']['ModerationLabels']]
            detections.sort(key=lambda moderation: moderation[1], reverse=True)
            moderations['moderations'].append(detections)

            # get all redetected moderations and sort by confidence
            redetections = [[moderation['Name'], round(moderation['Confidence'], 2)] for moderation in record['redetections']['ModerationLabels']]
            redetections.sort(key=lambda moderation: moderation[1], reverse=True)
            moderations['moderations'].append(redetections)

            # add to summary
            summary['moderations'] = moderations

            # # summarize celebrities
            # celebrities = {'counts': [], 'celebrities': []}
            # celebrities['counts'].append(len(record['detections']['CelebrityFaces']))
            #
            # # get all detected celebrities and sort by confidence
            # detections = [[celebrity['Name']] for celebrity in record['detections']['CelebrityFaces']]
            # detections.sort(key=lambda celebrity: celebrity[0], reverse=True)
            # celebrities['celebrities'].append(detections)
            #
            # # add to summary
            # summary['celebrities'] = celebrities

            # add notes to summary
            summary['notes'] = []
            if comparison in notes.keys():

                # add notes
                summary['notes'] += notes[comparison]

            # add to summaries
            summaries.append(summary)

            # add person summary
            summary['persons'] = {}
            summary['persons']['persons'] = [[], []]
            detections = [label for label in record['detections']['Labels'] if label['Name'] == 'Person']
            for label in detections:

                # go through each instance
                for instance in label['Instances']:

                    # construct element
                    name = label['Name']
                    confidence = round(instance['Confidence'], 2)
                    left = round(instance['BoundingBox']['Left'], 3)
                    top = round(instance['BoundingBox']['Top'], 3)
                    width = round(instance['BoundingBox']['Width'], 3)
                    height = round(instance['BoundingBox']['Height'], 3)

                    # add element
                    summary['persons']['persons'][0].append([name, confidence, [left, top], [width, height]])

            # get all redetected labels and sort by confidence
            redetections = [label for label in record['redetections']['Labels'] if label['Name'] == 'Person']
            for label in redetections:

                # go through each instance
                for instance in label['Instances']:

                    # construct element
                    name = label['Name']
                    confidence = round(instance['Confidence'], 2)
                    left = round(instance['BoundingBox']['Left'], 3)
                    top = round(instance['BoundingBox']['Top'], 3)
                    width = round(instance['BoundingBox']['Width'], 3)
                    height = round(instance['BoundingBox']['Height'], 3)

                    # add element
                    summary['persons']['persons'][1].append([name, confidence, [left, top], [width, height]])

    # save summaries
    with open('evaluation/summaries.json', 'w') as pointer:

        # dump in summaries
        json.dump(summaries, pointer)

    return None


# tally up false positives, false negatives
def tally(*mutes, protocol=None):
    """Tally up stats on false positives and negatives, etc.

    Arguments:
        *mutes=unpacked tuple of strings
        protocol=None: filter by protocol

    Returns:
        None
    """

    # tally
    tallies = {}

    # get summaries
    with open('evaluation/summaries.json', 'r') as pointer:

        # get summaries
        summaries = json.load(pointer)

    # filter by protocol
    if protocol:

        # filter out
        summaries = [record for record in summaries if protocol in record['name']]

    # set up tests
    tests = {}
    tests['relevant'] = lambda record: 'irrelevant' not in record['flags']
    tests['pristine'] = lambda record: 'persons' not in record['flags']
    tests['faces'] = lambda record: 'faces' not in record['flags']
    #tests['texts'] = lambda record: 'texts' not in record['flags']
    tests['moderations'] = lambda record: 'moderations' not in record['flags']
    tests['cars'] = lambda record: 'cars' not in record['flags']

    # delete tests in mutes
    for mute in mutes:

        # delete test
        del(tests[mute])

    # get list
    tests = [value for value in tests.values()]

    # get true positives
    approvals = [summary for summary in summaries if summary['batch'] == 'approvals' and all([test(summary) for test in tests])]
    tallies['true_positives'] = {'count': len(approvals)}

    # get false positives
    mistakes = [summary for summary in summaries if summary['batch'] == 'rejections' and all([test(summary) for test in tests])]
    tallies['false_positives'] = {'count': len(mistakes)}

    # get true negatives
    rejections = [summary for summary in summaries if summary['batch'] == 'rejections' and not all([test(summary) for test in tests])]
    tallies['true_negatives'] = {'count': len(rejections)}

    # get false negatives
    wastes = [summary for summary in summaries if summary['batch'] == 'approvals' and not all([test(summary) for test in tests])]
    tallies['false_negatives'] = {'count': len(wastes)}

    # add other info
    batches = [approvals, mistakes, rejections, wastes]
    labels = ['true_positives', 'false_positives', 'true_negatives', 'false_negatives']
    for batch, label in zip(batches, labels):

        # add up texts
        detections = sum([record['texts']['counts'][0] for record in batch])
        redetections = sum([record['texts']['counts'][1] for record in batch])
        tallies[label]['texts'] = [detections, redetections]

        # add up faces
        detections = sum([record['faces']['counts'][0] for record in batch])
        redetections = sum([record['faces']['counts'][1] for record in batch])
        tallies[label]['faces'] = [detections, redetections]

        # add up moderations
        detections = sum([record['moderations']['counts'][0] for record in batch])
        redetections = sum([record['moderations']['counts'][1] for record in batch])
        tallies[label]['moderations'] = [detections, redetections]

        # # add up celebrities
        # detections = sum([record['celebrities']['counts'][0] for record in batch])
        # tallies[label]['celebrities'] = [detections]

        # add up relevances
        relevant = [record for record in batch if 'irrelevant' not in record['flags']]
        tallies[label]['relevant'] = len(relevant)

        # # add up proprieties
        # appropriate = [record for record in batch if record['appropriate']]
        # tallies[label]['appropriate'] = len(appropriate)

        # # add up pristines
        # pristine = [record for record in batch if record['pristine']]
        # tallies[label]['pristine'] = len(pristine)

        # add up originals
        original = [record for record in batch if record['original']]
        tallies[label]['original'] = len(original)

    # print tallies
    for item in tallies.items():

        # print
        print(' ')
        print('{}: {}'.format(item[0], item[1]))

    # print confustion matrix
    print(' ')
    print('confusion matrix: ')
    print('              a', ' r')
    print(' approvals: ', tallies['true_positives']['count'], tallies['false_negatives']['count'])
    print('rejections: ', tallies['false_positives']['count'], tallies['true_negatives']['count'])

    return None


# determine highest model contributions
def contribute(record, machine):
    """Calculate the contribution of each label to each score.

    Arguments:
        record: dict
        machine: dict

    Returns:
        dict
    """

    # get the coefficients
    intercepts = machine['intercepts']
    categories = [key for key in machine['intercepts'].keys()]

    # get contributions to each category from each label
    contributions = {category: {} for category in categories}
    for category in categories:

        # go through labels
        for chunk in record['labels']['labels']:

            # go through each chunk
            for label in chunk:

                # unpack
                name = label[0]
                confidence = label[1]

                # check for presence
                if name in machine.keys():

                    # calculate contribution
                    contribution = machine[name][category] * confidence / 100
                    contributions[category][name] = contribution

        # get total score
        contributions[category]['score'] = intercepts[category] + sum(contributions[category].values())

    return contributions


# prepare a sample from a record
def prepare(record, model, index):
    """Prepare a sample from a record.

    Arguments:
        record: dict
        model: str
        index: int

    Returns:
        dict
    """

    # set sample to record
    sample = record

    # make texts sample
    if model == 'texts':

        # begin sample
        sample = {'labels': {'labels': [[]]}}

        # check index for first tier
        if index < len(record['texts']['texts'][0]):

            # get attributes
            reservoir = record['texts']['texts'][0]
            level = 100
            position = index

        # otherwise second tier
        else:

            # get attributes
            reservoir = record['texts']['texts'][1]
            level = 0
            position = index - len(record['texts']['texts'][0])

        # construct sample
        text = reservoir[position]
        complexity = (len(set(text[0])) - 1) / len(text[0])
        sample['labels']['labels'][0].append(['Tier', level])
        sample['labels']['labels'][0].append(['Confidence', text[1]])
        sample['labels']['labels'][0].append(['Width', text[3][0] * 100])
        sample['labels']['labels'][0].append(['Height', text[3][1] * 100])
        sample['labels']['labels'][0].append(['Length', len(text[0])])
        sample['labels']['labels'][0].append(['Complexity', complexity * 100])

    # for faces
    if model == 'faces':

        # begin sample
        sample = {'labels': {'labels': [[]]}}

        # check index for first tier
        if index < len(record['faces']['faces'][0]):

            # get attributes
            reservoir = record['faces']['faces'][0]
            level = 100
            position = index

        # otherwise second tier
        else:

            # get attributes
            reservoir = record['faces']['faces'][1]
            level = 0
            position = index - len(record['faces']['faces'][0])

        # construct sample
        face = reservoir[position]
        sample['labels']['labels'][0].append(['Tier', level])
        sample['labels']['labels'][0].append(['Confidence', face[1]])
        sample['labels']['labels'][0].append(['Width', face[3][0] * 100])
        sample['labels']['labels'][0].append(['Height', face[3][1] * 100])
        sample['labels']['labels'][0].append(['Brightness', face[4]])
        sample['labels']['labels'][0].append(['Sharpness', face[5]])

    # print sample
    pprint(sample)

    return sample


# get machine model results
def mechanize(record, model, index, number=5):
    """Get the machine model results.
    
    Arguments:
        record: dict
        model: string
        index: int
        number=5: int, number of attributes to show
        
    Returns:
         None
    """

    # get machine results
    with open(model + '.json', 'r') as pointer:
        
        # get model
        machine = json.load(pointer)

    # prepare sample from record
    sample = prepare(record, model, index)

    # get contributions
    contributions = contribute(sample, machine)
    categories = [key for key in contributions.keys()]

    # get class scores
    scores = [(category, round(contributions[category]['score'], 2)) for category in categories]
    scores.sort(key=lambda score: score[1], reverse=True)

    # get prediction
    prediction = scores[0][0]

    # find category
    designation = prediction
    markers = {'lands': 'land', 'mosquitoes': 'mosquito', 'trees': 'tree', 'clouds': 'cloud'}
    for marker, signifier in markers.items():

        # find designation
        if signifier in record['name']:

            # find category with marker in it
            possibilities = [category for category in categories if marker in category]
            if len(possibilities) > 0:

                # set first as designation
                designation = possibilities[0]

    # print heading
    print('\n{} -!-> {}\n'.format(designation, prediction))

    # print scores
    [print(score) for score in scores]

    # get top factors for wrong answer
    print('\ntop factors for {}:'.format(prediction))
    factors = [(item[0], round(item[1], 2)) for item in contributions[prediction].items()]
    factors = [item for item in factors if item[0] != 'score']
    factors.sort(key=lambda pair: pair[1], reverse=True)
    [print('     {}: {}'.format(*factor)) for factor in factors[:number]]

    # get top factors against correct answer
    print('\ntop factors against {}:'.format(designation))
    factors = [(item[0], round(item[1], 2)) for item in contributions[designation].items()]
    factors = [item for item in factors if item[0] != 'score']
    factors.sort(key=lambda pair: pair[1])
    [print('     {}: {}'.format(*factor)) for factor in factors[:number]]

    return None


# put file into new folder
def fling(record, folder):
    """Fling the image from the record into a new folder for analysis later.

    Arguments:
        record: dict
        folder: str, the path name

    Returns:
        None
    """

    # get record name
    path = record['name']

    # get stub
    name = path.split('/')[-1]

    # get name
    image = Image.open(record['name'])
    image.save(folder + '/' + name)

    return None


# inspect summaries
def inspect(subset, protocol='', skip=0):
    """Inspect a subset of evaluations.

    Arguments:
        subset: str or function
        skip=0: number to skip to

    Returns:
        None
    """

    # define functions
    functions = {}
    functions['true_positives'] = lambda record: record['batch'] == 'approvals' and record['approve']
    functions['false_positives'] = lambda record: record['batch'] == 'rejections' and record['approve']
    functions['true_negatives'] = lambda record: record['batch'] == 'rejections' and not record['approve']
    functions['false_negatives'] = lambda record: record['batch'] == 'approvals' and not record['approve']

    # define more functions
    functions['approved_moderations'] = lambda record: record['batch'] == 'approvals' and sum(record['moderations']['counts']) > 0
    functions['rejected_moderations'] = lambda record: record['batch'] == 'rejections' and sum(record['moderations']['counts']) > 0
    functions['approved_texts'] = lambda record: record['batch'] == 'approvals' and sum(record['texts']['counts']) > 0
    functions['rejected_texts'] = lambda record: record['batch'] == 'rejections' and sum(record['texts']['counts']) > 0
    functions['approved_faces'] = lambda record: record['batch'] == 'approvals' and sum(record['faces']['counts']) > 0
    functions['rejected_faces'] = lambda record: record['batch'] == 'rejections' and sum(record['faces']['counts']) > 0
    functions['approved_celebrities'] = lambda record: record['batch'] == 'approvals' and sum(record['celebrities']['counts']) > 0
    functions['rejected_celebrities'] = lambda record: record['batch'] == 'rejections' and sum(record['celebrities']['counts']) > 0

    # define more functions
    functions['small_faces'] = lambda record: record['faces']['counts'][1] > 0 and record['faces']['counts'][0] < 1 and 'faces' in record['flags']
    functions['small_texts'] = lambda record: record['texts']['counts'][1] > 0 and record['texts']['counts'][0] < 1 and 'texts' in record['flags']
    functions['approved_texts'] = lambda record: record['batch'] == 'approvals' and 'texts' in record['flags']
    functions['rejected_texts'] = lambda record: record['batch'] == 'rejections' and 'texts' in record['flags']
    functions['faces'] = lambda record: sum(record['faces']['counts']) > 0
    functions['approved_moderations'] = lambda record: record['batch'] == 'approvals' and sum(record['moderations']['counts']) > 0 and record['approve']
    functions['celebrities'] = lambda record: record['celebrities']['counts'][0] > 0

    # check for string
    function = subset
    if str(subset) == subset:

        # try to get function
        try:

            # get function
            function = functions[subset]

        # otherwise
        except KeyError:

            # assume notes
            function = lambda record: subset in record['notes']

    # open summaries
    with open('evaluation/summaries.json', 'r') as pointer:

        # get summaries
        summaries = json.load(pointer)

    # open notes
    with open('evaluation/notes.json', 'r') as pointer:

        # get notes
        notes = json.load(pointer)

    # apply protocol filter
    records = [record for record in summaries if protocol in record['name']]

    # get subset
    records = [record for record in records if function(record)]

    # go through records
    leave = False
    for index, record in enumerate(records[skip:]):

        # get name
        image = Image.open(record['comparison'])
        image.save('evaluation/diagnosis.jpg')

        # summarize
        print(' ')
        print('{} #{} of {}: '.format(subset, index + skip, len(records)))
        print('name: {}'.format(record['name']))
        print(' ')
        print('approve: {}'.format(record['approve']))
        print(' ')
        print('flags: {}'.format(record['flags']))
        print(' ')
        print('original: {}'.format(record['original']))
        print('texts: {}, {}'.format(record['texts']['counts'][0], record['texts']['counts'][1]))
        print('faces: {}, {}'.format(record['faces']['counts'][0], record['faces']['counts'][1]))
        print('persons: {}, {}'.format(len(record['persons']['persons'][0]), len(record['persons']['persons'][1])))
        print('moderations: {}, {}'.format(record['moderations']['counts'][0], record['moderations']['counts'][1]))
        print(' ')
        print('notes: {}'.format(record['notes']))
        print(' ')

        # pause
        pause = input('??')

        # go through choices
        while pause not in ('', ' '):

            # add notes
            if 'note' in pause:

                # get note
                note = pause.split()[1]

                # check for entry
                name = record['comparison']
                if name not in notes.keys():

                    # add empty list
                    notes[name] = []

                # add note
                notes[name].append(note)

                # write file
                with open('evaluation/notes.json', 'w') as pointer:

                    # write file
                    json.dump(notes, pointer)

            # add notes
            if 'denote' in pause:

                # get note
                note = pause.split()[1]

                # check for entry
                name = record['comparison']
                if name not in notes.keys():

                    # add empty list
                    notes[name] = []

                # add note
                notes[name] = [entry for entry in notes[name] if entry != note]

                # write file
                with open('evaluation/notes.json', 'w') as pointer:

                    # write file
                    json.dump(notes, pointer)

            # view record
            if pause == 'view':

                # print
                pprint(record)

            # view model results
            if 'model' in pause:

                # get model
                model = pause.split()[1]
                try:

                    # get the indes
                    index = int(pause.split()[2])

                # assume zero
                except IndexError:

                    # set index to zero
                    index = 0

                # print vector machine results
                mechanize(record, model, index)

            # view model results
            if 'fling' in pause:

                # get model
                folder = pause.split()[1]

                # print vector machine results
                fling(record, folder)

            # clear boxes
            if 'clear' in pause:

                # get name
                image = Image.open(record['comparison'])
                image.save('evaluation/diagnosis.jpg')

            if 'draw' in pause:

                # parse
                draw, feature, index = pause.split()

                # make list
                aggregate = record[feature][feature][0] + record[feature][feature][1]

                # get coordinates
                left, top = aggregate[int(index)][2]
                width, height = aggregate[int(index)][3]

                # get image
                image = Image.open(record['comparison'])

                # change to get coordinates
                array = numpy.array(image)
                shape = array.shape

                # change to absolute pixels
                horizontal = int(shape[1] / 2)
                vertical = int(shape[0])
                left = int(left * horizontal)
                width = int(width * horizontal)
                top = int(top * vertical)
                height = int(height * vertical)

                # get coordinates for horizontal lines
                points = []
                for coordinate in range(left, left + width):

                    # thick lines
                    for thickness in range(3):

                        # add to points
                        points.append((coordinate, top + thickness))
                        points.append((coordinate + horizontal, top + thickness))
                        points.append((coordinate, top + height - thickness))
                        points.append((coordinate + horizontal, top + height - thickness))

                # get coordinates for vertical lines
                for coordinate in range(top, top + height):

                    # thick lines
                    for thickness in range(3):

                        # add to points
                        points.append((left + thickness, coordinate))
                        points.append((left + horizontal + thickness, coordinate))
                        points.append((left + width - thickness, coordinate))
                        points.append((left + horizontal + width - thickness, coordinate))

                # convert all points to black
                points = list(set(points))
                for column, row in points:

                    # try to draw box
                    try:

                        # convert to orange
                        array[row][column][0] = 0
                        array[row][column][1] = 255
                        array[row][column][2] = 255

                    # unless index error
                    except IndexError:

                        # skip
                        pass

                # save image
                image = Image.fromarray(array)
                image.save('evaluation/diagnosis.jpg')

            if 'quadrant' in pause:

                # get image
                image = Image.open(record['comparison'])

                # change to get coordinates
                array = numpy.array(image)
                shape = array.shape

                # change to absolute pixels
                horizontal = int(shape[1] / 2)
                vertical = int(shape[0])

                # set offsets
                offsets = [(0, 0), (2, 0), (0, 2), (2, 2), (1, 1), (1, 3), (3, 1)]

                # calculate all offsets
                for offset, offsetii in offsets:

                    # parse
                    draw, feature, index = pause.split()

                    # make list
                    aggregate = record[feature][feature][0] + record[feature][feature][1]

                    # get coordinates
                    left, top = aggregate[int(index)][2]
                    width, height = aggregate[int(index)][3]

                    # calculate offsets
                    offset = int(offset * horizontal / 4)
                    offsetii = int(offsetii * vertical / 4)

                    # calculate bounding box
                    left = int((left * horizontal) / 2) + offset
                    width = int(width * horizontal / 2)
                    top = int((top * vertical) / 2) + offsetii
                    height = int(height * vertical / 2)

                    # past horizontal boundary?
                    if left + width > horizontal:

                        # subtract
                        left = left - horizontal

                    # check veritical boundary
                    if top + height > vertical:

                        # subtract
                        top = top - vertical

                    # get coordinates for horizontal lines
                    points = []
                    for coordinate in range(left, left + width):

                        # thick lines
                        for thickness in range(3):

                            # add to points
                            points.append((coordinate, top + thickness))
                            points.append((coordinate + horizontal, top + thickness))
                            points.append((coordinate, top + height - thickness))
                            points.append((coordinate + horizontal, top + height - thickness))

                    # get coordinates for vertical lines
                    for coordinate in range(top, top + height):

                        # thick lines
                        for thickness in range(3):

                            # add to points
                            points.append((left + thickness, coordinate))
                            points.append((left + horizontal + thickness, coordinate))
                            points.append((left + width - thickness, coordinate))
                            points.append((left + horizontal + width - thickness, coordinate))

                    # convert all points to black
                    points = list(set(points))
                    for column, row in points:

                        # try to draw box
                        try:

                            # convert to orange
                            array[row][column][0] = 0
                            array[row][column][1] = 255
                            array[row][column][2] = 255

                        # unless index error
                        except IndexError:

                            # skip
                            pass

                # save image
                image = Image.fromarray(array)
                image.save('evaluation/diagnosis.jpg')

            # exit
            if pause in ('XXX', 'xxx', 'XX', 'xx', 'X', 'x'):

                # break
                leave = True
                break

            # restate input
            pause = input('??')

        # check for exit
        if leave:

            # break
            break

    return None


# summarize notes
def notify():
    """Tally up notes.

    Arguments:
        secondary=

    Returns:
        None
    """

    # open file
    with open('evaluation/notes.json', 'r') as pointer:

        # get notes
        notes = json.load(pointer)

    # open summaries
    with open('evaluation/summaries.json', 'r') as pointer:

        # get summaries
        summaries = json.load(pointer)

    # count all notes
    entries = [note for entry in notes.values() for note in entry]
    counter = Counter(entries)

    # print entries
    print(' ')
    entries = [item for item in counter.items()]
    entries.sort(key=lambda pair: pair[1], reverse=True)
    entries.sort(key=lambda pair: pair[0][0])
    [print(entry) for entry in entries]
    print(' ')

    # print confusions
    for feature in ('relevant', 'text', 'face', 'person', 'moderations'):

        # make stubs
        stubs = ('ok_', 'missed_', 'ok_non', 'missed_non')

        # begin confusion matrices
        confusion = {stub: 0 for stub in stubs}

        # add counts to confusion
        for stub in stubs:

            # add to confusion
            confusion[stub] = counter.setdefault(stub + feature, 0)

        # print confusion matrix
        print(' ')
        print('{}  {}  {}'.format(' ' * (len(feature) + 3), feature[0], 'n'))
        print('   {}: {} {}'.format(feature, confusion['ok_'], confusion['missed_']))
        print('{}: {} {}'.format('non' + feature, confusion['missed_non'], confusion['ok_non']))

    return None


# augment a note with another note
def imply(note, implication):
    """Imply one note for a photo from another.

    Arguments:
        note: str
        implication: str

    Returns:
        None
    """

    # open file
    path = 'evaluation/notes.json'
    with open(path, 'r') as pointer:

        # get notes
        data = json.load(pointer)

    # go through notes
    for name, notes in data.items():

        # augment the note
        if note in notes:

            # also add the implication
            notes.append(implication)

        # remove duplicates
        data[name] = list(set(notes))

    # resave file
    with open(path, 'w') as pointer:

        # save
        json.dump(data, pointer)

    return None


# gather up all moderations at various scan resolutions
def gather():
    """Gather up all moderation data from scans at different resolutions.

    Arguments:
        None

    Returns:
        None
    """

    # establish resolutions and number of scans for each
    resolutions = ['1x1', '2x2', '3x3', '1x2', '2x1', '1x3', '3x1']
    scans = [1, 8, 18, 4, 4, 6, 6]

    # establish insert sizes
    sizes = [10, 20, 40, 80, 160, 320]

    # get all inserts
    inserts = os.listdir('demo/inserts')
    inserts = list(set([insert.split('_')[0] for insert in inserts]))

    # make data records for all inserts
    data = {insert: {resolution: {'moderations': [999], 'persons': [999]} for resolution in resolutions} for insert in inserts}

    # go through each data file
    for resolution in resolutions:

        # open file
        with open('demo/inserts_' + resolution + '.json', 'r') as pointer:

            # get records
            records = json.load(pointer)

        # go through each insert
        for insert in inserts:

            # and each size
            for size in sizes:

                # check for moderation data
                name = 'demo/inserts/' + insert + '_' + str(size) + '.jpg'
                moderations = records[name]['detections']['ModerationLabels'] + records[name]['redetections']['ModerationLabels']
                if len(moderations) > 0:

                    # add to data
                    data[insert][resolution]['moderations'].append(size)

                # check for person data
                triggers = ['Person']
                labels = records[name]['detections']['Labels'] + records[name]['redetections']['Labels']
                labels = [label for label in labels if label['Name'] in triggers]
                if len(labels) > 0:

                    # add to data
                    data[insert][resolution]['persons'].append(size)

    # get resolution breakdown
    breakdown = {resolution: {'moderations': {size: 0 for size in sizes + [999]}, 'persons': {size: 0 for size in sizes + [999]}} for resolution in resolutions}

    # make reports
    reports = []
    for insert in inserts:

        # create report
        report = {'name': insert, 'moderations': [], 'persons': []}
        for resolution in resolutions:

            # get smallest size
            moderation = min(data[insert][resolution]['moderations'])
            person = min(data[insert][resolution]['persons'])

            # add to report
            report['moderations'].append(moderation)
            report['persons'].append(person)

            # add to breakdown
            breakdown[resolution]['moderations'][moderation] += 1
            breakdown[resolution]['persons'][person] += 1

        # append
        reports.append(report)

    # print resolutions
    print(' ')
    print('resolutions: {}'.format(resolutions))
    print(' ')

    # sort
    reports.sort(key=lambda report: min(report['moderations'] + report['persons']), reverse=True)
    for report in reports:

        # print
        print(report['moderations'], report['persons'], report['name'])

    # save moderation reports
    with open('moderations.json', 'w') as pointer:

        # dump file
        analysis = {'resolutions': resolutions, 'sizes': sizes, 'scans': scans, 'data': data}
        json.dump(analysis, pointer)

    # print resolutions
    print(' ')
    print('resolutions: {}'.format(resolutions))
    print(' ')

    # get reports where moderation is better than persons
    print(' ')
    print('detected by moderations but not by persons:')
    betters = []
    for report in reports:

        # zip moderations and persons
        zipper = [pair for pair in zip(report['moderations'], report['persons'])]
        if any([pair[0] < pair[1] for pair in zipper]):

            # print report
            betters.append(report)
            print(report['moderations'], report['persons'], report['name'])

    # print breakdown
    print(' ')
    sizes.sort(key=lambda size: size, reverse=True)
    print('sizes: {}'.format([999] + sizes))
    print(' ')

    # assign point values to each size
    points = {999: 0, 320: 1, 160: 2, 80: 4, 40: 8, 20: 16, 10: 32}
    print('points: {}'.format(points))
    print(' ')

    # make summaries
    summaries = []
    for resolution, scan in zip(resolutions, scans):

        # sort moderations
        moderations = breakdown[resolution]['moderations']
        score = sum([points[size] * counts for size, counts in moderations.items()])
        ratio = round(score / scan, 2)
        moderations = [item for item in moderations.items()]
        moderations.sort(key=lambda pair: pair[0], reverse=True)
        moderations = [pair[1] for pair in moderations]

        # sort persons
        persons = breakdown[resolution]['persons']
        scoreii = sum([points[size] * counts for size, counts in persons.items()])
        ratioii = round(scoreii / scan, 2)
        persons = [item for item in persons.items()]
        persons.sort(key=lambda pair: pair[0], reverse=True)
        persons = [pair[1] for pair in persons]

        # add summary
        summary = [resolution, scan, moderations, score, ratio, persons, scoreii, ratioii]
        summaries.append(summary)

    # sort summaries and print
    summaries.sort(key=lambda summary: summary[4], reverse=True)

    # print for moderations
    print(' ')
    print('moderations:')
    for summary in summaries:

        # print
        print('{} ({} scans): {}, {} points ({} points/ scan)'.format(summary[0], summary[1], summary[2], summary[3], summary[4]))

    # sort summaries and print
    summaries.sort(key=lambda summary: summary[7], reverse=True)

    # print for persons
    print(' ')
    print('persons:')
    for summary in summaries:

        # print
        print('{} ({} scans): {}, {} points ({} points/ scan)'.format(summary[0], summary[1], summary[5], summary[6], summary[7]))

    return None


# browse a record
def browse(name):
    """Browse the labels of a particular file.

    Arguments:
        name: str

    Returns:
        None
    """

    # open file
    with open('demo/inserts_2x2.json', 'r') as pointer:

        # get data
        data = json.load(pointer)

    # construct path
    path = 'demo/inserts/' + name + '.jpg'
    labels = data[path]['detections']['Labels']
    labelsii = data[path]['redetections']['Labels']

    # get image
    image = Image.open(path)
    image.save('demo/browsing.jpg')

    # print labels
    for label in labels:

        # print
        print(label['Name'], round(label['Confidence'], 2))

    # print redetecdtion labels
    print(' ')
    for label in labelsii:

        # print
        print(label['Name'], round(label['Confidence'], 2))

    # spacer
    print(' ')

    return None


# redistribute data files amongst new categories
def redistribute(source, sink):
    """Redistribute the data from one directory into the data for another.

    Arguments:
        source: directory
        sink: directory

    Returns:
        None
    """

    # begin data pool
    pool = {}

    # gather all data from the source directory
    paths = [path for path in os.listdir(source) if '.' not in path]
    for path in paths:

        # get all files in subdirectory
        pathsii = os.listdir('/'.join([source, path]))
        for pathii in pathsii:

            # if it is a json file
            if '.json' in pathii:

                # open file
                name = '/'.join([source, path, pathii])
                with open(name, 'r') as pointer:

                    # get scan results
                    results = json.load(pointer)

                # add to data pool
                pool.update(results)

    # get all source directories
    paths = os.listdir(sink)
    for path in paths:

        # get all folders
        folders = [folder for folder in os.listdir('/'.join([sink, path])) if '.' not in folder]
        for folder in folders:

            # begin data
            data = {}

            # get contents
            images = os.listdir('/'.join([sink, path, folder]))
            for image in images:

                # get the record from the pool
                name = [key for key in pool.keys() if image in key][0]

                # make deposit path
                identifier = '/'.join([sink, path, folder, image])
                data[identifier] = pool[name]

            # write data file
            deposit = '/'.join([sink, path, folder]) + '.json'
            with open(deposit, 'w') as pointer:

                # write file
                json.dump(data, pointer)

    return None


# demonstrate the best resolution with graphs
def demonstrate():
    """Demonstrate the best resolution settings for moderations and labels.

    Arguments:
        None

    Returns:
        None
    """

    # get moderations data
    with open('moderations.json', 'r') as pointer:

        # load up
        moderations = json.load(pointer)

    # get triggers data
    triggers = load('demo/triggers.json')

    # prune data for red herrings
    moderations['data'] = {key: value for key, value in moderations['data'].items() if triggers['demo/moderations/' + key + '.jpg'] != 'red herring'}

    # get sizes and resolutions
    resolutions = moderations['resolutions']
    scans = moderations['scans']
    sizes = moderations['sizes']
    sizes.reverse()

    # pair up resolutions and scans
    points = {size: [] for size in sizes}
    pairs = [pair for pair in zip(resolutions, scans) if pair[0] not in ('1x2', '1x3')]
    for resolution, number in pairs:

        # get percent detected for each size
        for size in sizes:

            # get total number
            total = len(moderations['data'].keys())
            count = 0
            for name in moderations['data'].keys():

                # check for detection of that size
                if size in moderations['data'][name][resolution]['moderations']:

                    # advance count
                    count += 1

            # add percentage to points
            percent = round(100 * count / total, 2)
            points[size].append((number, percent))

    # sort all points
    [points[size].sort(key=lambda pair: pair[0]) for size in sizes]

    # plot points
    pyplot.cla()
    colors = ['magenta', 'red', 'orange', 'green', 'cyan', 'blue']
    for color, size in zip(colors, sizes):

        # plot the lines
        xs = [point[0] for point in points[size]]
        ys = [point[1] for point in points[size]]
        pyplot.plot(xs, ys, color=color, label=str(size) + ' pixels')

    # save the figure
    pyplot.xlabel('number of scans')
    pyplot.ylabel('percent detected')
    pyplot.xticks([1, 4, 6, 8, 18])
    pyplot.legend()
    pyplot.title('percent moderations detected by image height in pixels')
    pyplot.savefig('moderations.png')

    # pair up resolutions and scans
    points = {size: [] for size in sizes}
    pairs = [pair for pair in zip(resolutions, scans) if pair[0] not in ('1x2', '1x3')]
    for resolution, number in pairs:

        # get percent detected for each size
        for size in sizes:

            # get total number
            total = len(moderations['data'].keys())
            count = 0
            for name in moderations['data'].keys():

                # check for detection of that size
                if size in moderations['data'][name][resolution]['persons']:

                    # advance count
                    count += 1

            # add percentage to points
            percent = round(100 * count / total, 2)
            points[size].append((number, percent))

    # sort all points
    [points[size].sort(key=lambda pair: pair[0]) for size in sizes]

    # plot points
    pyplot.cla()
    colors = ['magenta', 'red', 'orange', 'green', 'cyan', 'blue']
    for color, size in zip(colors, sizes):

        # plot the lines
        xs = [point[0] for point in points[size]]
        ys = [point[1] for point in points[size]]
        pyplot.plot(xs, ys, color=color, label=str(size) + ' pixels')

    # save the figure
    pyplot.xlabel('number of scans')
    pyplot.ylabel('percent detected')
    pyplot.xticks([1, 4, 6, 8, 18])
    pyplot.legend()
    pyplot.title('percent moderations detected as persons by image height')
    pyplot.savefig('persons.png')

    # pair up resolutions and scans
    points = {size: [] for size in sizes}
    pairs = [pair for pair in zip(resolutions, scans) if pair[0] not in ('1x2', '1x3')]
    resolution = '2x2'
    number = 8

    # got through second set
    for resolutionii, numberii in pairs:

        # get percent detected for each size
        for size in sizes:

            # get total number
            total = len(moderations['data'].keys())
            count = 0
            for name in moderations['data'].keys():

                # check for detection in moderations and person
                person = moderations['data'][name][resolution]['persons']
                moderation = moderations['data'][name][resolutionii]['moderations']
                if size in moderation or size in person:

                    # advance count
                    count += 1

            # add percentage to points
            percent = round(100 * count / total, 2)
            points[size].append((number + numberii, percent))

    # sort all points
    [points[size].sort(key=lambda pair: pair[0]) for size in sizes]

    # plot points
    pyplot.cla()
    colors = ['magenta', 'red', 'orange', 'green', 'cyan', 'blue']
    for color, size in zip(colors, sizes):

        # plot the lines
        xs = [point[0] for point in points[size]]
        ys = [point[1] for point in points[size]]
        pyplot.plot(xs, ys, color=color, label=str(size) + ' pixels')

    # save the figure
    pyplot.xlabel('number of scans')
    pyplot.ylabel('percent detected')
    pyplot.xticks([9, 12, 14, 16, 26])
    pyplot.legend()
    pyplot.title('percent combined detections assuming 8 scans for persons')
    pyplot.savefig('combined.png')

    # print nondetects
    forties = [key for key, value in moderations['data'].items() if 40 not in value['2x2']['persons']]
    # print nondetects
    eighties = [key for key, value in moderations['data'].items() if 80 not in value['2x2']['persons']]
    difference = [entry for entry in forties if entry not in eighties]
    print('not detected at 80:')
    print(eighties)
    print('not detected at 40:')
    print(forties)
    print('difference:')
    print(difference)

    return None


# make csv of moderation labels
def damn():
    """Make csv of moderation labels.

    Arguments:
        None

    Returns:
        None
    """

    # load up moderation scans
    with open('demo/moderationscans.json', 'r') as pointer:

        # get moderations
        moderations = json.load(pointer)

    # load up moderations at single resolution
    with open('demo/inserts_1x1.json', 'r') as pointer:

        # get single moderations
        singles = json.load(pointer)

    # load up labels at 2x2 resolution
    with open('demo/inserts_2x2.json', 'r') as pointer:

        # get scans
        labels = json.load(pointer)

    # save triggers
    with open('demo/triggers.json', 'r') as pointer:

        # save triggers
        triggers = json.load(pointer)

    # get the highest ranked moderation for each image
    damnations = []
    breakdown = {}
    for path, data in moderations.items():

        # get the highest moderation
        detections = data['detections']['ModerationLabels']
        detections.sort(key=lambda detection: detection['Confidence'])

        # make entry
        entry = {'group': triggers[path], 'Full': False, '320': False, '160': False, '80': False, '40': False}

        # check length
        if len(detections) > 0:

            # append
            damnation = detections[0]['Name']
            damnations.append(damnation)

            # add to group name
            entry['Full'] = True

        # check for persons
        if 'Person' in [entry['Name'] for entry in data['detections']['Labels']]:

            # add to group name
            entry['Full'] = True

        # # print if len 0
        # if len(detections) < 1:
        #
        #     # print
        #     print(path)

        # get detections
        for level in ('320', '160', '80', '40'):

            # get path name
            name = path.replace('moderations', 'inserts').replace('.jpg', '_' + level + '.jpg')

            # check for moderations
            if 'moderations' in singles[name]['flags']:

                # switch to true
                entry[level] = True

            # check for persons
            members = labels[name]['detections']['Labels'] + labels[name]['redetections']['Labels']
            if 'Person' in [member['Name'] for member in members]:

                # switch to true
                entry[level] = True

        # add path to breakdown
        breakdown[path] = entry

    # count members
    counter = Counter(damnations)

    # make csv
    headers = ['detection', 'count']
    rows = [item for item in counter.items()]
    rows.sort(key=lambda item: item[1], reverse=True)
    with open('damnations.csv', 'w') as pointer:

        # write rows
        csv.writer(pointer).writerows([headers] + rows)

    # print breakdown
    print(' ')

    # go through each level
    for level in ('320', '160', '80', '40'):

        # count all categories
        counter = Counter([entry['group'] for entry in breakdown.values() if entry[level]])
        print(level)
        print(counter)
        print(' ')

    # print nondetects
    for path, entry in breakdown.items():

        # print
        if entry['group'] == 'Nondetect':

            # print
            print(path)
            print(entry)

    # print each entry
    print(' ')
    breakdowns = [item for item in breakdown.items()]
    breakdowns.sort(key=lambda item: item[1]['group'])

    # go through them
    for down in breakdowns:

        # print
        print(' ')
        print(down)

    # print counter
    print(' ')
    counter = Counter([value for value in triggers.values()])
    counter = [item for item in counter.items()]
    counter.sort(key=lambda item: item[1], reverse=True)
    for entry in counter:

        # print
        print(entry)

    # go through breakdowns
    categories = [item[0] for item in counter]
    for field in ('Full', '320', '160', '80', '40'):

        # print field
        print(field)

        # sum up each category
        for category in categories:

            # go through breakdowns
            amount = 0
            for down in breakdowns:

                # add to amount
                if down[1]['group'] == category and down[1][field]:

                    # add point
                    amount += 1

            # print
            print(field, category, amount)

    return None


# scrub redetections from evaluation data
def scrub(path):
    """Scrub out redetections for all but labels.

    Arguments:
        path: path to json data file

    Returns:
        None
    """

    # open file
    with open(path, 'r') as pointer:

        # retrieve
        records = json.load(pointer)

    # go through each entry
    for image, data in records.items():

        # set redetctions to empty
        data['redetections']['TextDetections'] = []
        data['redetections']['FaceDetails'] = []
        data['redetections']['ModerationLabels'] = []

    # save file
    with open(path, 'w') as pointer:

        # write file
        json.dump(records, pointer)

    return None


# make a csv file from a confusion matrix
def confuse(name, nameii, goods, waste, mistakes, bads, path=None):
    """Generate a confusion matrix csv.

    Arguments:
        name: good label
        nameii: bad label
        goods: int
        bads: int
        mistakes: int
        waste: int
        path=None: path name for file

    Returns:
        None
    """

    # begin with header
    header = [' ', name + ' by AI', nameii + ' by AI']

    # make good row
    percent = 0
    percentii = 0
    if goods + waste > 0:

        # calculate
        percent = round(100 * goods / (goods + waste), 1)
        percentii = round(100 * waste / (goods + waste), 1)

    # make row
    row = [name + ' by human', '{} ({} %)'.format(goods, percent), '{} ({} %)'.format(waste, percentii)]

    # make bad row
    percent = 0
    percentii = 0
    if bads + mistakes > 0:

        # calculate
        percent = round(100 * mistakes / (mistakes + bads), 1)
        percentii = round(100 * bads / (mistakes + bads), 1)

    # make row
    rowii = [nameii + ' by human', '{} ({} %)'.format(mistakes, percent), '{} ({} %)'.format(bads, percentii)]

    # make csv
    if not path:

        # set default path
        path = name + '_confusion.csv'

    # write file
    with open(path, 'w') as pointer:

        # write rows
        csv.writer(pointer).writerows([header, row, rowii])

    return None


# get breakdown of false negatives
def falsify():
    """Get breakdown of false negatives.

    Arguments:
        None

    Returns:
        None
    """

    # open summaries
    with open('evaluation/summaries.json', 'r') as pointer:

        # get summaries
        summaries = json.load(pointer)

    # only keep false negatives
    records = [record for record in summaries if record['batch'] == 'approvals' and not record['approve']]

    # get flags
    flags = [tuple(record['flags']) for record in records]

        # count members
    counter = Counter(flags)

    # make csv
    headers = ['detection', 'count']
    rows = [item for item in counter.items()]
    rows.sort(key=lambda item: item[1], reverse=True)
    with open('wastes.csv', 'w') as pointer:

        # write rows
        csv.writer(pointer).writerows([headers] + rows)

    # print
    for row in rows:

        # print
        print(row)

    return None


# estimate number of photos per month
def estimate():
    """Estimate number of photos per month.

    Arguments:
        None

    Returns:
        None
    """

    # set protocols
    protocols = ['clouds', 'lands', 'trees', 'mosquitoes']
    translation = {'mosquito': 'mosquitoes', 'land cover': 'lands', 'tree height': 'trees', 'clouds': 'clouds'}

    # function for exracting month
    extract = lambda date: '-'.join(date.split('-')[:2])

    # load up rejections data
    with open('report/rejections.csv') as pointer:

        # get rejections
        rows = [row for row in csv.reader(pointer)]

    # make photo records
    rows = [row for row in rows[1:] if row[2] != 'unknown' and row[0] > '2019-08Z']
    photos = [{'month': extract(row[0]), 'url': row[1], 'protocol': translation[row[2]], 'status': 'rejected', 'source': '??'} for row in rows]

    # load up approvals data
    sources = []
    for protocol in protocols:

        # get file
        path = 'report/' + protocol + '.csv'
        with open(path, 'r') as pointer:

            # get rows
            rows = [row for row in csv.reader(pointer)]

        # get data
        headers = rows[0]
        records = [{header: entry for header, entry in zip(headers, row)} for row in rows[1:]]
        for record in records:

            # get data source
            field = [key for key in record.keys() if 'DataSource' in key][0]
            source = record[field]
            sources.append(source)

            # go through each key
            fields = [key for key in record.keys() if 'PhotoUrl' in key]
            for field in fields:

                # split urls on semicolon
                urls = record[field].split(';')
                urls = [url for url in urls if 'rejected' not in url]
                for url in urls:

                    # make photo record
                    photo = {'month': extract(record['measuredDate']), 'url': url, 'protocol': protocol, 'status': 'approved', 'source': source}
                    photos.append(photo)

    # count all sources
    counter = Counter(sources)
    sources = [item for item in counter.items()]
    sources.sort(key=lambda item: item[1], reverse=True)
    sources = {source[0]: index for index, source in enumerate(sources)}
    sources.update({'??': len(sources)})
    print([key for key in sources.keys()])

    # count by month, status
    summaries = [(photo['month'], photo['status'], photo['source']) for photo in photos]
    counter = Counter(summaries)
    counter = [item for item in counter.items()]

    # combine sources into one breakdown
    breakdowns = {}
    for summary, count in counter:

        # make key from first items
        key = (summary[0], summary[1])
        if key not in breakdowns:

            # add to key
             breakdowns[key] = [0] * len(sources)

        # add to breakdown
        breakdowns[key][sources[summary[2]]] = count

    # convert to lists
    breakdowns = [list(key) + value for key, value in breakdowns.items()]
    breakdowns.sort(key=lambda item: item[1])
    breakdowns.sort(key=lambda item: item[0])

    # print counter
    pprint(breakdowns)

    # create csv
    headers = ['month', 'status'] + [key for key in sources.keys()]
    rows = [headers] + [breakdown for breakdown in breakdowns]
    with open('report/months.csv', 'w') as pointer:

        # write rows
        csv.writer(pointer).writerows(rows)

    return None


# calculate max and average size of faces
def facelift():
    """Calculate the average size of detected faces.

    Arguments:
        None

    Returns:
        None
    """

    # open summaries
    with open('evaluation/summaries.json', 'r') as pointer:

        # get summaries
        summaries = json.load(pointer)

    # get faces subset
    records = [record for record in summaries if not record['approve'] and record['faces']['counts'][0] > 0]

    # calculate areas
    areas = [record['faces']['faces'][0][0][3][0] * record['faces']['faces'][0][0][3][1] for record in records]

    # print max
    print('max: {}'.format(max(areas)))
    print('average: {}'.format(numpy.average(areas)))

    # print top faces
    zipper = [(record, area) for record, area in zip(records, areas)]
    zipper.sort(key=lambda pair: pair[1], reverse=True)
    for pair in zipper[:10]:

        # print
        print(pair[0]['name'], pair[1])

    return None


# trigger function to group moderation scans by category
def trigger():
    """Define trigger labels for moderations.

    Arguments:
        None

    Returns:
        None
    """

    # load moderation scans
    with open('demo/moderationscans.json', 'r') as pointer:

        # get moderations
        moderations = json.load(pointer)

    # go through each photo
    triggers = {}
    for photo in moderations.keys():

        # put photo into diagnosis
        image = Image.open(photo);
        image.save('demo/diagnosis.jpg')

        # ask for trigger
        trigger = input('trigger?')
        triggers[photo] = trigger

    # save triggers
    with open('demo/triggers.json', 'w') as pointer:

        # save triggers
        json.dump(triggers, pointer)

    return None


# load photo match pairs
def bridge(path):
    """Pick the right match for each group of photos.

    Arguments:
        path: str

    Returns:
        None
    """

    # load up matches
    matches = load(path)

    # go through each pair
    singles = {}
    count = 0
    for desk, urls in matches.items():

        # status and advance count
        print('#{} of {}...'.format(count, len(matches)))
        count += 1

        # load up image
        desktop = Image.open(desk)
        desktop.save('evaluation/desk_image.jpg')

        # check urls length
        if len(urls) == 1:

            # add to list
            singles[desk] = urls[0]

        # otherwise
        else:

            # go through loop
            pause = ''
            phase = 0
            while not pause.isdigit():

                # print number
                print('#{} of {}'.format(phase, len(urls)))

                # get web image
                web = Image.open(urlopen(urls[phase]))
                web.save('evaluation/web_image.jpg')

                # advance phase
                phase = (phase + 1) % len(urls)

                # get input
                pause = input('>>>?')

            # set digit
            singles[desk] = urls[int(pause)]

    # save file
    deposit = path.replace('matches', 'singles')
    dump(singles, deposit)

    return None


# excise a bad url from the feed list
def excise(*sites):
    """Excise bad sites from the url feed.

    Arguments:
        *sites: unpacked tuple of sites

    Returns:
        None
    """

    # open up errors, feed, and identities
    errors = load('evaluation/approval_errors.json')
    feed = load('evaluation/approval_url_feed.json')
    identities = load('evaluation/urls_identities.json')

    # add sites to errors
    for site in sites:

        # add to errors
        errors.append(site)

    # subtract errors from feed
    feed = {url: '' for url in feed.keys() if url not in errors}

    # subract identities from feed
    feed = {url: '' for url in feed.keys() if url not in identities.keys()}

    # save files
    dump(errors, 'evaluation/approval_errors.json')
    dump(feed, 'evaluation/approval_url_feed.json')

    # print length
    print('feed length: {}'.format(len(feed)))

    return None


# identify traits of photos
def identify(number=5000, chunk=100):
    """Identify photos by a few choice pixels.

    Arguments:
        number: int, number of records

    Returns:
        None
    """

    # get time
    initial = time()

    # get feed list
    feed = load('evaluation/mosquito_feed.json')

    # get identities
    path = 'evaluation/mosquito_identities.json'
    identities = load(path)

    # get rid of any photo already identified
    pool = [key.replace(' ', '') for key in feed.keys()]
    pool = [member for member in pool if member not in identities.keys()]

    # print length
    print('length of feed: {}'.format(len(feed)))
    print('length of identities: {}'.format(len(identities)))
    print('entire pool length: {}'.format(len(pool)))

    # get top number
    pool = pool[:number]

    # set up thread
    thread = Pool(chunk)

    # while pool is not empty
    while len(pool) > 0:

        print('pool length: {}'.format(len(pool)))

        # get chunk
        members = pool[:chunk]
        pool = pool[chunk:]

        # get pool results
        results = thread.map(tag, members)

        # add results
        for member, result in zip(members, results):

            # if not none
            if result:

                # add to identities
                identities[member] = result

        # save
        print('saving {}...'.format(path))
        dump(identities, path)

        # print time
        final = time() - initial
        print('took {} minutes.'.format(final / 60))

    return None


# tag a photo with pixel info
def tag(url):
    """Tag a url with pixel info.

    Arguments:
        url: file path

    Returns:
        None
    """

    print(url)

    # get info
    try:

        # get image
        image = Image.open(urlopen(url))
        image = numpy.array(image)

        # get dimensions
        width = int(len(image[0]))
        height = int(len(image))

        # get points
        points = [[0.25, 0.25], [0.75, 0.75], [0.25, 0.75], [0.75, 0.25]]
        points += [[0.5, 0.5], [0.25, 0.5], [0.5, 0.25], [0.5, 0.75], [0.75, 0.5]]

        # get pixels
        pixels = []
        for point in points:

            # get pixel values
            x = floor(width * point[0])
            y = floor(height * point[1])
            r = int(image[y][x][0])
            g = int(image[y][x][1])
            b = int(image[y][x][2])

            # make pixel
            pixel = {'x': x, 'y': y, 'r': r, 'g': g, 'b': b, 'source': 'python'}
            pixels.append(pixel)

        # make identifier
        identifier = {'width': width, 'height': height, 'pixels': pixels}

    # except an error
    except:

        # print error
        print('error!: {}'.format(url))

        # error is none
        identifier = None

    return identifier


# match photos
def match(inward, onward, outward):
    """Match photos from two files.

    Arguments:
        inward: file path
        onward: second file path
        outward: deposit file path

    Returns:
        None
    """

    # get photo identities
    identities = load(inward)
    pool = load(onward)

    # make outward file
    make(outward)

    # begin matches
    matches = {}

    # go through each inward image
    count = 0
    for path, info in identities.items():

        # status
        count += 1
        print('{} of {}'.format(count, len(identities)))

        # get width and height
        width = info['width']
        height = info['height']

        # get all images with same width and height
        congruents = {member: infoii for member, infoii in pool.items() if infoii['width'] == width and infoii['height'] == height}

        # determine distances between congruents and original
        distances = []
        for congruent, infoii in congruents.items():

            # determine distance between pixels
            distance = 0
            for pixel, pixelii in zip(info['pixels'], infoii['pixels']):

                # add squared differences
                for channel in ('r', 'g', 'b'):

                    # add squared difference
                    distance += (pixel[channel] - pixelii[channel]) ** 2

            # add to distances
            distances.append((congruent, distance))

        # sort
        distances.sort(key=lambda pair: pair[1])

        # make image
        image = Image.open(path)
        image = numpy.array(image)

        # go through each congruent
        for congruent, distance in distances:

            # make second image
            congruence = Image.open(urlopen(congruent))
            congruence = numpy.array(congruence)

            # combine images
            combination = numpy.concatenate((image, congruence), 1)
            combination = Image.fromarray(combination)
            combination.save('evaluation/diagnosis.jpg')

            # print
            print(congruent)
            print(distance)

            # present choice
            pause = input('>>?')

            # check input
            if pause in ('', ' '):

                # add match and break
                matches[path] = congruent
                break

            # check for skip
            elif pause in ('skip',):

                # just break
                break

            # otherwise continue
            else:

                # pass
                pass

    # save matches
    dump(matches, outward)

    # summarize
    print('matches found: {}'.format(len(matches)))
    print('matches not found:')
    for path, info in identities.items():

        # check for inclusion
        if path not in matches.keys():

            # print path
            print(path)

    return None


# get photo projections
def reabsorb():
    """Absorb all photos urls from several protocols.

    Arguments:
        None

    Returns:
        None
    """

    # protocols
    photos = []
    for protocol in protocols:

        # read in csv and find photos
        names = os.listdir('csvs/' + protocol)

        # get headings
        paths = ['csvs/' + protocol + '/' + name for name in names]

        # convert csvs
        documents = []
        for path in paths:

            # read in csvs
            with open(path, 'r') as pointer:

                # read in rows
                rows = [row for row in csv.reader(pointer)]

            # make dict
            headers = rows[0]
            records = [{field: info for field, info in zip(headers, row)} for row in rows[1:]]
            documents += records

        # go through records
        for record in documents:

            # go through each key
            fields = [key for key in record.keys() if 'PhotoUrl' in key]
            for field in fields:

                # split urls on semicolon
                urls = record[field].split(';')
                urls = [url for url in urls if url not in ('', ' ')]
                for url in urls:

                    # make photo record
                    photo = {'date': record['measuredDate'], 'url': url, 'protocol': protocol, 'field': field}
                    photos.append(photo)

        # keys = [key for key in documents[0].keys() if 'photo' in key.lower()]
        # for record in documents:
        #
        #     # go through keys
        #     for key in keys:
        #
        #         # split by ';'
        #         urls = [url for url in record[key].split(';')]
        #         urls = [url for url in urls if 'https' in url]
        #
        #         # make entry for each
        #         for url in urls:
        #
        #             # make entry
        #             photo = {'date': record['measuredDate'], 'field': key, 'protocol': protocol, 'url': url}
        #             photos.append(photo)

    # write to json
    with open('evaluation/photo_counts.json', 'w') as pointer:

        # dump into json
        json.dump(photos, pointer)

    return None


# verify
def verify():
    """Verify that the quality level does not seriously impact detections.

    Arguments:
        None

    Returns:
        None
    """

    # load up test data
    data = load('quality/testing.json')

    # find all base images
    bases = []
    for key in data.keys():

        # get the base from the key
        base = '_'.join([splitting for splitting in key.split('_')[:-1]])
        bases.append(base)

    # remove duplicates
    bases = list(set(bases))

    # go through each base
    erosion = {base: {} for base in bases}
    for base in bases:

        # go through all keys
        for key in data.keys():

            # check for inclusion
            if base in key:

                # get quality
                quality = int(key.replace(base, '').replace('_', '').replace('.jpg', ''))

                # get labels
                labels = {label['Name']: label['Confidence'] for label in data[key]['detections']['Labels'] + data[key]['redetections']['Labels']}
                erosion[base][quality] = labels

    # convert to report
    report = {base: {} for base in bases}
    for base, info in erosion.items():

        # get all qualities
        qualities = [key for key in info.keys()]
        qualities.sort(reverse=True)

        # get all label names
        labels = [label for quality in qualities for label in erosion[base][quality].keys()]
        labels = list(set(labels))
        labels.sort()

        # add qualities
        report[base]['qualities'] = qualities

        # add label entries
        for label in labels:

            # make list
            confidences = []
            for quality in qualities:

                # try to find confidence
                try:

                    # get confidence
                    confidence = int(erosion[base][quality][label])

                # otherwise
                except KeyError:

                    # confidence is 0
                    confidence = 0

                # append confidence
                confidences.append(confidence)

            # add entry
            report[base][label] = confidences

    # create plot colors
    colors = {9: 'g--', 8: 'r--', 7: 'b', 6: 'y', 5: 'm'}

    # begin plot
    pyplot.cla()
    pyplot.title('quality vs detection confidence')
    pyplot.xlabel('quality (%)')
    pyplot.ylabel('confidence (%)')

    # create plots
    for base in bases:

        # plot each label
        steady = 0
        unsteady = 0
        for index, label in enumerate(report[base].keys()):

            # don't plot qualities
            if label != 'qualities':

                # get qualities and confidence
                qualities = report[base]['qualities']
                confidences = report[base][label]

                # get color
                nonzeroes = [confidence for confidence in confidences if confidence > 0]
                color = colors[int(numpy.average(nonzeroes) / 10)]

                # add to steady or unsteady
                if 0 in confidences[1:8] and confidences[0] != 0:

                    # add to unsteady
                    unsteady += 1
                    print('{}: {}'.format(label, confidences))

                # otherwise
                else:

                    # add to steady
                    steady += 1

                # plot
                pyplot.plot(qualities, confidences, color, label=label)

        # print base summary
        print('{}: '.format(base))
        print('steady: {}'.format(steady))
        print('unsteady: {}'.format(unsteady))

    # save figure
    pyplot.savefig('quality/erosion.png')
    pyplot.cla()
    pyplot.close()

    # save data as json
    dump(report, 'quality/erosion.json')

    return None