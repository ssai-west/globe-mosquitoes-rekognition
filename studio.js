// studio.js for image manipulation

// import Jimp image manipulation modeule
var Jimp = require('jimp');

// import support modules
var knobs = require('./knobs.js');
var tools = require('./tools.js');

// make dictionary of Jimp MIMEs
var mimes = {'image/jpeg': Jimp.MIME_JPEG, 'image/png': Jimp.MIME_PNG, 'image/bmp': Jimp.MIME_BMP};


// blur an image to a given degree (pixel radius)
function blur(image, degree) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // clone the image and blur it
        var clone = xerox(image);
        clone.blur(degree, function(error, blurred) {

            // if an error occurs
            if (error) {

                // print it
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the blurred image
                resolve(blurred);
            }
        })
    })
    return promise;
}


// shrink a snippet, blur it, and enlarge it again
function bounce(box, image) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // punch out the snippet
        var clone = xerox(image);
        var snippet = punch(box.box, clone);
        var size = Math.round(snippet.bitmap.height);

        // scale the snippet to a height of 50 pixels
        var scaling = scale(snippet, 50);
        scaling.then(function(scaled) {

            // blur the snippet according to mode
            var degree = knobs.degrees[box.mode]
            var blurring = blur(scaled, degree);
            blurring.then(function(blurred) {

                // rescale to original size
                var rescaling = studio.scale(blurred, size);
                rescaling.then(function(rescaled) {

                    // and resolve the promise with a snippet object
                    resolve({'snippet': rescaled, 'box': box.box});
                })
            })
        })
    })
    return promise;
}


// bound a polygon in its bounding box
function bound(polygon) {

    // get xs and ys
    var xs = polygon.map(function(point) {return point.X});
    var ys = polygon.map(function(point) {return point.Y});

    // calculate left and top
    var left = Math.min(...xs);
    var top = Math.min(...ys);

    // calculate width and height
    var width = Math.max(...xs) - left;
    var height = Math.max(...ys) - top;

    // make box
    var box = {"Left": left, "Top": top, "Width": width, "Height": height};

    return box;
}


// budge the coordinates of a bounding box according to a quadrant
function budge(box, quadrant) {

    // coorect for negative widths or heights
    var box = rectify(box);

    // correct left and top
    var left = (box.Left / quadrant.resolution[0]) + quadrant.Left + quadrant.shift[0];
    var top = (box.Top / quadrant.resolution[1]) + quadrant.Top + quadrant.shift[1];

    // correct width and height
    var width = box.Width / quadrant.resolution[0];
    var height = box.Height / quadrant.resolution[1];

    // correct for wrapping across boundaries
    [left, top, width, height] = unwrap(left, top, width, height);

    // redefine bounding box
    box = {'Left': left, 'Top': top, 'Width': width, 'Height': height};

    return box;
}


// create a buffer from an image
function buff(image) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // get mime
        var mime = image.getMIME();

        // default Jimp format to Auto
        var format = Jimp.AUTO;
        if (Object.keys(mimes).includes(mime)) {

            // unless specific mime is available
            format = mimes[mime];
        }
        // create buffer
        image.getBuffer(format, function(error, buffer) {

            // if an error occurs
            if (error) {

                // log it
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the buffer
                resolve(buffer);
            }
        })
    })
    return promise;
}


// convert a png file to a bmp
function bump(path, directory) {

    // deconstruct name
    var name = path.split('/').pop().replace('.png', '');

    // read the file
    Jimp.read(path, function (error, image) {

        // rewrite to new directory
        var deposit = directory + '/' + name + '.bmp';
        tools.debug('writing ' + deposit + '...', 2);
        image.write(deposit);
    })
    return;
}


// keep a polygon coherent across quadrant boundaries
function cohere(polygon) {

    // correct points that are coherently beyond the bounds for xs
    coherent = polygon.every(function(point) {return point.X >= 1.0});
    polygon.forEach(function(point) {

        // wrap to the other side
        if (coherent) {

            // correct point
            point.X = point.X - 1;
        }
        // otherwise round to the border
        else {

            // round
            point.X = Math.min(point.X, 1);
        }
    })
    // correct points that are coherently beyond the bounds for ys
    coherent = polygon.every(function(point) {return point.Y >= 1.0});
    polygon.forEach(function(point) {

        // wrap to the other side
        if (coherent) {

            // correct point
            point.Y = point.Y - 1;
        }
        // otherwise round to the border
        else {

            // round
            point.Y = Math.min(point.Y, 1);
        }
    })
    return polygon;
}


// diagonalize an image by shifting it diagonally and wrapping the edges around
function diagonalize(source, shifts) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // unpack shifts
        var [shift, shiftii] = shifts;

        // get the image
        var imagining = imagine(source);
        imagining.then(function(image) {

            // get mime
            var mime = image.getMIME();

            // get image width and height
            var width = image.bitmap.width;
            var height = image.bitmap.height;

            // define offsets
            var offset = 1 - shift;
            var offsetii = 1 - shiftii;

            // punch out main section
            var box = {'Left': shift, 'Top': shiftii, 'Width': offset, 'Height': offsetii};
            var main = punch(box, image);

            // punch out left
            var box = {'Left': 0, 'Top': shiftii, 'Width': shift, 'Height': offsetii};
            var left = punch(box, image);

            // punch out top
            var box = {'Left': shift, 'Top': 0, 'Width': offset, 'Height': shiftii};
            var top = punch(box, image);

            // punch out corner
            var box = {'Left': 0, 'Top': 0, 'Width': shift, 'Height': shiftii};
            var corner = punch(box, image);

            // create a blank image
            new Jimp(width, height, function(error, diagonal) {

                // if an error occurs
                if (error) {

                    // log it
                    tools.debug(error, 1);
                }
                // otherwise
                if (!error) {

                    // reapply mime
                    diagonal._originalMime = mime;

                    // assemble new image with composites
                    diagonal.composite(main, 0, 0);
                    diagonal.composite(left, offset * width, 0);
                    diagonal.composite(top, 0, offsetii * height);
                    diagonal.composite(corner, offset * width, offsetii * height);

                    // and resolve the promise with the diagonal image
                    resolve(diagonal);
                }
            })
        })
    })
    return promise;
}


// dice an image into quadrants according to resolution settings
function dice(resolution, resolutionii) {

    // begin boxes
    var boxes = [];

    // but skip if both components are only 1
    if (resolution > 1 || resolutionii > 1) {

        // get reciprocal of resolution
        var reciprocal = 1 / resolution;
        var reciprocalii = 1 / resolutionii;

        // get steps
        var steps = [...Array(resolution).keys()];
        var stepsii = [...Array(resolutionii).keys()];

        // for each column
        steps.forEach(function(step) {

            // and each row
            stepsii.forEach(function(stepii) {

                // make a box
                var box = {'Left': step * reciprocal, 'Top': stepii * reciprocalii};
                Object.assign(box, {'Width': reciprocal, 'Height': reciprocalii});
                boxes.push(box);
            })
        })
    }
    return boxes;
}


// lower a jpg's quality towards a file size limit
function disqualify(image, quality, limit) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // set initial image quality
        image.quality(quality);

        // convert to a buffer
        var buffing = buff(image);
        buffing.then(function(buffer) {

            // and measure its size
            tools.debug('quality: ' + quality);
            tools.debug('buffer size: ' + buffer.length);

            // resolve with the buffer if it is already below the limit
            if (buffer.length <= limit) {

                // resolve with buffer
                resolve(buffer);
            }
            // otherwise
            else {

                // decrease quality by 1 and retry
                quality = quality - 1;
                resolve(disqualify(image, quality, limit));
            }
        })
    })
    return promise;
}


// lower a png's file size towards a limit
function disquantify(image, factor, limit) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // scale image by a factor
        var scaling = scale(image, Math.floor(factor * image.bitmap.height));
        scaling.then(function(scaled) {

            // determine file size
            var buffing = buff(scaled);
            buffing.then(function(buffer) {

                // print status
                tools.debug('factor: ' + factor);
                tools.debug('buffer size: ' + buffer.length);

                // if already below the limit
                if (buffer.length <= limit) {

                    // resolve with buffer
                    resolve(buffer);
                }
                // otherwise
                else {

                    // decrease scaling factor by 1% and retry
                    factor = factor - 0.01;
                    resolve(disquantify(image, factor, limit));
                }
            })
        })
    })
    return promise;
}


// equate two detections
function equate(primary, secondary) {

    // check for the same name
    var equality = primary.Name == secondary.Name;

    return equality;
}


// estimate the quality of a jpg images at max file size
function estimate(image, limit) {

    // create promise
    var promise = new Promise(function (resolve, reject) {

        // check for estimation settings
        if (knobs.estimates['jpg']) {

            // set image quality to 100
            var clone = xerox(image);
            clone.quality(100);

            // convert to a file buffer
            var measuring = buff(clone);
            measuring.then(function(measurement) {

                // and measure the file length
                var hundred = measurement.length;
                tools.debug('file size at quality 100: ' + hundred);

                // set quality to 50
                var cloneii = xerox(image);
                cloneii.quality(50);

                // convet to a file buffer
                var measuringii = buff(cloneii);
                measuringii.then(function(measurementii) {

                    // and measure the file length again
                    var fifty = measurementii.length;
                    tools.debug('file size at quality 50: ' + fifty);

                    // determine parameters of hyperbola: y = 1 / a(x - c)
                    var center = (hundred * 100 - fifty * 50) / (hundred - fifty);
                    var multiplier = 1 / (hundred * (100 - center));

                    // extrapolate the desired quality from the file limit: x = 1 / ay + c
                    var quality = (1 / (multiplier * limit)) + center;
                    quality = Math.floor(quality);
                    tools.debug('estimated quality: ' + quality);

                    // resolve with estimated quality
                    resolve(quality);
                })
            })
        }
        // otherwise
        else {

            // default to 100 and resolve the promise
            quality = 100;
            resolve(quality);
        }
    })
    return promise;
}


// extrapolate the best scale factor to meet the file size limit for a png
function extrapolate(image, limit) {

    // create promise
    var promise = new Promise(function (resolve, reject) {

        // check for estimation settings
        if (knobs.estimates['png']) {

            // create a file buffer
            var measuring = buff(image);
            measuring.then(function(measurement) {

                // and estimate the scaling factor from its length
                var factor = limit / measurement.length;

                // determine height of new image and rescale
                var size = Math.round(factor * image.bitmap.height);
                var rescaling = scale(image, size);
                rescaling.then(function(rescaled) {

                    // create buffer
                    var buffing = buff(rescaled);
                    buffing.then(function(buffer) {

                        // print status
                        tools.debug('original height: ' + image.bitmap.height);
                        tools.debug('rescaling factor: ' + factor);
                        tools.debug('new height: ' + size);
                        tools.debug('buffer size: ' + buffer.length);

                        // and resolve promise with buffer
                        resolve(factor);
                    })
                })
            })
        }
        // otherwise
        else {

            // default to 100% and resolve the promise
            factor = 1.0;
            resolve(factor);
        }
    })
    return promise;
}


// fragment a box into smaller horizontal boxes
function fragment(box, size, limit=100) {

    // begin fragments
    var fragments = [];

    // reset box height if necessary
    box.Height = Math.min(box.Height, 1.0 - box.Top);

    // calculate height and number of fragments
    var height = size * box.Height;
    var number = Math.ceil(height / limit);
    var chunk = box.Height / number;

    // for each of several trials
    var trials = [...Array(number).keys()]
    trials.forEach(function(trial) {

        // create a new fragment
        var top = box.Top + trial * chunk;
        var section = {'Left': box.Left, 'Top': top, 'Width': box.Width, 'Height': chunk};
        fragments.push(section);
    })
    return fragments;
}


// fuse together an original and modified image
function fuse(path, pathii) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // get first image from the path
        tools.debug('loading ' + path + '...');
        var imagining = studio.imagine(path);
        imagining.then(function(original) {

            // and get the second image
            tools.debug('loading ' + pathii + '...');
            reimagining = studio.imagine(pathii);
            reimagining.then(function(erasure) {

                // clone the original
                var clone = xerox(original);

                // perform resizing
                var width = original.bitmap.width;
                var height = original.bitmap.height;
                var stretching = stretch(clone, 2 * width, height);
                stretching.then(function(stretched) {

                    // add images to composite
                    stretched.composite(original, 0, 0);
                    stretched.composite(erasure, width, 0);

                    // and resolve with composite image
                    resolve(stretched);
                })
            })
        })
    })
    return promise;
}


// get the colors from a pixel in an image
function glimpse(horizontal, vertical, image) {

    // get the color breakdown of a pixel in the image
    var hexadecimal = image.getPixelColor(horizontal, vertical);
    var color = Jimp.intToRGBA(hexadecimal);

    return color;
}


// grab the image from a path or bitmap
function imagine(source) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // read in file
        Jimp.read(source, function(error, image) {

            // if unsuccessful
            if (error) {

                // log the error
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the image
                resolve(image);
            }
        })
    })
    return promise;
}


// nudge the coordinates of a polygon according to the quadrant
function nudge(polygon, quadrant) {

    // go through each point in the polygon
    polygon.forEach(function(point) {

        // correct for negatives
        point.X = Math.max(0, point.X);
        point.Y = Math.max(0, point.Y);

        // calculate x and y for quadrant shifts
        var x = (point.X / quadrant.resolution[0]) + quadrant.Left + quadrant.shift[0];
        var y = (point.Y / quadrant.resolution[1]) + quadrant.Top + quadrant.shift[1];

        // correct point
        point.X = x;
        point.Y = y;
    })
    // check for wrapping across boundaries
    polygon = cohere(polygon);

    return polygon;
}


// measure the overlap between two bounding boxes
function overlap(detection, redetection, mode) {

    // get boxes if faces
    if (mode == 'faces') {

        // get boxes
        var primary = detection.BoundingBox;
        var secondary = redetection.BoundingBox;
    }
    // get boxes if texts
    if (mode == 'texts') {

        // get boxes
        var primary = bound(detection.Geometry.Polygon);
        var secondary = bound(redetection.Geometry.Polygon);
    }
    // calculate size of redetection
    var size = secondary.Width * secondary.Height;

    //  get bounds of intersection
    var left = Math.max(primary.Left, secondary.Left);
    var right = Math.min(primary.Width + primary.Left, secondary.Width + secondary.Left);
    var top = Math.max(primary.Top, secondary.Top);
    var bottom = Math.min(primary.Height + primary.Top, secondary.Height + secondary.Top);

    // calculate intersection and fraction thereof
    var intersection = (right - left) * (bottom - top);
    var fraction = intersection / size;

    return fraction;
}


// peel off exif information from the bitmap
function peel(bitmap) {

    // begin promise
    var promise = new Promise(function(resolve, reject) {

        // make the image
        var imagining = imagine(bitmap);
        imagining.then(function(image) {

            // get mime type
            var mime = image.getMIME();

            // get exif data
            var exif = image._exif;

            // clone to erase exif information
            var clone = xerox(image);

            // recreate the bitmap as a buffer
            var buffing = buff(clone);
            buffing.then(function(buffer) {

                // resolve the promise with the exif data and buffer
                resolve([exif, buffer]);
            })
        })
    })
    return promise;
}


// convert a png file to a jpeg
function peg(path, directory) {

    // deconstruct name
    var name = path.split('/').pop().replace('.png', '');

    // read the file
    Jimp.read(path, function (error, image) {

        // rewrite to new directory
        var deposit = directory + '/' + name + '.jpg';
        tools.debug('writing ' + deposit + '...', 2);
        image.write(deposit);
    })
    return;
}


// punch out detection bounding box from image
function punch(box, image) {

    // clone image
    var snippet = xerox(image);

    // get image attributes
    var horizontal = snippet.bitmap.width;
    var vertical = snippet.bitmap.height;

    // calculate bounding box in absolute scale
    var left = Math.floor(box.Left * horizontal);
    var top = Math.floor(box.Top * vertical);
    var width = Math.ceil(box.Width * horizontal);
    var height = Math.ceil(box.Height * vertical);

    // correct for edge cases
    left = Math.max(left, 0);
    top = Math.max(top, 0);
    width = Math.min(width, horizontal - left);
    height = Math.min(height, vertical - top);

    // crop
    snippet.crop(left, top, width, height);

    return snippet;
}


// check the buffer size versus quality for a jpg file
function qualify(path) {

    // grab file
    var deposit = 'quality/qualities.json';
    var studies = tools.retrieve(deposit);
    studies[path] = {};

    // load up image
    var imagining = imagine(path);
    imagining.then(function(image) {

        // make into buffer and get size
        var mime = image.getMIME();
        var buffing = buff(image, mime);
        buffing.then(function(buffer) {

            // print length
            tools.debug('mime: ' + mime);
            tools.debug('original: ');
            tools.debug('length: ' + buffer.length);
            studies[path]['original'] = buffer.length;
            tools.dump(studies, deposit);
        })
        // define qualities
        var qualities = [100, 99, 98, 97, 95, 93, 90, 85, 80, 70, 60, 50, 40, 30, 20, 10];
        qualities.forEach(function(quality) {

            // clone image and adjust quality
            var clone = xerox(image);
            clone.quality(quality);

            // make into buffer and get size
            var mime = image.getMIME();
            var buffing = buff(clone, mime);
            buffing.then(function(buffer) {

                // print length
                tools.debug('quality: ' + quality);
                tools.debug('length: ' + buffer.length);

                // save to file
                var studies = tools.retrieve(deposit);
                studies[path][quality] = buffer.length;
                tools.dump(studies, deposit);
            })
        })
    })
    return;
}


// check the buffer size versus image size for a png file
function quantify(path) {

    // grab file
    var deposit = 'quality/quantities.json';
    var studies = tools.retrieve(deposit);
    studies[path] = {};

    // load up image
    var imagining = imagine(path);
    imagining.then(function(image) {

        // make into buffer and get size
        var mime = image.getMIME();
        var buffing = buff(image, mime);
        buffing.then(function(buffer) {

            // print length
            tools.debug('mime: ' + mime);
            tools.debug('original: ');
            tools.debug('height: ' + image.bitmap.height);
            tools.debug('length: ' + buffer.length);
            studies[path]['original'] = buffer.length;
            tools.dump(studies, deposit);

            // define quantities
            var quantities = [0.99, 0.98, 0.97, 0.95, 0.93, 0.90, 0.85, 0.80, 0.70, 0.60, 0.50, 0.40, 0.30, 0.20, 0.10];
            quantities.forEach(function(quantity) {

                // clone image
                var clone = xerox(image);

                // get size
                var size = Math.ceil(image.bitmap.height * quantity);

                // scale the image
                var scaling = scale(clone, size);
                scaling.then(function(scaled) {

                    // make into buffer and get size
                    var mime = scaled.getMIME();
                    var buffing = buff(scaled, mime);
                    buffing.then(function(buffer) {

                        // print stats
                        tools.debug('');
                        tools.debug('mime: ' + mime);
                        tools.debug('size: ' + size);
                        tools.debug('quantity: ' + quantity);
                        tools.debug('length: ' + buffer.length);

                        // save to file
                        var studies = tools.retrieve(deposit);
                        studies[path][quantity] = buffer.length;
                        tools.dump(studies, deposit);
                    })
                })
            })
        })
    })
    return;
}


// correct the coordinates of a bounding box if the width or height is negative
function rectify(box) {

    // check for negative in left or top
    box.Left = Math.max(0, box.Left);
    box.Top = Math.max(0, box.Top);

    // if the width is negative
    if (box.Width < 0) {

        // correct left and width
        var left = Math.max(0, box.Left + box.Width);
        box.Left = left;
        box.Width = -box.Width;
    }
    // if the height is negative
    if (box.Height < 0) {

        // correct top and height
        var top = Math.max(0, box.Top + box.Height);
        box.Top = top;
        box.Height = -box.Height;
    }
    return box;
}


// scale an image to another size (height)
function scale(image, size) {

    // create promise
    var promise = new Promise(function(resolve, reject) {

        // get image properties
        var width = image.bitmap.width;
        var height = image.bitmap.height;

        // get ratio
        var ratio = size / height;

        // adjust width and height
        width = Math.ceil(width * ratio);
        height = Math.ceil(height * ratio);

        // make clone
        var clone = xerox(image)

        // perform resizing
        clone.resize(width, height, Jimp.AUTO, function(error, resized) {

            // if unsuccessful
            if (error) {

                // print error
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the resized image
                resolve(resized);
            }
        })
    })
    return promise;
}


// scramble an image
function scramble(source, degree=16) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // get image
        var imagining = imagine(source);
        imagining.then(function(image) {

            // create blank image
            var width = degree * Math.floor(image.bitmap.width / degree);
            var height = degree * Math.floor(image.bitmap.height / degree);

            // rescale image
            var stretching = stretch(image, width, height);
            stretching.then(function(stretched) {

                // create new image
                new Jimp(width, height, function(error, blank) {

                    // if unsuccessful
                    if (error) {

                        // log error
                        tools.debug(error, 1);
                    }
                    // otherwise
                    if (!error) {

                        // get boxes
                        var boxes = dice(degree, degree);

                        // for each box
                        boxes.forEach(function(box) {

                            // punch out the tile
                            var tile = punch(box, stretched);

                            // recalculate coordinates
                            var left = (1 - (box.Left + box.Width)) * width;
                            var top = (1 - (box.Top + box.Height)) * height;

                            // superimpose on the blank image
                            blank.composite(tile, left, top);
                        })
                        // invert the colors
                        blank.invert();

                        // resolve the promise with the scrambled image
                        resolve(blank);
                    }
                })
            })
        })
    })
    return promise;
}


// shrink an encoding so that it is below AWS size limits
function shrink(bitmap, limit) {

    // make a promise
    var promise = new Promise(function(resolve, reject) {

        // if bitmap length is already within the limit
        if (bitmap.length <= limit) {

            // resolve immediately
            resolve(bitmap);
        }
        // otherwise shrink the bitmap
        else {

            // get image
            var imagining = imagine(bitmap);
            imagining.then(function(image) {

                // determine MIME type
                var mime = image.getMIME();

                // print status
                tools.debug('file too big!');
                tools.debug('file size: ' + bitmap.length);
                tools.debug('limit: ' + limit);
                tools.debug('mime: ' + mime);

                // if the image is a jpeg
                if (mime == 'image/jpeg') {

                    // estimate quality
                    var estimating = estimate(image, limit);
                    estimating.then(function(quality) {

                        // double check limit and lower quality stepwise
                        var disqualifying = studio.disqualify(image, quality, limit);
                        disqualifying.then(function(buffer) {

                            // and resolve with buffer
                            resolve(buffer);
                        })
                    })
                }
                // otherwise assume png/bmp or equivalent
                else {

                    // extrapolate scale factor
                    var extrapolating = extrapolate(image, limit);
                    extrapolating.then(function(factor) {

                        // double check that the limit has been met, or shrink further
                        var disquantifying = studio.disquantify(image, factor, limit);
                        disquantifying.then(function(buffer) {

                            // and resolve the promise with the buffer
                            resolve(buffer);
                        })
                    })
                }
            })
        }
    })
    return promise;
}


// rescale an image in both directions
function stretch(image, width, height) {

    // create promise
    var promise = new Promise(function(resolve, reject) {

        // perform resizing
        image.resize(width, height, Jimp.AUTO, function(error, resized) {

            // if unsuccessful
            if (error) {

                // print error
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the stretched image
                resolve(resized);
            }
        })
    })
    return promise;
}


// repeatedly blur a snippet
function stutter(snippet, degrees) {

    // set up promise
    var promise = new Promise(function(resolve, reject) {

        // if an empty list
        if (degrees.length < 1) {

            // resolve immediately
            resolve(snippet);
        }
        // otherwise get last degree
        if (degrees.length > 0) {

            // get degree
            var degree = degrees.pop();

            // and blur
            var blurring = blur(snippet, degree);
            blurring.then(function(blurred) {

                // then blur to next degree
                snippet = stutter(blurred, degrees);
            })
        }
    })
    return promise;
}


// superimpose an image with a snippet
function superimpose(snippet, image, left, top) {

    // construct promise
    var promise = new Promise(function(resolve, reject) {

        // make composite
        image.composite(snippet, left, top, function(error, clone) {

            // if unsuccessful
            if (error) {

                // log the error
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the clone
                resolve(clone);
            }
        })
    })
    return promise;
}


// undo the exif rotation
function untwist(image) {

    // construct promise
    var promise = new Promise(function(resolve, reject) {

        // perform exif rotations
        image.exifRotate(function(error, image) {

            // if unsuccessful
            if (error) {

                // log error to screen
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve the promise with the untwisted image
                var clone = xerox(image);
                resolve(clone);
            }
        })
    })
    return promise;
}


// unwrap a bounding box that wraps across the image boundary
function unwrap(left, top, width, height) {

    // if the left coordinate is past the right boundary
    if (left > 1.0) {

        // correct the left coordinate
        left = left - 1.0;
    }
    // otherwise
    else {

        // make sure width is in bounds
        width = Math.min(width, 1.0 - left);
    }
    // if the top coordinate is past the bottom boundary
    if (top > 1.0) {

        // correct the top coordinate
        top = top - 1.0;
    }
    // otherwise
    else {

        // make sure height is in bounds
        height = Math.min(height, 1.0 - top);
    }
    return [left, top, width, height];
}


// visualize a text file
function visualize(path) {

    // load in text file
    var textualizing = tools.textualize(path);
    textualizing.then(function(text) {

    // convert to binary
    var bitmap = Buffer.from(text, 'base64');

        // make new image
        new Jimp(100, 100, function(error, image) {

            // if unsuccessful
            if (error) {

                // log error
                tools.debug(error, 1);
            }
            // otherwise
            if (!error) {

                // replace bitmap
                image.bitmap.data = bitmap;

                // make deposit path and save
                var deposit = path.replace('.txt', '.png');
                image.write(deposit);
            }
        })
    })
    return;
}


// xerox an image, retaining mime info
function xerox(image) {

    // get the mime
    var mime = image.getMIME();

    // make clone
    var clone = image.clone();

    // reattach mime
    clone._originalMime = mime;

    return clone;
}


// set exports
exports = {'Jimp': Jimp, 'blur': blur, 'bounce': bounce, 'bound': bound, 'budge': budge, 'buff': buff, 'bump': bump};
Object.assign(exports, {'cohere': cohere, 'diagonalize': diagonalize, 'dice': dice, 'disqualify': disqualify});
Object.assign(exports, {'disquantify': disquantify, 'equate': equate, 'estimate': estimate, 'extrapolate': extrapolate});
Object.assign(exports, {'fragment': fragment, 'fuse': fuse, 'glimpse': glimpse, 'imagine': imagine, 'nudge': nudge});
Object.assign(exports, {'overlap': overlap, 'peel': peel, 'peg': peg, 'punch': punch, 'qualify': qualify});
Object.assign(exports, {'quantify': quantify, 'rectify': rectify, 'scale': scale, 'scramble': scramble});
Object.assign(exports, {'shrink': shrink, 'stretch': stretch, 'stutter': stutter, 'superimpose': superimpose});
Object.assign(exports, {'untwist': untwist, 'unwrap': unwrap, 'visualize': visualize, 'xerox': xerox});
module.exports = exports;

// status
console.log('studio loaded.');