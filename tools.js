// tools.js for general processing tools

// import file manipulation
var fs = require('fs-extra');

// import util for inspecting objects
var util = require('util');

// import knobs
var knobs = require('./knobs.js');


// average values in a list
function average(array) {

    // default mean to zero
    var mean = 0;

    // if array is nonzero
    if (array.length > 0) {

        // sum values
        var sum = array.reduce(function(a, b) {return a + b}, 0);

        // and divide by the length
        mean = sum / array.length;
    }
    return mean;
}


// batch run a segment
function batch(operator, segment) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // for each member of the segment
        var promises = [];
        var outputs = [];
        segment.forEach(function(member) {

            // operate on the member
            var operating = operator(member);
            promises.push(operating);
            operating.then(function(output) {

                // and add output to outputs
                outputs.push(output);
            })
        })
        // when all promises are resolved
        Promise.all(promises).then(function() {

            // resolve the main promise with the outputs
            resolve(outputs);
        })
    })
    return promise;
}


// bitmap a file
function bite(path) {

    // load into bitmap
    var bitmap = fs.readFileSync(path);

    return bitmap;
}


// chain together batches
function chain(operator, pool, chunk=5, outputs=[]) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // print status
        tools.debug('incoming outputs: ' + outputs.length);

        // if pool is empty
        if (pool.length < 1) {

            // resolve the promise with the outputs
            resolve(outputs);
        }
        // otherwise recurse
        else {

            // clip batch from pool
            var batch = pool.slice(0, chunk);
            pool = pool.slice(chunk, pool.length);

            // operate on each member of the batch
            var promises = []
            batch.forEach(function(member) {

                // print status
                debug('operating on ' + member + '...', 3);

                // operate on member
                var operating = operator(member);
                promises.push(operating);
                operating.then(function(output) {

                    // add to outputs
                    outputs.push(output);
                })
            })
            // print status
            debug(promises);

            // once all promises are fulfilled
            Promise.all(promises).then(function() {

                // print the status
                debug(promises, 3);

                // continue with the next batch
                resolve(chain(operator, pool, chunk, outputs));
            })
        }
    })
    return promise;
}


// chop a pool into chunks
function chop(pool, chunk) {

    // get number of batches
    var number = Math.ceil(pool.length / chunk);

    // for each number
    var batches = [];
    for (i=0; i < number; i++) {

        // add a batch
        batches.push(pool.slice(i * chunk, (i + 1) * chunk));
    }
    return batches;
}


// view output on screen for debugging
function debug(data, level=2) {

    // if level is no greater than debugging level
    if (level <= knobs.debugging) {

        // log to console
        console.log(data);
    }
    return;
}


// calculate the standard deviation
function deviate(array) {

        // get the standard deviation, as mean of squares - square of mean
        var squares = array.map(function(member) {return member ** 2});
        squares = squares.reduce(function(a, b) {return a + b}, 0) / array.length;

        // get the mean
        var mean = array.reduce(function(a, b) {return a + b}, 0) / array.length;

        // calculate deviation
        var variance = squares - mean ** 2;
        var deviation = Math.sqrt(variance);

    return deviation;
}


// dump data into a json file
function dump(data, deposit) {

    // dump into json deposit path
    fs.writeFileSync(deposit, JSON.stringify(data, '', 4));

return;
}


// base 64 encode a file
function encode(path) {

    // load up bitmap
    var bitmap = fs.readFileSync(path);

    // convert to base64
    base = bitmap.toString('base64');

    return base;
}


// make a new file
function make(path) {

    // create blank object
    var blank = {};

    // dump into the path
    tools.dump(blank, path);

    return;
}


// get all paths from a directory
function pathfind(directory) {

    // get all files from directory
    var names = fs.readdirSync(directory);

    // for each name in the directory
    var paths = [];
    names.forEach(function(name) {

        // construct the full path
        var path = directory + '/' + name;
        paths.push(path);
    })
    return paths;
}


// retrieve a json file as an object
function retrieve(path) {

    // read in data
    var data = fs.readFileSync(path);
    data = JSON.parse(data);

    return data;
}


// see data from a promise
function see(promise) {

    // view promise
    promise.then(function(data) {

        // inspect object
        console.log(util.inspect(data, false, null, true));
    })
    return;
}


// sleep for some miliseconds
function sleep(milliseconds) {

    // create promise
    promise = new Promise(function(resolve, reject) {

        // set timeout
        setTimeout(resolve, (milliseconds));
    })
    return promise;
}


// get the stub from a path
function stub(path) {

    // get rid of extension and directories
    var name = path.split('.')[0].split('/').pop();

    return name;
}


// read in a text file
function transcribe(path) {

    // make promise
    var promise = new Promise(function(resolve, reject) {

        // read in path
        fs.readFile(path, 'utf8', function(error, text) {

            // if unsuccessful
            if (error) {

                // print error status
                debug(error, 1);
            }
            // otherwise
            if (!error) {

                // resolve promise with the text
                resolve(text);
            }
        })
    })
    return promise;
}


// set exports
exports = {'average': average, 'batch': batch, 'bite': bite, 'chain': chain, 'chop': chop, 'debug': debug};
Object.assign(exports, {'deviate': deviate, 'dump': dump, 'encode': encode, 'make': make, 'pathfind': pathfind});
Object.assign(exports, {'retrieve': retrieve, 'see': see, 'sleep': sleep, 'stub': stub, 'transcribe': transcribe});
module.exports = exports;

// status
console.log('tools loaded.');